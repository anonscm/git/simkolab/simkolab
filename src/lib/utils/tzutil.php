<?php
/*-
 * Copyright © 2014, 2015, 2016
 *	⮡ tarent solutions GmbH (http://www.tarent.de)
 *	mirabilos <t.glaser@tarent.de>
 * Copyright © 2009–2012
 *	Metaways Infosystems GmbH (http://www.metaways.de)
 *	Jonas Fischer <j.fischer@metaways.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *-
 * Functions associated with handling ActiveSync TimeZone information.
 * General idea derived from Tine 2.0 (because the functions in Z-Push
 * are bad, and those in Horde even worse), which seems to derive from
 * the php.net documentation. Lots of fixes and assumptions reduction;
 * endianness fixes, etc.
 */


/*
 * Helper to convert a transition to a [MS-DTYP] §2.3.13 SYSTEMTIME
 *
 * @param array $trans		transition to convert
 * @result array		SYSTEMTIME structure
 */
function tz2as_trans2st($trans) {
	$dp = getdate($trans['ts']);
	$rv = array(
		'wYear' => $dp['year'],
		'wMonth' => $dp['mon'],
		'wDayOfWeek' => $dp['wday'],
		'wDay' => $dp['mday'],
		'wHour' => $dp['hours'],
		'wMinute' => $dp['minutes'],
		'wSecond' => $dp['seconds'],
		'wMilliseconds' => 0,
	    );

	/*
	 * Day-in-month format is specified by setting the wYear member
	 * to zero, setting the wDayOfWeek member to an appropriate
	 * weekday, and using a wDay value in the range 1 through 5 to
	 * select the correct day in the month. Using this notation, the
	 * first Sunday in April can be specified, as can the last
	 * Thursday in October (5 is equal to “the last”).
	 */
	$rv['wYear'] = 0;

	/*
	 * PHP is not expressive enough for figuring out whether this is
	 * the last occurrence or the fifth (or fourth), so we just take
	 * the n-th occurrence of the day in the month here.
	 */
	$rv['wDay'] = (int)(($rv['wDay'] - 1) / 7) + 1;

	return $rv;
}

/*
 * Convert a DateTimeZone object into an ActiveSync timezone string
 *
 * @param DateTimeZone $tz	PHP/Olson timezone to convert
 * @param time_t $starttime	(optional) timestamp corresponding to start
 *				year (default = current date)
 * @result string		base64 string per [MS-ASDTYPE] §2.6.4
 */
function tz2as($tz, $starttime=false) {
	if ($starttime === false)
		$starttime = time();
	$startdate = new DateTime('@' . $starttime);
	$startdate->setTimezone($tz);

	$y = $startdate->format('Y');
	$to_dst = NULL;
	$to_std = NULL;

	/*
	 * We cannot use mktime here because these need to be calculated
	 * relative to the chosen timezone, not the system-wide timezone!
	 */
	$dbeg = new DateTime(($y - 1) . '-12-01 00:00:00', $tz);
	$dend = new DateTime(($y + 1) . '-01-01 00:00:00', $tz);
	$transitions = $tz->getTransitions($dbeg->getTimestamp(), $dend->getTimestamp());

	foreach ($transitions as $index => $transition) {
		$dtr = new DateTime('@' . $transition['ts']);
		$dtr->setTimezone($tz);
		/* nor can we use strftime here, see above! */
		if ($dtr->format('Y') == $y) {
			$transition2 = NULL;
			if (isset($transitions[$index + 1])) {
				$dtn = new DateTime('@' .
				    $transitions[$index + 1]['ts']);
				$dtn->setTimezone($tz);
				if ($dtn->format('Y') == $y)
					$transition2 = $transitions[$index + 1];
			}
			$to_dst = $transition['isdst'] ? $transition : $transition2;
			$to_std = $transition['isdst'] ? $transition2 : $transition;
			break;
		}
	}

	$empty_st = array(
		'wYear' => 0,
		'wMonth' => 0,
		'wDayOfWeek' => 0,
		'wDay' => 0,
		'wHour' => 0,
		'wMinute' => 0,
		'wSecond' => 0,
		'wMilliseconds' => 0,
	    );

	if ($to_std) {
		$ofsbias = $to_std['offset'] / -60;
		$stdname = $to_std['abbr'];
		if ($to_dst) {
			$stdtime = tz2as_trans2st($to_std);
			/*
			 * SYSTEMTIME is UTC, but time zone transition
			 * information apparently wants local time here.
			 *
			 * http://mail2.leonidas.com/bin/opennetcf.windowsce.xml
			 * says: The date and local time when the
			 * transition from Daylight time to Standard
			 * time occurs. WTF?
			 */
			$stdtime['wHour'] += $to_std['offset'] / 3600;
			$stdbias = 0;
			$dstname = $to_dst['abbr'];
			$dsttime = tz2as_trans2st($to_dst);
			/* transitions use local time */
			$dsttime['wHour'] += $to_std['offset'] / 3600;
			$dstbias = ($to_dst['offset'] - $to_std['offset']) / -60;
		} else {
			$stdtime = $empty_st;
			$stdbias = 0;
			$dstname = '';
			$dsttime = $empty_st;
			$dstbias = 0;
		}
	} else {
		/* no transition to Standard time found */
		$ofsbias = $tz->getOffset($startdate);
		$stdname = $tz->getName();
		$stdtime = $empty_st;
		$stdbias = 0;
		$dstname = '';
		$dsttime = $empty_st;
		$dstbias = 0;
	}

	/* convert names to UCS-2LE */
	$mb_encoding = mb_internal_encoding();
	mb_internal_encoding("UTF-8");
	$stdnameW = mb_convert_encoding($stdname, "UTF-16LE", "UTF-8");
	if (mb_convert_encoding($stdnameW, "UTF-8", "UTF-16LE") !== $stdname) {
		/* invalid; try alternative name first */
		$stdname = $tz->getName();
		$stdnameW = mb_convert_encoding($stdname, "UTF-16LE", "UTF-8");
	}
	if (mb_convert_encoding($stdnameW, "UTF-8", "UTF-16LE") !== $stdname)
		/* invalid */
		$stdnameW = '';
	$dstnameW = mb_convert_encoding($dstname, "UTF-16LE", "UTF-8");
	if (mb_convert_encoding($dstnameW, "UTF-8", "UTF-16LE") !== $dstname)
		/* invalid — just use standard name, like MS does */
		$dstnameW = $stdnameW;
	mb_internal_encoding($mb_encoding);

	/* arrgh! PHP pack() has BE/LE only for unsigned ints! */
	if (!defined('BYTE_ORDER')) {
		define('BIG_ENDIAN', 4321);
		define('LITTLE_ENDIAN', 1234);
		$x = pack('L', 0x01020304);
		if ($x === "\x01\x02\x03\x04")
			define('BYTE_ORDER', 'BIG_ENDIAN');
		elseif ($x === "\x04\x03\x02\x01")
			define('BYTE_ORDER', 'LITTLE_ENDIAN');
		else {
			/* PDP Endian and other catastrophes */
			throw new Exception('weird host byte order');
		}
	}

	/* yes, this is ugly */
	$s0 = pack('l', $ofsbias);
	$s1 = pack('l', $stdbias);
	$s2 = pack('l', $dstbias);
	if (BYTE_ORDER == BIG_ENDIAN) {
		/* we need to bswap some things */
		$s0 = $s0[3] . $s0[2] . $s0[1] . $s0[0];
		$s1 = $s1[3] . $s1[2] . $s1[1] . $s1[0];
		$s2 = $s2[3] . $s2[2] . $s2[1] . $s2[0];
	}
	return base64_encode($s0 .
	    pack('a64vvvvvvvv', $stdnameW,
	    $stdtime['wYear'], $stdtime['wMonth'], $stdtime['wDayOfWeek'],
	    $stdtime['wDay'], $stdtime['wHour'], $stdtime['wMinute'],
	    $stdtime['wSecond'], $stdtime['wMilliseconds']) . $s1 .
	    pack('a64vvvvvvvv', $dstnameW,
	    $dsttime['wYear'], $dsttime['wMonth'], $dsttime['wDayOfWeek'],
	    $dsttime['wDay'], $dsttime['wHour'], $dsttime['wMinute'],
	    $dsttime['wSecond'], $dsttime['wMilliseconds']) . $s2);
}
