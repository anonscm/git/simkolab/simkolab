<?php
/**
 * Syncphony for SimKolab — a client library to access kolab-ws — is
 * Copyright © 2010–2012 Sven Braun, tarent GmbH
 * Copyright © 2013–2018 mirabilos <t.glaser@tarent.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *-
 * Implement communication with the Kolab Webservice. The class provides
 * methods to fetch contacts and events (appointments) from kolab-ws via
 * SOAP calls, reusing the Syncphony web service. Items are tracked in a
 * database, with their own, new, unique ID, as well as its Kolab Folder
 * and Item ID.
 */

// changable in /opt/wildfly-9.0.2.Final/standalone/configuration/kolab-ws.properties
define_dfl('DEFAULT_SYNC_WINDOW_PAST_DAYS', 60);
define_dfl('DEFAULT_SYNC_WINDOW_FUTURE_DAYS', 60);

function xmlescape($s) {
    return (str_replace(
      array('&',     '<',     '>',     '"',     "'"),
      array('&#38;', '&#60;', '&#62;', '&#34;', '&#39;'),
      $s));
}

class syncphony {
    public $client;
    private $user;
    private $userpassword;
    private $wsdl;
    private $statusWsdl;
    private $secureHeader;
    public $currentTimestring;
    public $currentTimestamp;
    private $_deviceId;
    private $asv;
    private $standardFolder = array();

    /* +++ Initialisation +++ */

    /**
     * Initialises the Syncphony wrapper; call init() to create
     * the SOAP WSSecurity Header, check the database connection,
     * create the SOAP client, use it to fetch the current time
     * from kolab-ws and thus testing the WS connection/server!
     *
     * @param <string> $username    Kolab eMail address
     * @param <string> $password    Kolab user password
     * @param <string> $deviceId    ActiveSync device ID
     * @param <float>  $as_version  ActiveSync version
     */
    public function __construct($username, $password, $deviceId, $as_version) {
        $this->wsdl = SYNCPHONY_SERVICE_WSDL;
        $this->statusWsdl = SYNCPHONY_SERVICE_STATUS_WSDL;
        $this->user = trim($username);
        $this->userpassword = $password;
        $this->_deviceId = $deviceId;
        $this->asv = $as_version;
    }

    /* separate function to avoid logging the password */
    public function init() {
        // create secure header
        $this->secureHeader = $this->createSOAPSecurityHeader();
        // create SOAP Client
        try {
            $this->client = new FilteringSoapClient(SYNCPHONY_SERVICE_WSDL, array(
                'exceptions' => 0,
                'trace' => 1,
              ));
        } catch (SoapFault $fault) {
            throw new FatalException('syncphony initialisation failed: ' .
              'SOAP fault: ' . $fault->getMessage(), 0, NULL, LOGLEVEL_FATAL);
        }
        // initialise database link
        if (!sk_schemavsn())
            throw new FatalException('syncphony initialisation failed: ' .
              'no database connection', 0, NULL, LOGLEVEL_FATAL);
        // get current time from WS
        // Format: Y-m-d'T'H:i:s.u'Z' (2010-11-24T10:37:47.695Z)
        $res = $this->soapcall('getCurrentTime', array());
        if (is_soap_fault($res) || empty($res->return))
            throw new FatalException('syncphony initialisation failed: ' .
              'no kolab-ws connection', 0, NULL, LOGLEVEL_FATAL);
        // Format: Y-m-d'T'H:i:s'Z' (2010-11-24T10:37:47Z)
        $cur = syncphony::parseDateTime($res->return);
        $this->currentTimestring = syncphony::renderDateTime($cur);
        $this->currentTimestamp = syncphony::renderTimestamp($cur);
        // success
    }

    /* +++ SOAP library wrapper +++ */

    /*
     * Somewhat sane wrapper around __soapCall passing all those magic
     * arguments as well, and getting us rid of exceptions, and
     * auto-logging SOAP faults. Callers may use is_soap_fault() on the
     * result to do their own subsequent fault processing.
     */
    private function soapcall($rmethod, $args) {
        try {
            $res = $this->client->__soapCall($rmethod, $args, NULL,
              $this->secureHeader);
        } catch (SoapFault $fault) {
            if (!is_soap_fault($fault)) {
                /* we assume something went wrong */
                util_logerr(LOGLEVEL_ERROR, 'SOAP fault not a SoapFault object!');
                debugLog('Exception message: ' . $fault->getMessage());
                ZLog::WriteEnd();
                throw new StatusException('SOAP fault not a SoapFault object!', SYNC_COMMONSTATUS_SERVERERRORRETRYLATER);
            }
            $res = $fault;
        }
        if (is_soap_fault($res)) {
            if (SYNCPHONY_CATCH_ALL_SOAPFAULTS) {
                /* random retry delay to spread server load */
                $rdelay = 20 + util_randnum(0xFF, 40);
                set_time_limit($rdelay + 10);
                $msg = 'SOAP fault, sleeping for ' . $rdelay . 's';
            } else
                $msg = 'SOAP fault';
            util_debugJ(true, NULL, $msg, array(
                '-method' => $rmethod,
                '-params' => $args,
                'code' => util_ifsetor($res->faultcode, NULL),
                'desc' => util_ifsetor($res->faultstring, NULL),
                'sender' => util_ifsetor($res->faultactor, NULL),
                'source' => ($res->getFile() . ':' . $res->getLine()),
                'trace' => $res->getTraceAsString(),
              ));
            if (SYNCPHONY_CATCH_ALL_SOAPFAULTS) {
                sleep($rdelay);
                throw new StatusException('SOAP fault', SYNC_COMMONSTATUS_SERVERERRORRETRYLATER);
            }
        } elseif (SOAP_DEBUG)
            util_debugJ(NULL, true, 'successful SOAP call', array(
                '-method' => $rmethod,
                '-params' => $args,
                'result' => $res,
              ));
        return $res;
    }

    /**
     * createSOAPSecurityHeader() creates a WSSecurity SOAP Header wich will be
     * used by syncphony for authentication.
     * @return SoapHeader
     */
    private function createSOAPSecurityHeader() {
        $nameSpace = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
        $nameSpaceTypePassword = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText';

        $userToken = new SoapVar($this->user, XSD_STRING, NULL, $nameSpace, NULL, $nameSpace);
        $passwToken = new SoapVar('<wssehack:Password xmlns:wssehack="' .
          xmlescape($nameSpace) . '" Type="' . xmlescape($nameSpaceTypePassword) .
          '">' . xmlescape($this->userpassword) . '</wssehack:Password>', XSD_ANYXML);

        $userNameToken = new UsernameToken($userToken, $passwToken);
        $userNameTokenObject = new SoapVar($userNameToken, SOAP_ENC_OBJECT, NULL, $nameSpace,
                        'UsernameToken', $nameSpace);

        $securityHeaderFrame = new SoapSecurityHeader($userNameTokenObject);
        $userToken = new SoapVar($securityHeaderFrame, SOAP_ENC_OBJECT, NULL, $nameSpace,
                        'UsernameToken', $nameSpace);

        $secHeaderValue = new SoapVar($userToken, SOAP_ENC_OBJECT, NULL,
                        $nameSpace, 'Security', $nameSpace);
        $secHeader = new SoapHeader($nameSpace, 'Security', $secHeaderValue);

        return $secHeader;
    }

    /* +++ date-related helper functions +++ */

    /* takes a timestring and formats it as xs:dateTime (empty ⇒ NULL) */
    public static function reformatDateTime($in) {
        return syncphony::renderDateTime(syncphony::parseDateTime($in));
    }

    /* takes a timestring and formats it as timestamp (empty ⇒ NULL) */
    public static function reformatTimestamp($in) {
        return syncphony::renderTimestamp(syncphony::parseDateTime($in));
    }

    /* same as reformatTimestamp, at 00:00:00 localtime (empty ⇒ NULL) */
    public static function reformatDatestamp($in) {
        return syncphony::renderDatestamp(syncphony::parseDateTime($in));
    }

    /* same as reformatDatestamp, takes DateTime, can round up (NULL ⇒ NULL) */
    public static function renderDatestamp($di, $roundup=0) {
        global $tz_utc, $tz_sk;

        if ($di === NULL)
            return NULL;

        $di->setTimezone($tz_utc);
        $dm = explode(' ', $di->format('Y m d'));
        $do = new DateTime();
        $do->setTimezone($tz_sk);
        $do->setDate($dm[0], $dm[1], $dm[2] + $roundup);
        $do->setTime(0, 0, 0);
        return syncphony::renderTimestamp($do);
    }

    /* takes a timestamp, rounds down, formats as date-only (empty ⇒ NULL) */
    public static function timetDateTrunc($ts, $roundup=0, $tz=NULL) {
        global $tz_utc, $tz_sk;

        if (empty($ts))
            return NULL;
        if ($tz === NULL)
            $tz = $tz_sk;

        $d = new DateTime(NULL, $tz_utc);
        $d->setTimestamp($ts);
        $d->setTimezone($tz);
        $m = explode(' ', $d->format('Y m d'));
        $d->setDate($m[0], $m[1], $m[2] + $roundup);
        return $d->format('Y-m-d');
    }

    /* formats a timestamp as xs:dateTime (empty ⇒ NULL) */
    public static function timetDateTime($ts) {
        global $tz_utc;

        if (empty($ts))
            return NULL;

        $d = new DateTime(NULL, $tz_utc);
        $d->setTimestamp($ts);
        return syncphony::renderDateTime($d);
    }

    /* formats a DateTime as xs:dateTime (NULL ⇒ NULL) */
    public static function renderDateTime($d) {
        global $tz_utc;

        if ($d === NULL)
            return NULL;

        $d->setTimezone($tz_utc);
        return $d->format('Y-m-d\TH:i:s\Z');
    }

    /* formats a DateTime as timestamp (NULL ⇒ NULL) */
    public static function renderTimestamp($d) {
        if ($d === NULL)
            return NULL;

        return $d->getTimestamp() ? : 10;
    }

    /* parses a timestring into a local DateTime (empty ⇒ NULL) */
    public static function parseDateTime($in) {
        global $tz_utc, $tz_sk;

        if (empty($in))
            return NULL;

        $d = new DateTime($in, $tz_utc);
        $d->setTimezone($tz_sk);
        return $d;
    }

    /* takes a timestamp, checks if it’s midnight localtime (empty ⇒ false) */
    public static function timetIsMidnight($ts) {
        global $tz_utc, $tz_sk;

        if (empty($ts))
            return false;

        $di = new DateTime(NULL, $tz_utc);
        $di->setTimestamp($ts);
        $di->setTimezone($tz_sk);
        $dm = explode(' ', $di->format('Y m d'));
        $do = new DateTime();
        $do->setTimezone($tz_sk);
        $do->setDate($dm[0], $dm[1], $dm[2]);
        $do->setTime(0, 0, 0);

        return ($do->getTimestamp() == $ts);
    }

    /* parses a datestring into a local DateTime at day’s end (empty ⇒ NULL) */
    public static function parseDateUntil($in) {
        global $tz_utc, $tz_sk;

        if (empty($in))
            return NULL;

        $di = new DateTime($in, $tz_utc);
        $dm = explode(' ', $di->format('Y m d'));
        $do = new DateTime();
        $do->setTimezone($tz_sk);
        $do->setDate($dm[0], $dm[1], $dm[2]);
        $do->setTime(23, 59, 59);
        return $do;
    }

    /* converts a KolabXML body to an AS body/asbody */
    private static function bodyFromKolab(&$kolab, &$mobile) {
        $body = util_ifsetor($kolab->body, '');
        Utils::CheckAndFixEncoding($body);
        $body = util_sanitise_multiline_submission(trim($body));
        if (!$body) {
            /* assumes $mobile is new, i.e. no update or need to unset */
            return;
        }

        if (Request::GetProtocolVersion() >= 12.0) {
            $mobile->asbody = new SyncBaseBody();
            $mobile->asbody->data = StringStreamWrapper::Open($body);
            $mobile->asbody->type = SYNC_BODYPREFERENCE_PLAIN;
        } else {
            $mobile->body = StringStreamWrapper::Open($body);
        }
    }

    /* converts an AS body/asbody or RTF to a KolabXML body */
    private static function bodyToKolab(&$mobile, &$kolab) {
        $body = '';
        if (util_ifsetor($mobile->body)) {
            $body = $mobile->body;
        } elseif (isset($mobile->asbody) && util_ifsetor($mobile->asbody->data)) {
            $body = $mobile->asbody->data;
            if (is_resource($body))
                $body = stream_get_contents($body);
            switch (util_ifsetor($mobile->asbody->type, NULL)) {
            case NULL:
                util_debugJ('bodyToKolab: asbody->type unset');
                break;
            case SYNC_BODYPREFERENCE_PLAIN:
                break;
            case SYNC_BODYPREFERENCE_HTML:
                $body = Utils::ConvertHtmlToText(util_fixutf8($body));
                break;
            case SYNC_BODYPREFERENCE_RTF:
                $body = syncphony::getPlainTextFromRTF($body);
                break;
            case SYNC_BODYPREFERENCE_MIME:
                util_debugJ('bodyToKolab: asbody->type MIME not supported');
                break;
            default:
                util_debugJ('bodyToKolab: asbody->type unknown:', $mobile->asbody->type);
                break;
            }
        } elseif (util_ifsetor($mobile->rtf)) {
            $body = syncphony::getPlainTextFromRTF($mobile->rtf);
        }

        $body = trim(util_fixutf8($body));
        if ($body)
            $kolab['body'] = $body;
        else
            unset($kolab['body']);
    }

    /* +++ ChangesSink +++ */

    public function checkStatus($kind) {
        $mod = 0;
        foreach ($this->getFolders($kind) as $currentFolder) {
            $lastUpdateTime = $this->getLastFolderUpdateTime($currentFolder);
            foreach (array('New', 'Updated', 'Removed') as $action) {
                $pimType = 'get' . $action . $kind . 'IDs';
                $res = $this->soapcall($pimType, array(
                    $pimType => array(
                        'folder' => array(
                            'name' => $currentFolder,
                          ),
                        'deviceId' => $this->_deviceId,
                        'lastUpdateTime' => $lastUpdateTime,
                  )));
                if (!is_soap_fault($res) && isset($res->return))
                    $mod += count($res->return);
            }
        }
        $rv = 'M:' . $mod . '-R:0-U:0';
        debugLog(strtolower($kind) . 'Count: ' . $rv);
        return $rv;
    }

    /**
     * getFolders - fetches a list of Kolab folder IDs for a specified
     * kind (Contact, Event) from kolab-ws
     */
    private function getFolders($kind) {
        $folderIDs = array();
        $result = $this->soapcall('get' . $kind . 'Folders', array());
        if (!is_soap_fault($result) && !empty($result->return))
            foreach (util_mkarray($result->return) as $value)
                $folderIDs[] = $value->name;
        return $folderIDs;
    }

    private function getLastFolderUpdateTime($folderId) {
        $lastUpdateTime = '1970-01-01T00:00:00Z';
        $res = db_query_params('SELECT last_modified FROM items
            WHERE folder_id=$1
            ORDER BY maybe_timestamptz(last_modified) DESC LIMIT 1',
          array($folderId));
        /* was there a sync of that folder yet? */
        if ($res && db_numrows($res) > 0)
            $lastUpdateTime = db_result($res, 0, 'last_modified');
        return $lastUpdateTime;
    }

    /* +++ GetMessageList +++ */

    /**
     * Searches Kolab folders for all available items of some kind
     * (Event, Contact) and returns an array with the stats of those
     * items, for further use by SimKolab
     */
    public function getMessageList($desc, $kind) {
        debugLog('Fetching ' . $desc . ' folders and stats');
        $stats = array();
        foreach ($this->getFolders($kind) as $folderID) {
            debugLog('Fetching ' . $desc . ' stats for folder: ' . $folderID);
            $fn = 'get' . $kind . 'ChangeTimes';
            $res = $this->soapcall($fn, array(
                $fn => array(
                    'folder' => array(
                        'name' => $folderID,
                      ),
                    'updateTime' => $this->currentTimestring,
              )));
            if (is_soap_fault($res) || !isset($res->return))
                continue;
            foreach (util_mkarray($res->return) as $statentry)
                array_push($stats, array(
                    'id' => syncphony::getIDFromDB($folderID, $statentry->ID, $statentry->changeDate),
                    'flags' => 1,
                    'mod' => $statentry->changeDate,
                  ));
        }
        return $stats;
    }

    /* +++ StatMessage +++ */

    /**
     * getStatsFromSyncphonyID - get stats from a single item
     * @param <int> $syncphonyID
     * @return <array> stats
     */
    public static function getStatsFromSyncphonyID($syncphonyID) {
        $res = db_query_params('SELECT id, last_modified FROM items
            WHERE id=$1',
          array($syncphonyID));
        if ($syncphonyID === '' || !$res || db_numrows($res) < 1) {
            return false;
        }
        return array(
            'id' => db_result($res, 0, 'id'),
            'flags' => 1,
            'mod' => db_result($res, 0, 'last_modified'),
          );
    }

    /* +++ helper functions for MeetingResponse +++ */

    public static function checkItemExists($folderID, $kolabID) {
        $res = db_query_params('SELECT id, last_modified FROM items
            WHERE folder_id=$1 AND kolab_id=$2',
          array($folderID, $kolabID));
        if (!$res)
            return NULL;
        if (db_numrows($res) < 1)
            /* no entry found */
            return false;
        /* we got one (and only one, as they are UNIQUE) */
        return db_result($res, 0, 'id');
    }

    /**
     * getKolabIDFromSyncphonyID - fetches the kolab ID and the kolab folder ID
     * of a corresponding syncphony ID from the database.
     * @param <int> $syncphonyID
     * @return <array of Strings> 'id' = kolab ID, 'folder_id' = kolab folder ID
     */
    public static function getKolabIDFromSyncphonyID($syncphonyID) {
        $res = db_query_params('SELECT kolab_id, folder_id FROM items
            WHERE id=$1',
          array($syncphonyID));
        if (!$res || db_numrows($res) < 1)
            return false;
        return array(
            'id' => db_result($res, 0, 'kolab_id'),
            'folder_id' => db_result($res, 0, 'folder_id'),
          );
    }

    public function getStandardFolder($kind) {
        if (!($n = util_ifsetor($this->standardFolder[$kind]))) {
            if (!is_soap_fault($res =
              $this->soapcall('get' . $kind . 'Folders', array())) &&
              !empty($res->return))
                $n = is_array($res->return) ? $res->return[0]->name :
                  $res->return->name;
            else
                $n = '';
            $this->standardFolder[$kind] = $n;
        }
        return $n;
    }

    public function setAttachment($folderId, $kolabId, $attachmentId, $lastUpdateTime, $attachmentData) {
        return $this->soapcall('updateAttachment', array(
            'updateAttachment' => array(
                'folder' => array(
                    'name' => $folderId,
                  ),
                'id' => $kolabId,
                'deviceId' => $this->_deviceId,
                'attachmentId' => $attachmentId,
                'lastUpdateTime' => $lastUpdateTime,
                'attachmentData' => $attachmentData,
              ),
          ));
    }

    /* +++ helper function to fetch a single item from kolab-ws +++ */

    private function getItem($kind, $folderID, $kolabID) {
        $action = 'get' . $kind;
        $res = $this->soapcall($action, array(
            $action => array(
                'folder' => array(
                    'name' => $folderID,
                  ),
                'id' => $kolabID,
          )));
        if (is_soap_fault($res) || !isset($res->return))
            return false;
        return $res->return;
    }

    /* +++ helper function to enter an entry into the DB +++ */

    /**
     * getIDFromDB - searches the database for the item; fetches, updates,
     * or creates it; returns the new short ID for SimKolab
     * @param <type> $folderID
     * @param <type> $kolabID
     * @param <type> $lastModifiedDate
     * @return string $syncphonyID
     */
    public static function getIDFromDB($folderID, $kolabID, $lastModifiedDate) {
        $iD = '';
        $res = db_query_params('SELECT id, last_modified FROM items
            WHERE folder_id=$1 AND kolab_id=$2',
          array($folderID, $kolabID));
        if (!$res)
            return $iD;
        if (db_numrows($res) < 1) {
            // no entry found
            $res = db_query_params('INSERT INTO items
                (last_modified, kolab_id, folder_id)
                VALUES ($1, $2, $3)',
              array($lastModifiedDate, $kolabID, $folderID));
            if (!$res)
                return $iD;
            // fetch id!
            $res = db_query_params('SELECT id FROM items
                WHERE folder_id=$1 AND kolab_id=$2',
              array($folderID, $kolabID));
            if (!$res)
                return $iD;
            elseif (db_numrows($res) > 0)
                $iD = db_result($res, 0, 'id');
        } else {
            $iD = db_result($res, 0, 'id');

            // check if change_date changed
            if ($lastModifiedDate &&
              (db_result($res, 0, 'last_modified') != $lastModifiedDate)) {
                $res = db_query_params('UPDATE items
                    SET last_modified=$1 WHERE id=$2',
                  array($lastModifiedDate, $iD));
                if (db_affected_rows($res) < 1)
                    util_debugJ(true, 'Failed to update DB: ' . db_error());
            }
        }
        return $iD;
    }

    public static function removeFromDB($folderID, $kolabID) {
        db_query_params('DELETE FROM items
            WHERE folder_id=$1 AND kolab_id=$2',
          array($folderID, $kolabID));
    }

    /* +++ DeleteMessage, MoveMessage (to Trash) +++ */

    public function deleteItem($id, $kind) {
        if (!($kolabId = syncphony::getKolabIDFromSyncphonyID($id)))
            return true;
        $folderId = $kolabId['folder_id'];
        $kolabId = $kolabId['id'];
        $fn = 'remove' . $kind;
        if (is_soap_fault($this->soapcall($fn, array(
            $fn => array(
                'folder' => array(
                    'name' => $folderId,
                  ),
                'deviceId' => $this->_deviceId,
                'id' => $kolabId,
                'lastUpdateTime' => $this->getLastUpdateTime($folderId, $kolabId, $kind),
          )))))
            return false;
        syncphony::removeFromDB($folderId, $kolabId);
        return true;
    }

    /**
     * Gets latest update time for a Kolab item.
     *
     * @param type $folderId
     * @param type $kolabId
     * @param type $kind 'Contact' or 'Event'
     * @return type
     */
    private function getLastUpdateTime($folderId, $kolabId, $kind) {
        if (empty($kolabId))
            return $this->currentTimestring;
        $fn = 'get' . $kind . 'ChangeTimes';
        $res = $this->soapcall($fn, array(
            $fn => array(
                'folder' => array(
                    'name' => $folderId,
          ))));
        if (!is_soap_fault($res) && !empty($res->return))
            foreach (util_mkarray($res->return) as $cur)
                if (!empty($cur->ID) && $cur->ID == $kolabId)
                    return syncphony::reformatDateTime($cur->changeDate);
        return $this->currentTimestring;
    }

    /* +++ Kolab <-> ActiveSync data type conversion helper functions +++ */

    public static function kolabAttendeeStatus($asAttendeeStatus) {
        switch ($asAttendeeStatus) {
        case 0:
            return 'none';
        case 2:
            return 'tentative';
        case 3:
            return 'accepted';
        case 4:
            return 'declined';
        case 5:
            return 'needs-action';
        }
        return 'none';
    }

    public static function asAttendeeStatus($kolabAttendeeStatus) {
        switch ($kolabAttendeeStatus) {
        case 'none':
            return 0;
        case 'tentative':
            return 2;
        case 'accepted':
        case 'delegated':
            return 3;
        case 'declined':
            return 4;
        case 'needs-action':
            return 5;
        }
        return false;
    }

    public static function kolabAttendeeType($asAttendeeType) {
        switch ($asAttendeeType) {
        case 1:
            return 'required';
        case 2:
            return 'optional';
        case 3:
            return 'resource';
        }
        return 'optional';
    }

    public static function asAttendeeType($kolabAttendeeType) {
        switch ($kolabAttendeeType) {
        case 'required':
        case 'req-participant':
            return 1;
        case 'optional':
            return 2;
        case 'resource':
            return 3;
        }
        return false;
    }

    public static function kolabBusyStatus($asBusyStatus) {
        switch ($asBusyStatus) {
        case 0:
            return 'free';
        case 1:
            return 'tentative';
        case 2:
            return 'busy';
        case 3:
        case 4:
            return 'outofoffice';
        }
        return 'busy';
    }

    public static function asBusyStatus($kolabBusyStatus) {
        switch ($kolabBusyStatus) {
        case 'free':
            return 0;
        case 'tentative':
            return 1;
        case 'busy':
            return 2;
        case 'outofoffice':
            return 3;
        }
        return 2;
    }

    public static function kolabSensitivity($asSensitivity) {
        switch ($asSensitivity) {
        case 0:
            return 'public';
        case 1:
        case 2:
            return 'private';
        case 3:
        default:
            return 'confidential';
        }
    }

    public static function asSensitivity($kolabSensitivity) {
        switch (strtolower($kolabSensitivity)) {
        case 'public':
            return 0;
        case 'private':
            return 2;
        case 'confidential':
        default:
            return 3;
        }
    }

    /* +++ helper function to convert Rich Text bodies +++ */

    private static function getPlainTextFromRTF($rtf) {
        $rtfObject = new z_RTF();
        $rtfObject->loadrtf(base64_decode($rtf));
        $rtfObject->output('ascii');
        $rtfObject->parse();
        $res = utf8_encode($rtfObject->out);
        /* return original when decoding fails, just in case */
        return ($res ? $res : $rtf);
    }

    /* +++ helper function to fetch an attachment from kolab-ws +++ */

    /**
     * fetches an attachment over the syncphony webservice
     * @param String $folder
     * @param String $kolabID
     * @param String $attachmentID
     * @return type
     */
    private function getObjectAttachment($folder, $kolabID, $attachmentID=NULL) {
        if (!isset($folder) || !isset($kolabID))
            return null;
        debugLog('fetching attachments of current object');
        $param = array(
            'getAttachment' => array(
                'folder' => array(
                    'name' => $folder,
                  ),
                'id' => $kolabID,
                'attachmentId' => $attachmentID,
          ));
        $result = $this->soapcall('getAttachment', $param);
        if (is_soap_fault($result) || !isset($result->return) ||
          empty($result->return->content))
            return false;
        return array(
            'data' => base64_encode($result->return->content),
            'type' => $result->return->contentType,
          );
    }

    /* +++ helper functions to merge data when creating/updating items +++ */

    public static function mergeAddresses($oldKolabAddresses, $changedMobileContact, $updateContact=false) {
        $addresses = array();

        // home address
        if (isset($changedMobileContact->homecity)) {
            foreach ($oldKolabAddresses as $key => $currentAddress) {
                if ($currentAddress->type == 'home') {
                    unset($oldKolabAddresses[$key]);
                    break;
                }
            }

            $newAddress = new stdClass();
            $newAddress->type = 'home';
            $newAddress->street = $changedMobileContact->homestreet;
            $newAddress->locality = $changedMobileContact->homecity;
            $newAddress->region = $changedMobileContact->homestate;
            $newAddress->postalCode = $changedMobileContact->homepostalcode;
            $newAddress->country = $changedMobileContact->homecountry;
            $addresses[] = $newAddress;
        }

        // work address
        if (isset($changedMobileContact->businesscity)) {
            foreach ($oldKolabAddresses as $key => $currentAddress) {
                if ($currentAddress->type == 'business') {
                    unset($oldKolabAddresses[$key]);
                    break;
                }
            }

            $newAddress = new stdClass();
            $newAddress->type = 'business';
            $newAddress->street = $changedMobileContact->businessstreet;
            $newAddress->locality = $changedMobileContact->businesscity;
            $newAddress->region = $changedMobileContact->businessstate;
            $newAddress->postalCode = $changedMobileContact->businesspostalcode;
            $newAddress->country = $changedMobileContact->businesscountry;
            $addresses[] = $newAddress;
        }
        // other address
        if (isset($changedMobileContact->othercity)) {
            if (count($oldKolabAddresses) > 0) {
                reset($oldKolabAddresses);
                $firstKey = key($oldKolabAddresses);
                $addressType = $oldKolabAddresses[$firstKey]->type;
                unset($oldKolabAddresses[$firstKey]);
            }
            $newAddress = new stdClass();
            $newAddress->type = 'other'; //$addressType;
            $newAddress->street = $changedMobileContact->otherstreet;
            $newAddress->locality = $changedMobileContact->othercity;
            $newAddress->region = $changedMobileContact->otherstate;
            $newAddress->postalCode = $changedMobileContact->otherpostalcode;
            $newAddress->country = $changedMobileContact->othercountry;
            $addresses[] = $newAddress;
        } elseif ($updateContact) {
            if (count($oldKolabAddresses) > 0) {
                array_shift($oldKolabAddresses);
            }
        }

        if ($oldKolabAddresses != null) {
            foreach ($oldKolabAddresses as $key => $currentAddress) {
                $newAddress = new stdClass();
                $newAddress->type = $currentAddress->type;
                $newAddress->street = $currentAddress->street;
                $newAddress->locality = $currentAddress->locality;
                $newAddress->region = $currentAddress->region;
                $newAddress->postalCode = $currentAddress->postalCode;
                $newAddress->country = $currentAddress->country;
                $addresses[] = $newAddress;
            }
        }
        return $addresses;
    }

    private static function getFullName($fullname, $first, $last, $fallback) {
        if (($name = trim($fullname)))
            return $name;
        if (($name = trim($first) . ' ' . trim($last)))
            return $name;
        return trim($fallback);
    }

    private static function fixupFullName($soap) {
        return syncphony::getFullName(util_ifsetor($soap['fullName'], ''),
          util_ifsetor($soap['givenName'], ''),
          util_ifsetor($soap['lastName'], ''),
          util_ifsetor($soap['organization'], ''));
    }

    public static function mergeEMailAddresses($oldKolabEmailAddresses, $changedMobileContact) {
        // return array
        $eMailAddresses = array();
        $emailCount = 0;
        $noChangesCount = 0;
        // eMail Address 1
        if (isset($changedMobileContact->email1address)) {
            $emailCount++;
            if (isset($oldKolabEmailAddresses[$changedMobileContact->email1address])) {
                $noChangesCount++;
                $name = $oldKolabEmailAddresses[$changedMobileContact->email1address];
                unset($oldKolabEmailAddresses[$changedMobileContact->email1address]);
            }
            $newEMail = new stdClass();
            $newEMail->displayName = syncphony::getFullName(util_ifsetor($name, ''),
              util_ifsetor($changedMobileContact->lastname, ''),
              util_ifsetor($changedMobileContact->lastname, ''), '');
            unset($name);
            $newEMail->smtpAddress = $changedMobileContact->email1address;
            $eMailAddresses[] = $newEMail;
        }
        // eMail Address 2
        if (isset($changedMobileContact->email2address)) {
            $emailCount++;
            if (isset($oldKolabEmailAddresses[$changedMobileContact->email2address])) {
                $noChangesCount++;
                $name = $oldKolabEmailAddresses[$changedMobileContact->email2address];
                unset($oldKolabEmailAddresses[$changedMobileContact->email2address]);
            }
            $newEMail = new stdClass();
            $newEMail->displayName = syncphony::getFullName(util_ifsetor($name, ''),
              util_ifsetor($changedMobileContact->lastname, ''),
              util_ifsetor($changedMobileContact->lastname, ''), '');
            unset($name);
            $newEMail->smtpAddress = $changedMobileContact->email2address;
            $eMailAddresses[] = $newEMail;
        }
        // eMail Address 3
        if (isset($changedMobileContact->email3address)) {
            $emailCount++;
            if (isset($oldKolabEmailAddresses[$changedMobileContact->email3address])) {
                $noChangesCount++;
                $name = $oldKolabEmailAddresses[$changedMobileContact->email3address];
                unset($oldKolabEmailAddresses[$changedMobileContact->email3address]);
            }
            $newEMail = new stdClass();
            $newEMail->displayName = syncphony::getFullName(util_ifsetor($name, ''),
              util_ifsetor($changedMobileContact->lastname, ''),
              util_ifsetor($changedMobileContact->lastname, ''), '');
            unset($name);
            $newEMail->smtpAddress = $changedMobileContact->email3address;
            $eMailAddresses[] = $newEMail;
        }

        if ($oldKolabEmailAddresses != null) {
            foreach ($oldKolabEmailAddresses as $currentEMailAddress => $currentDisplayName) {
                $newEMail = new stdClass();
                $newEMail->displayName = $currentDisplayName;
                $newEMail->smtpAddress = $currentEMailAddress;
                $eMailAddresses[] = $newEMail;
            }
        }
        return $eMailAddresses;
    }

    public static function mergePhoneNumbers($oldKolabPhoneNumbers, $changedMobileContact) {
        // return array
        $phoneNumbers = array();
        // car
        if (isset($changedMobileContact->carphonenumber)) {
            if (isset($oldKolabPhoneNumbers[$changedMobileContact->carphonenumber]) && $oldKolabPhoneNumbers[$changedMobileContact->carphonenumber] = 'car') {
                unset($oldKolabPhoneNumbers[$changedMobileContact->carphonenumber]);
            }
            $newPhone = new stdClass();
            $newPhone->type = 'car';
            $newPhone->number = $changedMobileContact->carphonenumber;
            $phoneNumbers[] = $newPhone;
        }
        // business + fax
        if (isset($changedMobileContact->businessphonenumber)) {
            if (isset($oldKolabPhoneNumbers[$changedMobileContact->businessphonenumber]) && $oldKolabPhoneNumbers[$changedMobileContact->businessphonenumber] = 'business1') {
                unset($oldKolabPhoneNumbers[$changedMobileContact->businessphonenumber]);
            }
            $newPhone = new stdClass();
            $newPhone->type = 'business1';
            $newPhone->number = $changedMobileContact->businessphonenumber;
            $phoneNumbers[] = $newPhone;
        }
        if (isset($changedMobileContact->business2phonenumber)) {
            if (isset($oldKolabPhoneNumbers[$changedMobileContact->business2phonenumber]) && $oldKolabPhoneNumbers[$changedMobileContact->business2phonenumber] = 'business1') {

                unset($oldKolabPhoneNumbers[$changedMobileContact->business2phonenumber]);
            }
            $newPhone = new stdClass();
            $newPhone->type = 'business1';
            $newPhone->number = $changedMobileContact->business2phonenumber;
            $phoneNumbers[] = $newPhone;
        }
        if (isset($changedMobileContact->businessfaxnumber)) {
            if (isset($oldKolabPhoneNumbers[$changedMobileContact->businessfaxnumber]) && $oldKolabPhoneNumbers[$changedMobileContact->businessfaxnumber] = 'businessfax') {
                unset($oldKolabPhoneNumbers[$changedMobileContact->businessfaxnumber]);
            }
            $newPhone = new stdClass();
            $newPhone->type = 'businessfax';
            $newPhone->number = $changedMobileContact->businessfaxnumber;
            $phoneNumbers[] = $newPhone;
        }
        // home + fax
        if (isset($changedMobileContact->homephonenumber)) {
            if (isset($oldKolabPhoneNumbers[$changedMobileContact->homephonenumber]) && $oldKolabPhoneNumbers[$changedMobileContact->homephonenumber] = 'home1') {
                unset($oldKolabPhoneNumbers[$changedMobileContact->homephonenumber]);
            }
            $newPhone = new stdClass();
            $newPhone->type = 'home1';
            $newPhone->number = $changedMobileContact->homephonenumber;
            $phoneNumbers[] = $newPhone;
        }
        if (isset($changedMobileContact->home2phonenumber)) {
            if (isset($oldKolabPhoneNumbers[$changedMobileContact->home2phonenumber]) && $oldKolabPhoneNumbers[$changedMobileContact->home2phonenumber] = 'home1') {
                unset($oldKolabPhoneNumbers[$changedMobileContact->home2phonenumber]);
            }
            $newPhone = new stdClass();
            $newPhone->type = 'home1';
            $newPhone->number = $changedMobileContact->home2phonenumber;
            $phoneNumbers[] = $newPhone;
        }
        if (isset($changedMobileContact->homefaxnumber)) {
            if (isset($oldKolabPhoneNumbers[$changedMobileContact->homefaxnumber]) && $oldKolabPhoneNumbers[$changedMobileContact->homefaxnumber] = 'homefax') {
                unset($oldKolabPhoneNumbers[$changedMobileContact->homefaxnumber]);
            }
            $newPhone = new stdClass();
            $newPhone->type = 'homefax';
            $newPhone->number = $changedMobileContact->homefaxnumber;
            $phoneNumbers[] = $newPhone;
        }
        // mobile
        if (isset($changedMobileContact->mobilephonenumber)) {
            if (isset($oldKolabPhoneNumbers[$changedMobileContact->mobilephonenumber]) && $oldKolabPhoneNumbers[$changedMobileContact->mobilephonenumber] = 'mobile') {
                unset($oldKolabPhoneNumbers[$changedMobileContact->mobilephonenumber]);
            }
            $newPhone = new stdClass();
            $newPhone->type = 'mobile';
            $newPhone->number = $changedMobileContact->mobilephonenumber;
            $phoneNumbers[] = $newPhone;
        }
        // pager
        if (isset($changedMobileContact->pagernumber)) {
            if (isset($oldKolabPhoneNumbers[$changedMobileContact->pagernumber]) && $oldKolabPhoneNumbers[$changedMobileContact->pagernumber] = 'pager') {
                unset($oldKolabPhoneNumbers[$changedMobileContact->pagernumber]);
            }
            $newPhone = new stdClass();
            $newPhone->type = 'pager';
            $newPhone->number = $changedMobileContact->pagernumber;
            $phoneNumbers[] = $newPhone;
        }
        // radio
        if (isset($changedMobileContact->radiophonenumber)) {
            if (isset($oldKolabPhoneNumbers[$changedMobileContact->radiophonenumber]) && $oldKolabPhoneNumbers[$changedMobileContact->radiophonenumber] = 'radio') {
                unset($oldKolabPhoneNumbers[$changedMobileContact->radiophonenumber]);
            }
            $newPhone = new stdClass();
            $newPhone->type = 'radio';
            $newPhone->number = $changedMobileContact->radiophonenumber;
            $phoneNumbers[] = $newPhone;
        }

        // List of phone number types, that should already have been handled
        $phoneNumberTypes = array('car', 'business1', 'businessfax', 'home1', 'homefax', 'mobile', 'pager', 'radio');
        foreach ($oldKolabPhoneNumbers as $currentNumber => $currentType) {
            if (in_array($currentType, $phoneNumberTypes)) {
                unset($oldKolabPhoneNumbers[$currentNumber]);
            } else {
                $newPhone = new stdClass();
                $newPhone->type = $currentType;
                $newPhone->number = $currentNumber;
                $phoneNumbers[] = $newPhone;
                unset($oldKolabPhoneNumbers[$currentNumber]);
            }
        }
        return $phoneNumbers;
    }

    /* +++ GetMessage +++ */

    public function getSyncItem($kind, $syncphonyID) {
        if (!($kolabId = syncphony::getKolabIDFromSyncphonyID($syncphonyID)))
            return false;
        $folderId = $kolabId['folder_id'];
        $kolabId = $kolabId['id'];

        util_debugJ('fetching ' . $kind . ' from WS', array(
            'folderId' => $folderId,
            'kolabId' => $kolabId,
          ));

        if (!$folderId || !$kolabId || empty($soapItem =
          $this->getItem($kind, $folderId, $kolabId)))
            return false;

        switch ($kind) {
        case 'Contact':
            return $this->getSyncContact($soapItem, $folderId);
        case 'Event':
            return $this->getSyncAppointment($soapItem);
        }
        util_debugJ(true, 'invalid kind, cannot happen!');
        return false;
    }

    /**
     * getSyncAppointment - generates a new SyncAppointment object from a SOAP WS
     * result
     * @param <array> $appointmentSOAP
     * @return SyncAppointment
     */
    private function getSyncAppointment($appointmentSOAP) {
        global $tz_sk;

        $appointment = new SyncAppointment();
        $appointment->uid = util_ifsetor($appointmentSOAP->uid, '');
        // The time at which this calendar item was created or modified
        if (isset($appointmentSOAP->lastModificationDate))
            $appointment->dtstamp = syncphony::reformatTimestamp($appointmentSOAP->lastModificationDate);
        else
            $appointment->dtstamp = $this->currentTimestamp;
        $tstart = syncphony::parseDateTime($appointmentSOAP->startDate);
        $tend = syncphony::parseDateTime($appointmentSOAP->endDate);
        if (!empty($appointmentSOAP->allDay)) {
            // [MS-ASCAL] 2.2.2.1, 2.2.2.41; [MS-ASDTYPE] 2.6.5
            // a Compact DateTime represents UTC time (MS-ASDTYPE)
            // an AllDayEvent is “midnight to midnight”, local time
            /* See also: updateSoapEventBody() */
            $appointment->alldayevent = 1;
            $appointment->starttime = syncphony::renderDatestamp($tstart);
            $appointment->endtime = syncphony::renderDatestamp($tend, 1);
        } else {
            $appointment->alldayevent = 0;
            $appointment->starttime = syncphony::renderTimestamp($tstart);
            $appointment->endtime = syncphony::renderTimestamp($tend);
        }
        $appointment->timezone = tz2as($tz_sk, $appointment->starttime);

        if (isset($appointmentSOAP->alarm) && $appointmentSOAP->alarm > 0)
            $appointment->reminder = $appointmentSOAP->alarm;

        syncphony::bodyFromKolab($appointmentSOAP, $appointment);

        $appointment->subject = util_ifsetor($appointmentSOAP->summary, '');
        $appointment->location = util_ifsetor($appointmentSOAP->location, '');
        $appointment->busystatus = syncphony::asBusyStatus($appointmentSOAP->showTimeAs);
        $appointment->sensitivity = syncphony::asSensitivity($appointmentSOAP->sensitivity);
        $appointment->bodytruncated = 0;
        $appointment->organizeremail = trim(util_ifsetor($appointmentSOAP->organizerSmtpAddress, ''));
        $appointment->organizername = util_ifsetor($appointmentSOAP->organizerDisplayName, '');

        // reccurence process
        if (isset($appointmentSOAP->recurrence)) {
            $wd = util_ifsetor($appointmentSOAP->recurrence->weekDays, 0);
            $wd = (($wd & 0x3F) << 1) | (($wd & 0x40) >> 6);
            $rec = new SyncRecurrence();
            $rec->type = -1;
            switch ($appointmentSOAP->recurrence->cycle) {
            case 'daily':
                $rec->type = 0;
                if ($wd)
                    $rec->dayofweek = $wd;
                break;
            case 'weekly':
                $rec->type = 1;
                if ($wd)
                    $rec->dayofweek = $wd;
                break;
            case 'monthly':
                if (isset($appointmentSOAP->recurrence->monthDay)) {
                    $rec->type = 2;
                    $rec->dayofmonth = $appointmentSOAP->recurrence->monthDay;
                } elseif (isset($appointmentSOAP->recurrence->week) && $wd) {
                    /*XXX special values per [MS-ASCAL] §2.2.2.36.1 not handled in Kolab */
                    $rec->type = 3;
                    $rec->weekofmonth = $appointmentSOAP->recurrence->week;
                    $rec->dayofweek = $wd;
                }
                break;
            case 'yearly':
                if (isset($appointmentSOAP->recurrence->monthDay)) {
                    $rec->type = 5;
                    $rec->dayofmonth = $appointmentSOAP->recurrence->monthDay;
                    $rec->monthofyear = $appointmentSOAP->recurrence->month;
                } elseif (isset($appointmentSOAP->recurrence->yearDay)) {
                    ; /* AS cannot handle this */
                } elseif (isset($appointmentSOAP->recurrence->week)) {
                    $rec->type = 6;
                    $rec->weekofmonth = $appointmentSOAP->recurrence->week;
                    if ($wd)
                        $rec->dayofweek = $wd;
                    $rec->monthofyear = $appointmentSOAP->recurrence->month;
                }
                break;
            }
            $rec->interval = util_ifsetor($appointmentSOAP->recurrence->interval);
            $rec->interval = $rec->interval ? $rec->interval : 1;
            if ($rec->interval < 1 || $rec->interval > 999)
                $rec->type = -1;
            if (util_ifsetor($appointmentSOAP->recurrence->rangeNumber))
                $rec->occurrences = $appointmentSOAP->recurrence->rangeNumber;
            elseif (isset($appointmentSOAP->recurrence->rangeDate)) {
                $runtil = syncphony::parseDateUntil($appointmentSOAP->recurrence->rangeDate);
                $rec->until = syncphony::renderTimestamp($runtil);
            }
            if ($rec->type == -1)
                util_debugJ(true, 'cannot parse recurrence for event UID ' . $appointment->uid);
            else
                $appointment->recurrence = $rec;
            if (isset($appointmentSOAP->recurrence->exclusion)) {
                $appointment->exceptions = array();
                foreach (util_mkarray($appointmentSOAP->recurrence->exclusion) as $currentExclusion) {
                    $newExclusion = new SyncAppointmentException();
                    $newExclusion->deleted = 1;
                    $newExclusion->alldayevent = $appointment->alldayevent;
                    /*XXX does this need (more) special handling for all-day events? */
                    $newExclusion->exceptionstarttime = syncphony::reformatTimestamp($currentExclusion);
                    array_push($appointment->exceptions, $newExclusion);
                }
            }
        }

        $resprsvp = true;
        $resptype = 0;
        if (isset($appointmentSOAP->attendee)) {
            $appointment->attendees = array();
            foreach (util_mkarray($appointmentSOAP->attendee) as $currentAttendee) {
                if (!($e = trim(util_ifsetor($currentAttendee->smtpAddress, ''))))
                    continue;
                $newAttendee = new SyncAttendee();
                $newAttendee->name = util_ifsetor($currentAttendee->displayName, '') ? :
                  preg_replace('/@.*$/', '', $e);
                $newAttendee->email = $e;
                if ($this->asv >= 12.0 && isset($currentAttendee->status))
                    if (($newAttendee->attendeestatus = syncphony::asAttendeeStatus($currentAttendee->status)) === false)
                        unset($newAttendee->attendeestatus);
                if ($this->asv >= 12.0 && isset($currentAttendee->role))
                    if (($newAttendee->attendeetype = syncphony::asAttendeeType($currentAttendee->role)) === false)
                        unset($newAttendee->attendeetype);
                if (isset($currentAttendee->status) &&
                  util_emailcase($e) == util_emailcase($this->user)) {
                    $resptype = syncphony::asAttendeeStatus($currentAttendee->status);
                    if (isset($currentAttendee->requestResponse) &&
                      (!$currentAttendee->requestResponse ||
                      $currentAttendee->requestResponse == 'false'))
                        $resprsvp = false;
                }
                array_push($appointment->attendees, $newAttendee);
            }
        }

        if ($appointment->organizeremail &&
          util_emailcase($appointment->organizeremail) == util_emailcase($this->user)) {
            $resprsvp = false;
            $resptype = 1;
            $appointment->meetingstatus = 3;
        } else
            $appointment->meetingstatus = 1;

        if ($this->asv >= 14.0) {
            $appointment->responserequested = $resprsvp;
            $appointment->responsetype = $resptype ? : 0;
        }

        if (isset($appointmentSOAP->categories)) {
            $appointment->categories = array();
            if (strpos($appointmentSOAP->categories, ',') !== false)
                array_push($appointment->categories, trim($appointmentSOAP->categories));
            else
                foreach (explode(',', $appointmentSOAP->categories) as $currentCategory)
                    array_push($appointment->categories, trim($currentCategory));
        }
        return $appointment;
    }

    /**
     * getSyncContact - generates a new SyncContact object from a SOAP WS result.
     * @param <array> $contactSOAP
     * @return SyncContact
     */
    private function getSyncContact($contactSOAP, $kolabFolderID) {
        $contact = new SyncContact();
        $contact->uid = util_ifsetor($contactSOAP->uid, '');
        $contact->fileas = syncphony::fixupFullName((array)$contactSOAP);
        $contact->firstname = util_ifsetor($contactSOAP->givenName, '');
        $contact->lastname = util_ifsetor($contactSOAP->lastName, '');
        $contact->middlename = util_ifsetor($contactSOAP->middleNames, '');
        $contact->webpage = util_ifsetor($contactSOAP->webPage, '');
        $contact->jobtitle = util_ifsetor($contactSOAP->jobTitle, '');
        $contact->title = util_ifsetor($contactSOAP->prefix, '');
        $contact->suffix = util_ifsetor($contactSOAP->suffix, '');
        $contact->companyname = util_ifsetor($contactSOAP->organization, '');
        $contact->managername = util_ifsetor($contactSOAP->managerName, '');
        $contact->imaddress = util_ifsetor($contactSOAP->imAddress, '');
        $contact->anniversary = isset($contactSOAP->anniversary) ? syncphony::reformatDatestamp($contactSOAP->anniversary) : '';
        $contact->birthday = isset($contactSOAP->birthday) ? syncphony::reformatDatestamp($contactSOAP->birthday) : '';

        if (isset($contactSOAP->assistant)) {
            if (!(strpos($contactSOAP->assistant, ';') === false)) {
                $assitant = explode(';', $contactSOAP->assistant);
                $contact->assistantname = trim($assitant[0]);
                $contact->assistnamephonenumber = trim($assitant[1]);
            } else {
                $contact->assistantname = trim($contactSOAP->assistant);
            }
        }

        if (isset($contactSOAP->pictureAttachmentId)) {
            $attachment = $this->getObjectAttachment($kolabFolderID, $contactSOAP->uid, $contactSOAP->pictureAttachmentId);
            $contact->picture = $attachment ? $attachment['data'] : '';
        } else {
            $contact->picture = '';
        }

        // children
        if (isset($contactSOAP->children)) {
            $contact->children = array();
            foreach (util_mkarray($contactSOAP->children) as $currentChild) {
                array_push($contact->children, $currentChild);
            }
        }
        // categories
        if (isset($contactSOAP->categories)) {
            $contact->categories = array();
            if (strpos($contactSOAP->categories, ',') === false)
                array_push($contact->categories, trim($contactSOAP->categories));
            else
                foreach (explode(',', $contactSOAP->categories) as $currentCategory)
                    array_push($contact->categories, trim($currentCategory));
        }

        // email
        if (isset($contactSOAP->email)) {
            $mailAddresses = array();
            foreach (util_mkarray($contactSOAP->email) as $mailObject) {
                $mailAddresses[] = $mailObject->smtpAddress;
            }
            $contact->email1address = $mailAddresses[0];
            if (isset($mailAddresses[1]))
                $contact->email2address = $mailAddresses[1];
            if (isset($mailAddresses[2]))
                $contact->email3address = $mailAddresses[2];
        }

        // addresses
        if (isset($contactSOAP->address)) {
            $hasBusinessAddress = false;
            $hasHomeAddress = false;
            $hasOtherAddress = false;
            foreach (util_mkarray($contactSOAP->address) as $addressObject) {
                switch ($addressObject->type) {
                case 'business':
                    if (!$hasBusinessAddress) {
                        $contact->businessstreet = util_ifsetor($addressObject->street, '');
                        $contact->businesscity = util_ifsetor($addressObject->locality, '');
                        $contact->businessstate = util_ifsetor($addressObject->region, '');
                        $contact->businesspostalcode = util_ifsetor($addressObject->postalCode, '');
                        $contact->businesscountry = util_ifsetor($addressObject->country, '');
                        $hasBusinessAddress = true;
                    } else if (!$hasOtherAddress) {
                        $contact->otherstreet = util_ifsetor($addressObject->street, '');
                        $contact->othercity = util_ifsetor($addressObject->locality, '');
                        $contact->otherstate = util_ifsetor($addressObject->region, '');
                        $contact->otherpostalcode = util_ifsetor($addressObject->postalCode, '');
                        $contact->othercountry = util_ifsetor($addressObject->country, '');
                        $hasOtherAddress = true;
                    }
                    break;
                case 'home':
                    if (!$hasHomeAddress) {
                        $contact->homestreet = util_ifsetor($addressObject->street, '');
                        $contact->homecity = util_ifsetor($addressObject->locality, '');
                        $contact->homestate = util_ifsetor($addressObject->region, '');
                        $contact->homepostalcode = util_ifsetor($addressObject->postalCode, '');
                        $contact->homecountry = util_ifsetor($addressObject->country, '');
                        $hasHomeAddress = true;
                    } else if (!$hasOtherAddress) {
                        $contact->otherstreet = util_ifsetor($addressObject->street, '');
                        $contact->othercity = util_ifsetor($addressObject->locality, '');
                        $contact->otherstate = util_ifsetor($addressObject->region, '');
                        $contact->otherpostalcode = util_ifsetor($addressObject->postalCode, '');
                        $contact->othercountry = util_ifsetor($addressObject->country, '');
                        $hasOtherAddress = true;
                    }
                    break;
                case 'other':
                    if (!$hasOtherAddress) {
                        $contact->otherstreet = util_ifsetor($addressObject->street, '');
                        $contact->othercity = util_ifsetor($addressObject->locality, '');
                        $contact->otherstate = util_ifsetor($addressObject->region, '');
                        $contact->otherpostalcode = util_ifsetor($addressObject->postalCode, '');
                        $contact->othercountry = util_ifsetor($addressObject->country, '');
                        $hasOtherAddress = true;
                        break;
                    }
                default:
                    break;
                }
            }
        }

        // phonenumbers
        if (isset($contactSOAP->phone)) {
            $home1 = false;
            $home2 = false;
            $business1 = false;
            $business2 = false;
            foreach (util_mkarray($contactSOAP->phone) as $numberObject) {
                if (!($number = trim(util_ifsetor($numberObject->number, ''))))
                    continue;
                switch ($numberObject->type) {
                case 'business1':
                    if (!$business1) {
                        $contact->businessphonenumber = $number;
                        $business1 = true;
                    } elseif (!$business2) {
                        $contact->business2phonenumber = $number;
                        $business2 = true;
                    }
                    break;
                case 'businessfax':
                    $contact->businessfaxnumber = $number;
                    break;
                case 'home1':
                    if (!$home1) {
                        $contact->homephonenumber = $number;
                        $home1 = true;
                    } elseif (!$home2) {
                        $contact->home2phonenumber = $number;
                        $home2 = true;
                    }
                    break;
                case 'homefax':
                    $contact->homefaxnumber = $number;
                    break;
                case 'mobile':
                    $contact->mobilephonenumber = $number;
                    break;
                case 'pager':
                    $contact->pagernumber = $number;
                    break;
                case 'car':
                    $contact->carphonenumber = $number;
                    break;
                default:
                    break;
                }
            }
        }

        syncphony::bodyFromKolab($contactSOAP, $contact);
        $contact->spouse = util_ifsetor($contactSOAP->spouseName, '');
        $contact->nickname = util_ifsetor($contactSOAP->nickName, '');
        $contact->department = util_ifsetor($contactSOAP->department, '');
        $contact->officelocation = util_ifsetor($contactSOAP->officeLocation, '');

        return $contact;
    }

    /* +++ ChangeMessage (top-level) +++ */

    public function changeItem($kind, $id, $asData, $newData=false) {
        $lckind = strtolower($kind);
        $picture = '';
        if (empty($id)) {
            debugLog('syncing new ' . $kind);
            $newData = $this->getSoapBody($kind, $newData, $asData);
            if (!($folderId = $this->getStandardFolder($kind))) {
                debugLog('could not get standard ' . $kind . ' folder ID');
                return false;
            }
            $fn = 'add' . $kind;
            if (is_soap_fault($res = $this->soapcall($fn, array(
                $fn => array(
                    'folder' => array(
                        'name' => $folderId,
                      ),
                    'deviceId' => $this->_deviceId,
                    $lckind => $newData[$lckind],
              )))) || !isset($res->return))
                return false;
            $kolabId = $res->return;
            if ($kind == 'Contact' && !empty($asData->picture)) {
                debugLog('syncing picture');
                sleep(1);
                $picture = $asData->picture;
            }
        } else {
            debugLog('updating existing ' . $kind);
            if (($kolabId = syncphony::getKolabIDFromSyncphonyID($id))) {
                $folderId = $kolabId['folder_id'];
                $kolabId = $kolabId['id'];
            }
        }
        if (!$kolabId || !$folderId) {
            util_debugJ(true, 'invalid folder or kolab ID', $folderId, $kolabId);
            return false;
        }
        $attachments = array();
        if (!empty($id)) {
            /* fetch existing item */
            if (!($soapData = $this->getItem($kind, $folderId, $kolabId)))
                return false;
            /* preprocess; prepare new data */
            if ($kind == 'Contact') {
                /* fetch picture if it will not be updated */
                if (empty($asData->picture) &&
                  isset($soapData->pictureAttachmentId) &&
                  ($picture = $this->getObjectAttachment($folderId, $kolabId,
                  $soapData->pictureAttachmentId)))
                    $picture = $picture['data'];
                else
                    $picture = '';
            } else {
                /* keep existing attachments unless prepared body */
                if ($newData === false && isset($soapData->inlineAttachment))
                    foreach (util_mkarray($soapEvent->inlineAttachment) as $currentAttachment)
                        if (($att = $this->getObjectAttachment($folderId, $kolabId, $currentAttachment)))
                            $attachments[$currentAttachment] = $att;
            }
            /* modify existing item with new data */
            $newData = $this->getSoapBody($kind, $newData, $asData, $soapData, $kolabId);
            /* update if there is any change */
            if ($newData != NULL) {
                $fn = 'update' . $kind;
                if (is_soap_fault(($res = $this->soapcall($fn, array(
                    $fn => array(
                        'folder' => array(
                            'name' => $folderId,
                          ),
                        'deviceId' => $this->_deviceId,
                        $lckind => $newData[$lckind],
                        'lastUpdateTime' => $this->getLastUpdateTime($folderId, $kolabId, $kind),
                  ))))))
                    return false;
                if (empty($res->return))
                    $attachments = array();
                elseif ($kind == 'Contact' && !empty($asData->picture))
                    $picture = $asData->picture;
            } else
                $attachments = array();
            /* item updated successfully, or update not needed */
        }
        /* handle file attachments */
        if ($picture)
            $attachments['foto.anhang'] = array(
                'data' => $picture,
                'type' => 'image/png',
              );
        if ($attachments)
            $lastUpdateTime = $this->getLastUpdateTime($folderId, $kolabId, $kind);
        foreach ($attachments as $attachmentId => $attachmentData)
            $this->setAttachment($folderId, $kolabId, $attachmentId, $lastUpdateTime, array(
                'content' => base64_decode($attachmentData['data']),
                'contentType' => $attachmentData['type'],
              ));
        /* fetch and stat new item */
        if (!($soapItem = $this->getItem($kind, $folderId, $kolabId)))
            return false;
        return syncphony::getStatsFromSyncphonyID(syncphony::getIDFromDB($folderId,
          $soapItem->uid, $soapItem->lastModificationDate));
    }

    private function getSoapBody($kind, $newData, $asData, $soapData=NULL, $kolabId=NULL) {
        if ($newData !== false)
            return $newData;
        if ($kind == 'Contact')
            return $this->updateSoapContactBody($asData, $soapData, $kolabId);
        else
            return $this->updateSoapEventBody($asData, $soapData, $kolabId);
    }

    /* +++ ChangeMessage (Event) +++ */

    private function updateSoapEventBody($mobileEvent, $soapEvent=NULL, $kolabId=NULL) {
        if (empty($soapEvent)) {
            $soapBody = array('event' => array());
            if (isset($mobileEvent->dtstamp)) {
                $ts = syncphony::timetDateTime($mobileEvent->dtstamp);
                $soapBody['event']['creationDate'] = $ts;
                $soapBody['event']['lastModificationDate'] = $ts;
            }
            if (!empty($mobileEvent->uid) && strlen($mobileEvent->uid) <= 300 &&
              !preg_match('/[^0-9a-fA-F_-]/', $mobileEvent->uid))
                $kolabId = $mobileEvent->uid;
        } else {
            $soapBody = array('event' => (array)$soapEvent);
            $soapBody['event']['creationDate'] = syncphony::reformatDateTime($soapBody['event']['creationDate']);
            $dKolab = syncphony::parseDateTime($soapBody['event']['lastModificationDate']);
            $soapBody['event']['lastModificationDate'] = syncphony::renderDateTime($dKolab);
            if ($mobileEvent->dtstamp < $dKolab->getTimestamp()) {
                debugLog('No need to update element, server element is newer.');
                return NULL;
            }
        }

        // update elements from mobile device

        if (!empty($kolabId))
            $soapBody['event']['uid'] = $kolabId;

        syncphony::bodyToKolab($mobileEvent, $soapBody['event']);

        $soapBody['event']['sensitivity'] = syncphony::kolabSensitivity(util_ifsetor($mobileEvent->sensitivity, false));

        if (isset($mobileEvent->subject)) {
            $soapBody['event']['summary'] = $mobileEvent->subject;
        } else {
            unset($soapBody['event']['summary']);
        }

        if (isset($mobileEvent->location)) {
            $soapBody['event']['location'] = $mobileEvent->location;
        } else {
            unset($soapBody['event']['location']);
        }

        if (isset($mobileEvent->organizername)) {
            $soapBody['event']['organizerDisplayName'] = $mobileEvent->organizername;
        } else {
            unset($soapBody['event']['organizerDisplayName']);
        }

        if (isset($mobileEvent->organizeremail)) {
            $soapBody['event']['organizerSmtpAddress'] = $mobileEvent->organizeremail;
        } else {
            $soapBody['event']['organizerSmtpAddress'] = $this->user;
        }

        /**
         * A primer on event date representations in SimKolab
         *
         * All-day events look like this in KolabXML:
         *  <start-date>2015-11-16</start-date>
         *  <end-date>2015-11-16</end-date>
         * This is what kolab-ws makes out of that:
         *  "startDate": "2015-11-16T00:00:00Z"
         *  "endDate": "2015-11-16T00:00:00Z"
         *  "allDay": true
         * Anniversaries look the same.
         *
         * On the ActiveSync WBXML side, this becomes:
         *  <POOMCAL:StartTime>20151129T230000Z</POOMCAL:StartTime>
         *  <POOMCAL:EndTime>20151130T230000Z</POOMCAL:EndTime>
         *  <POOMCAL:AllDayEvent>1</POOMCAL:AllDayEvent>
         * and:
         *  <POOMCONTACTS:Anniversary>1973-01-31T23:00:00.000Z</POOMCONTACTS:Anniversary>
         * Note how all-day events are always midnight local time,
         * but all timestamps transferred are always UTC.
         *
         * The streamer converts those all into time_t timestamps
         * and expects to be handed time_t to pass to the mobile.
         * See also: getSyncAppointment()
         */

        /* [MS-ASCAL] 3.2.4.4: what to do if not both start/end time are given */
        if (empty($mobileEvent->starttime) && empty($mobileEvent->endtime)) {
            /* neither are set */
            $tstart = syncphony::roundedNow();
            $tend = $tstart + 1800;
        } elseif (!empty($mobileEvent->starttime)) {
            /* start time is set */
            $tstart = $mobileEvent->starttime;
            if (!empty($mobileEvent->endtime)) {
                /* end time is set, brav */
                $tend = $mobileEvent->endtime;
            } elseif ($tstart < time()) {
                $tend = syncphony::roundedNow() + 1800;
            } else
                throw new StatusException(sprintf('start time %d > current time %d and end time not set',
                  $tstart, time()), SYNC_STATUS_CLIENTSERVERCONVERSATIONERROR);
        } else {
            /* start time is not set, end time is set */
            $tend = $mobileEvent->endtime;
            if ($tend < time())
                throw new StatusException(sprintf('end time %d < current time %d and start time not set',
                  $tend, time()), SYNC_STATUS_CLIENTSERVERCONVERSATIONERROR);
            $tstart = syncphony::roundedNow();
            if ($tstart > $tend)
                throw new StatusException(sprintf('rounded current time %d > end time %d',
                  $tstart, $tend), SYNC_STATUS_CLIENTSERVERCONVERSATIONERROR);
        }

        if (!empty($mobileEvent->alldayevent) &&
          /* [MS-ASCAL] 2.2.2.1: if start/end time not midnight, ignore */
          syncphony::timetIsMidnight($tstart) &&
          syncphony::timetIsMidnight($tend)) {
            $soapBody['event']['allDay'] = 1;
            $soapBody['event']['startDate'] = syncphony::timetDateTrunc($tstart);
            $soapBody['event']['endDate'] = syncphony::timetDateTrunc($tend, -1);
        } else {
            $soapBody['event']['allDay'] = 0;
            $soapBody['event']['startDate'] = syncphony::timetDateTime($tstart);
            $soapBody['event']['endDate'] = syncphony::timetDateTime($tend);
        }

        if (isset($mobileEvent->reminder)) {
            $soapBody['event']['alarm'] = $mobileEvent->reminder;
        } else {
            unset($soapBody['event']['alarm']);
        }

        // recurrence
        if (isset($mobileEvent->recurrence)) {
            $exceptions = array();
            if (isset($soapBody['event']['recurrence']->exclusion)) {
                $exceptions = $soapBody['event']['recurrence']->exclusion;
            }
            $wd = util_ifsetor($mobileEvent->recurrence->dayofweek, 0);
            $wd = (($wd & 0x7E) >> 1) | (($wd & 1) << 6);
            $soapBody['event']['recurrence'] = array();
            switch ($mobileEvent->recurrence->type) {
            case 0:
                $soapBody['event']['recurrence']['cycle'] = 'daily';
                if ($wd)
                    $soapBody['event']['recurrence']['weekDays'] = $wd;
                break;
            case 1:
                $soapBody['event']['recurrence']['cycle'] = 'weekly';
                $soapBody['event']['recurrence']['weekDays'] = $wd;
                break;
            case 2:
                $soapBody['event']['recurrence']['cycle'] = 'monthly';
                $soapBody['event']['recurrence']['monthDay'] = $mobileEvent->recurrence->dayofmonth;
                break;
            case 3:
                /*XXX special values per [MS-ASCAL] §2.2.2.36.1 not handled in Kolab */
                $soapBody['event']['recurrence']['cycle'] = 'monthly';
                $soapBody['event']['recurrence']['week'] = $mobileEvent->recurrence->weekofmonth;
                $soapBody['event']['recurrence']['weekDays'] = $wd;
                break;
            case 5:
                $soapBody['event']['recurrence']['cycle'] = 'yearly';
                $soapBody['event']['recurrence']['monthDay'] = $mobileEvent->recurrence->dayofmonth;
                $soapBody['event']['recurrence']['month'] = $mobileEvent->recurrence->monthofyear;
                break;
            case 6:
                $soapBody['event']['recurrence']['cycle'] = 'yearly';
                $soapBody['event']['recurrence']['week'] = $mobileEvent->recurrence->weekofmonth;
                $soapBody['event']['recurrence']['weekDays'] = $wd;
                $soapBody['event']['recurrence']['month'] = $mobileEvent->recurrence->monthofyear;
                break;
            }
            $soapBody['event']['recurrence']['interval'] = util_ifsetor($mobileEvent->recurrence->interval, 1);
            if (isset($mobileEvent->recurrence->until))
                $soapBody['event']['recurrence']['rangeDate'] = syncphony::timetDateTrunc($mobileEvent->recurrence->until);
            else
                $soapBody['event']['recurrence']['rangeNumber'] = util_ifsetor($mobileEvent->recurrence->occurrences, 0);
            if (isset($mobileEvent->exceptions)) {
                $exclusions = array();
                foreach ($mobileEvent->exceptions as $currentException) {
                    if (isset($currentException->deleted) && $currentException->deleted == 1 && isset($currentException->exceptionstarttime)) {
                        array_push($exclusions, $currentException->exceptionstarttime);
                    }
                }
                if (count($exclusions) == 1) {
                    $soapBody['event']['recurrence']['exclusion'] = $exclusions[0];
                } elseif (count($exclusions) > 1) {
                    $soapBody['event']['recurrence']['exclusion'] = $exclusions;
                }
            } elseif (isset($exceptions) && count($exceptions) > 0) {
                if (is_array($soapBody['event']['recurrence']))
                    $soapBody['event']['recurrence']['exclusion'] = $exceptions;
                elseif (is_object($soapBody['event']['recurrence']))
                    $soapBody['event']['recurrence']->exclusion = $exceptions;
            }
        } else {
            unset($soapBody['event']['recurrence']);
        }

        // attendees
        if (!isset($mobileEvent->attendees) || !count($mobileEvent->attendees)) {
            unset($soapBody['event']['attendee']);
        } else {
            $old_atts = array();
            if (isset($soapBody['event']['attendee']))
                $old_atts = util_mkarray($soapBody['event']['attendee']);
            $as_atts = util_mkarray($mobileEvent->attendees);
            $soapBody['event']['attendee'] = array();

            foreach ($old_atts as $att) {
                if (empty($att->smtpAddress))
                    continue;
                $found = false;
                foreach ($as_atts as $key => $tmp)
                    if ($tmp->email == $att->smtpAddress) {
                        $found = $key;
                        break;
                    }
                if ($found === false)
                    continue;
                $src = $as_atts[$found];
                $att = (array)$att;
                if (isset($src->name))
                    $att['displayName'] = trim($src->name);
                else
                    unset($att['displayName']);
                if (isset($src->email))
                    $att['smtpAddress'] = trim($src->email);
                else
                    unset($att['smtpAddress']);
                if (isset($src->attendeestatus))
                    $att['status'] = syncphony::kolabAttendeeStatus($src->attendeestatus);
                if (isset($src->attendeetype))
                    $att['role'] = syncphony::kolabAttendeeType($src->attendeetype);
                unset($as_atts[$found]);
                array_push($soapBody['event']['attendee'], $att);
            }

            foreach ($as_atts as $src) {
                $att = array();
                if (isset($src->name))
                    $att['displayName'] = trim($src->name);
                if (isset($src->email))
                    $att['smtpAddress'] = trim($src->email);
                $att['status'] = syncphony::kolabAttendeeStatus(util_ifsetor($src->attendeestatus, false));
                $att['role'] = syncphony::kolabAttendeeType(util_ifsetor($src->attendeetype, false));
                array_push($soapBody['event']['attendee'], $att);
            }
        }

        $soapBody['event']['showTimeAs'] = syncphony::kolabBusyStatus(util_ifsetor($mobileEvent->busystatus, false));

        if (isset($mobileEvent->categories))
            $soapBody['event']['categories'] = implode(',', util_mkarray($mobileEvent->categories));
        else
            unset($soapBody['event']['categories']);

        $soapBody['event']['colorLabel'] = 'none';

        return $soapBody;
    }

    /* +++ ChangeMessage (Contact) +++ */

    private function updateSoapContactBody($mobileContact, $soapContact=NULL, $kolabId=NULL) {
        if (empty($soapContact))
            $soapBody = array('contact' => array());
        else
            $soapBody = array('contact' => (array)$soapContact);

        foreach (array(
            'managername' => 'managerName',
            'companyname' => 'organization',
            'department' => 'department',
            'fileas' => 'fullName',
            'firstname' => 'givenName',
            'imaddress' => 'imAddress',
            'jobtitle' => 'jobTitle',
            'lastname' => 'lastName',
            'middlename' => 'middleNames',
            'officelocation' => 'officeLocation',
            'spouse' => 'spouseName',
            'suffix' => 'suffix',
            'title' => 'prefix',
            'webpage' => 'webPage',
            'nickname' => 'nickName',
          ) as $mAS => $mWS) {
            if (isset($mobileContact->$mAS))
                $soapBody['contact'][$mWS] = $mobileContact->$mAS;
            else
                unset($soapBody['contact'][$mWS]);
        }

        $soapBody['contact']['fullName'] = syncphony::fixupFullName($soapBody['contact']);

        foreach (array('anniversary', 'birthday') as $m) {
            if (isset($mobileContact->$m))
                $soapBody['contact'][$m] = syncphony::timetDateTrunc($mobileContact->$m);
            else
                unset($soapBody['contact'][$m]);
        }

        foreach (array('categories', 'children') as $m) {
            if (isset($mobileContact->$m))
                $soapBody['contact'][$m] = implode(',', util_mkarray($mobileContact->$m));
            else
                unset($soapBody['contact'][$m]);
        }

        $soapBody['contact']['assistant'] = '';
        if (isset($mobileContact->assistantname))
            $soapBody['contact']['assistant'] = trim($mobileContact->assistantname);
        if (isset($mobileContact->assistnamephonenumber))
            $soapBody['contact']['assistant'] .= ';' . trim($mobileContact->assistnamephonenumber);
        if (!$soapBody['contact']['assistant'])
            unset($soapBody['contact']['assistant']);

        syncphony::bodyToKolab($mobileContact, $soapBody['contact']);

        $addresses = array();
        if (isset($soapBody['contact']['address']))
            $addresses = util_mkarray($soapBody['contact']['address']);
        $addresses = syncphony::mergeAddresses($addresses, $mobileContact,
          !empty($soapContact));
        if (count($addresses))
            $soapBody['contact']['address'] = $addresses;
        else
            unset($soapBody['contact']['address']);

        $phonenumbers = array();
        if (isset($soapBody['contact']['phone']))
            foreach (util_mkarray($soapBody['contact']['phone']) as $currentPhone)
                $phonenumbers[$currentPhone->number] = $currentPhone->type;
        $phonenumbers = syncphony::mergePhoneNumbers($phonenumbers, $mobileContact);
        if (count($phonenumbers))
            $soapBody['contact']['phone'] = $phonenumbers;
        else
            unset($soapBody['contact']['phone']);

        $emails = array();
        if (isset($soapBody['contact']['email']))
            foreach (util_mkarray($soapBody['contact']['email']) as $currentEMail)
                $emails[$currentEMail->smtpAddress] = $currentEMail->displayName;
        $emails = syncphony::mergeEMailAddresses($emails, $mobileContact);
        if (count($emails))
            $soapBody['contact']['email'] = $emails;
        else
            unset($soapBody['contact']['email']);

        if (!empty($mobileContact->picture))
            $soapBody['contact']['pictureAttachmentId'] = 'foto.anhang';
        /*XXX no unset in case of update? */

        if (empty($soapContact)) {
            $soapBody['contact']['creationDate'] = $this->currentTimestamp;
            $soapBody['contact']['lastModificationDate'] = $this->currentTimestamp;
            $soapBody['contact']['sensitivity'] = 'public';
        } else {
            $soapBody['contact']['creationDate'] = syncphony::reformatDateTime($soapBody['contact']['creationDate']);
            $soapBody['contact']['lastModificationDate'] = syncphony::reformatDateTime($soapBody['contact']['lastModificationDate']);

            if (!empty($kolabId))
                $soapBody['contact']['uid'] = $kolabId;
        }

        return $soapBody;
    }
}
