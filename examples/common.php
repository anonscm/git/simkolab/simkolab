<?php
/*-
 * Copy to /etc/simkolab/common.php and adjust to your needs.
 * Default values are in: /usr/share/simkolab/bb/config.php
 */

/* +++ mandatory configuration values +++ */

/* domain to use to qualify users by default, with top-level domain */
//define('KOLAB_DOMAIN', 'kolab.example.com');

/* +++ site-specific configuration values (examples) +++ */

//define('LDAP_HOST', 'ldap://kolabsvr.tarent.de/');
//define('LDAP_ANONYMOUS_BIND', false);
//define('LDAP_BIND_USER', 'cn=manager,cn=internal,ou=kolabsvr,dc=tarent,dc=de');
//define('LDAP_BIND_PASSWORD', 'geheim');
define('LDAP_SEARCH_BASE', 'dc=bsikolab,dc=tarent,dc=de');

define('IMAP_SERVER', 'bsikolab.tarent.de');

/* +++ optional configuration values (with sane defaults) +++ */

/* lower = less RAM usage; higher = better debugging; default 262144 */
define('JSONDEBUG_TRUNCATE_SIZE', 2048);

/* logging: set to debugging or one of the more extreme levels */
//define('LOGLEVEL', LOGLEVEL_DEBUG);
define('LOGLEVEL', LOGLEVEL_WBXML);
//define('LOGLEVEL', LOGLEVEL_DEVICEID);
/* you are unlikely to need these */
//define('LOGLEVEL', LOGLEVEL_WBXMLSTACK);
define('LOGBINWBXML', false);

/* also log authentication failures */
define('LOGAUTHFAIL', true);

/* dump all SOAP calls (with passwords!) to the log; default false */
//define('SOAP_DEBUG', true);		// structured
//define('SOAP_LOGRAWXML', true);	// raw XML
