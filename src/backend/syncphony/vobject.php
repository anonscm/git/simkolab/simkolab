<?php
/*-
 * Copyright © 2014, 2016, 2017
 *	mirabilos <t.glaser@tarent.de>
 *
 * Provided that these terms and disclaimer and all copyright notices
 * are retained or reproduced in an accompanying document, permission
 * is granted to deal in this work without restriction, including un‐
 * limited rights to use, publicly perform, distribute, sell, modify,
 * merge, give away, or sublicence.
 *
 * This work is provided “AS IS” and WITHOUT WARRANTY of any kind, to
 * the utmost extent permitted by applicable law, neither express nor
 * implied; without malicious intent or gross negligence. In no event
 * may a licensor, author or contributor be held liable for indirect,
 * direct, other damage, loss, or other issues arising in any way out
 * of dealing in the work, even if advised of the possibility of such
 * damage or existence of a defect, except proven that it results out
 * of said person’s immediate fault when using the work as intended.
 *-
 * Wrapper around Sabre\vObject for SimKolab
 */

use Sabre\VObject;

function vobject_parse($body, $headers) {
	try {
		$v = VObject\Reader::read($body,
		    VObject\Reader::OPTION_FORGIVING);
	} catch (Exception $e) {
		util_debugJ('ERR', true, 'VObject\\Reader::read failed: ' . $e);
		$v = false;
	}
	return $v;
}

function vobject_convert_dt($d, $root, $isEnd=false, $interval=false, $intervaladd=true) {
	global $tz_sk;

	$s = $d->getDateTime();
	if ($interval !== false)
		if ($intervaladd)
			$s->add($interval);
		else
			$s->sub($interval);
	if (!$d->hasTime()) {
		if ($isEnd) {
			$m = explode(' ', $s->format('Y m d'));
			$s->setDate($m[0], $m[1], $m[2] - 1);
			return $s->format('d.m.Y') . ' (einschließlich)';
		}
		return $s->format('d.m.Y') . ' (ganztägig)';
	}
	$v = strval($d);
	if (preg_match('/^\d{8}T\d{6}Z$/', $v)) {
		$s->setTimezone($tz_sk);
		return $s->format('d.m.Y H:i:s P');
	}
	if (!preg_match('/^\d{8}T\d{6}$/', $v))
		return "unbekannter Zeitstempel ($v)";
	if (!isset($d['TZID']))
		return $s->format('d.m.Y H:i:s') . ' (jeweilige Ortszeit)';
	$tz = strval($d['TZID']);
	try {
		\Sabre\VObject\TimeZoneUtil::getTimeZone($tz, $root, true);
	} catch (\InvalidArgumentException $e) {
		return $s->format('d.m.Y H:i:s') . " (unbekannte Zeitzone $tz)";
	}
	$rv = $s->format('d.m.Y H:i:s P');
	$s->setTimezone($tz_sk);
	return $rv . ' (' . $s->format('d.m.Y H:i:s P') . ')';
}

function vobject_dt2dt($d, $root, $interval=false, $intervaladd=true) {
	global $tz_sk;

	$s = $d->getDateTime();
	if ($interval !== false)
		if ($intervaladd)
			$s->add($interval);
		else
			$s->sub($interval);
	if (!$d->hasTime())
		/* already UTC 00:00:00 */
		return $s;
	$v = strval($d);
	if (preg_match('/^\d{8}T\d{6}Z$/', $v))
		/* already UTC */
		return $s;
	if (!preg_match('/^\d{8}T\d{6}$/', $v))
		return NULL;
	if (!isset($d['TZID'])) {
		/*
		 * jeweilige Ortszeit; wir nehmen an, daß die
		 * Uhrzeit des Endgerätes SIMKOLAB_TIMEZONE ist
		 */
		return \Sabre\VObject\DateTimeParser::parseDateTime($v, $tz_sk);
	}
	/* mit Zeitzone interpretiert; auf Plausiblität prüfen */
	$tz = strval($d['TZID']);
	try {
		\Sabre\VObject\TimeZoneUtil::getTimeZone($tz, $root, true);
		/* $s wurde bereits mit korrekter TZif eingelesen */
	} catch (\InvalidArgumentException $e) {
		/* unbekannte Zeitzone; behandle als UTC */
		$s = \Sabre\VObject\DateTimeParser::parseDateTime($v . 'Z');
	}
	return $s;
}

function vobject_dt2timet($d, $root, $interval=false, $intervaladd=true) {
	return \syncphony::renderTimestamp(vobject_dt2dt($d, $root, $interval, $intervaladd));
}

/* formats vobject_dt2timet result as UTC-based datestamp */
function vobject_timet_day($ts, $roundup=0) {
	global $tz_utc;

	return syncphony::timetDateTrunc($ts, $roundup, $tz_utc);
}

/* returns DateTimeZone object for $d if valid, SIMKOLAB_TIMEZONE otherwise */
function vobject_dt2tz($d, $root, $interval=false, $intervaladd=true) {
	global $tz_sk;

	if (isset($d['TZID'])) {
		$tz = strval($d['TZID']);
		try {
			return \Sabre\VObject\TimeZoneUtil::getTimeZone($tz,
			    $root, true);
		} catch (\InvalidArgumentException $e) {
		}
	}
	return $tz_sk;
}

function vobject_check_iMip(&$v) {
	if (!isset($v->VEVENT))
		return array(false, 'ein unbekanntes vCard-Objekt (kein Kalendereintrag)');
	if (!isset($v->METHOD))
		return array(false, 'einen Kalendereintrag unbekannten Types');
	$m = false;
	switch ($v->METHOD) {
	case 'PUBLISH':
		$m = false;
		$s = 'einen veröffentlichten Kalendereintrag';
		break;
	case 'REQUEST':
		/* what we want */
		if (!$v->VEVENT->UID)
			return array('IPM.Schedule.Meeting', 'eine defekte Termineinladung');
		/* probably */
		return true;
		break;
	case 'REPLY':
		switch ((isset($v->VEVENT->ATTENDEE) &&
		    isset($v->VEVENT->ATTENDEE[0]) &&
		    !isset($v->VEVENT->ATTENDEE[1]) &&
		    isset($v->VEVENT->ATTENDEE[0]['PARTSTAT'])) ?
		    strtoupper($v->VEVENT->ATTENDEE[0]['PARTSTAT']) : '') {
		case 'ACCEPTED':
			$m = 'IPM.Schedule.Meeting.Resp.Pos';
			$s = 'eine positive Antwort (Zusage) auf eine Termineinladung';
			break;
		case 'DECLINED':
			$m = 'IPM.Schedule.Meeting.Resp.Neg';
			$s = 'eine negative Antwort (Ablehnung) auf eine Termineinladung';
			break;
		case 'TENTATIVE':
			$m = 'IPM.Schedule.Meeting.Resp.Tent';
			$s = 'eine „vielleicht“-Antwort (tentative) auf eine Termineinladung';
			break;
		case 'NEEDS-ACTION':
		default:
			/* eh? */
			$m = 'IPM.Schedule.Meeting.Resp';
			$s = 'eine Antwort auf eine Termineinladung';
			break;
		}
		break;
	case 'ADD':
		$m = 'IPM.Schedule.Meeting';	//XXX
		$s = 'einen neuen Wiederholungstermin für einen bestehenden Kalendereintrag';
		break;
	case 'CANCEL':
		$m = 'IPM.Schedule.Meeting.Canceled';
		$s = 'eine Absage (cancellation) für ein oder mehrere bestehende Kalendereinträge';
		break;
	case 'REFRESH':
		return array(false, 'eine Anfrage für die aktuelle Version eines Termins');
		break;
	case 'COUNTER':
		$m = 'IPM.Schedule.Meeting.Resp.Neg';	// mostly
		$s = 'einen Gegenvorschlag zu einer Termineinladung';
		break;
	case 'DECLINECOUNTER':
		$m = 'IPM.Schedule.Meeting.Resp.Neg';	// mostly
		$s = 'eine Ablehnung eines Gegenvorschlags einer Termineinladung';
		break;
	default:
		return array(false, 'eine unbekannte Anfrage (Typ "' . $v->METHOD . '")');
	}

	if (isset($v->VEVENT->SUMMARY))
		$s .= ' namens "' . $v->VEVENT->SUMMARY . '"';
	if (isset($v->VEVENT->ORGANIZER) && ($v->VEVENT->ORGANIZER)) {
		$x = vobject_vis_caladdress($v->VEVENT->ORGANIZER, false);
		$s .= ' von ' . $x[0];
	}
	if (isset($v->VEVENT->LOCATION))
		$s .= ' (Ort "' . $v->VEVENT->LOCATION . '")';
	if (isset($v->VEVENT->DTSTART))
		$s .= ' am ' . vobject_convert_dt($v->VEVENT->DTSTART, $v);
	if (isset($v->VEVENT->CREATED))
		$s .= ' (erstellt am ' . vobject_convert_dt($v->VEVENT->CREATED, $v) . ')';
	return array($m, $s);
}

function vobject_nicelist(&$p) {
	$a = array();

	foreach ($p as $v)
		$a[] = implode(', ', $v->getParts());

	return implode('; ', $a);
}

function vobject_vis_caladdress(&$p, $is_attendee) {
	$a = array();

	foreach ($p as $k => $v) {
		$adr = trim(str_ireplace('mailto:', '', strval($v)));
		$cn = '';
		if (isset($v['CN']))
			$cn .= $v['CN'];
		$cn = trim(str_ireplace('mailto:', '', $cn));
		if (!strcasecmp($cn, $adr))
			$cn = '';
		$x = $adr;
		if ($cn)
			$x = $cn . ' <' . $x . '>';
		if (isset($v['DIR']))
			$x .= ' <' . $v['DIR'] . '>';
		if ($is_attendee) {
			$y = isset($v['CUTYPE']) ? strval($v['CUTYPE']) : '';
			switch (strtoupper($y)) {
			case '':
			case 'INDIVIDUAL':
				$y = '';
				break;
			case 'GROUP':
				$y = 'Personengruppe';
				break;
			case 'RESOURCE':
				$y = 'Ressource';
				break;
			case 'ROOM':
				$y = 'Raum';
				break;
			case 'UNKNOWN':
				$y = 'unbekannt';
				break;
			default:
				$y = '"' . $y . '"';
				break;
			}
			if ($y)
				$x .= ' {' . $y . '}';
			$x .= ' (';
			$y = isset($v['ROLE']) ? strval($v['ROLE']) : '';
			switch (strtoupper($y)) {
			case '':
			case 'REQ-PARTICIPANT':
				$x .= 'Teilnahme notwendig';
				break;
			case 'OPT-PARTICIPANT':
				$x .= 'Teilnahme optional';
				break;
			case 'NON-PARTICIPANT':
				$x .= 'zur Kenntnisnahme';
				break;
			case 'CHAIR':
				$x .= 'Sitzungsleiter';
				break;
			default:
				$x .= 'Teilnahme als "' . $y . '"';
				break;
			}
			$x .= '; ';
			$y = isset($v['PARTSTAT']) ? strval($v['PARTSTAT']) : '';
			switch (strtoupper($y)) {
			case '':
			case 'NEEDS-ACTION':
				$x .= 'benötigt Entscheidung';
				break;
			case 'ACCEPTED':
				$x .= 'akzeptiert';
				break;
			case 'DECLINED':
				$x .= 'abgelehnt';
				break;
			case 'TENTATIVE':
				$x .= 'ungewiß';
				break;
			case 'DELEGATED':
				$x .= 'delegiert';
				break;
			default:
				$x .= 'Teilnahmestatus "' . $y . '"';
				break;
			}
			if (isset($v['RSVP']) &&
			    strtoupper(strval($v['RSVP'])) == 'TRUE')
				$x .= '; Antwort erbeten';
			$x .= ')';
			if (isset($v['MEMBER']))
				$x .= ' {Mitglied von ' .
				    trim(str_ireplace('mailto:', '',
				    strval($v['MEMBER']))) . '}';
			if (isset($v['DELEGATED-FROM']))
				$x .= ' {Delegiert von ' .
				    trim(str_ireplace('mailto:', '',
				    strval($v['DELEGATED-FROM']))) . '}';
			if (isset($v['DELEGATED-TO']))
				$x .= ' {Delegiert an ' .
				    trim(str_ireplace('mailto:', '',
				    strval($v['DELEGATED-TO']))) . '}';
		}
		if (isset($v['SENT-BY']))
			$x .= ' - i.V. <' . trim(str_ireplace('mailto:', '',
			    strval($v['SENT-BY']))) . '>';
		$a[$k] = $x;
	}

	return $a;
}

function vobject_split_caladdress(&$p) {
	$a = array();

	foreach ($p as $k => $v) {
		$adr = trim(str_ireplace('mailto:', '', strval($v)));
		$cn = '';
		if (isset($v['CN']))
			$cn .= $v['CN'];
		$cn = trim(str_ireplace('mailto:', '', $cn));
		if (!strcasecmp($cn, $adr))
			$cn = '';
		if ($cn)
			$a[$k] = array($cn, $adr);
		else
			$a[$k] = array(false, $adr);
	}

	return $a;
}
