<?php
/***********************************************
* File      :   imap.php
* Project   :   SimKolab 4
* Descr     :   This backend is based on
*               'BackendDiff' and implements an
*               IMAP interface
*
* Created   :   10.10.2007
*
* Copyright 2013 - 2018 mirabilos <t.glaser@tarent.de>
* Copyright 2007 - 2016 Zarafa Deutschland GmbH
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License, version 3,
* as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Consult the Debian copyright file for details.
************************************************/

require_once('/usr/share/simkolab/bb/lib/utils/tzutil.php');

class BackendSyncphony extends BackendDiff {
    protected $server;
    protected $mbox;
    protected $mboxFolder;
    protected $username;
    protected $domain;
    protected $sk_mailboxes;
    protected $serverdelimiter;
    protected $sinkfolders;
    protected $sinkstates_imap;
    protected $sinkstates_pim;
    protected $excludedFolders;
    protected $syncphony;

    /**----------------------------------------------------------------------------------------------------------
     * default backend methods
     */

    /**
     * Authenticates the user
     *
     * @param string        $username
     * @param string        $domain
     * @param string        $password
     *
     * @access public
     * @return boolean
     * @throws FatalException   if php-imap module can not be found
     */
    public function Logon($username, $domain, $password) {
        $this->server = "{" . IMAP_SERVER . ":" . IMAP_PORT . "/imap" . IMAP_OPTIONS . "}";

        if (!function_exists("imap_open"))
            throw new FatalException("BackendSyncphony(): php-imap module is not installed", 0, null, LOGLEVEL_FATAL);

        $this->excludedFolders = array();
        if (defined('IMAP_EXCLUDED_FOLDERS') && strlen(IMAP_EXCLUDED_FOLDERS) > 0) {
            foreach (explode('|', IMAP_EXCLUDED_FOLDERS) as $fld)
                $this->excludedFolders[] = strtolower(mb_convert_encoding($fld,
                  'UTF7-IMAP', 'UTF-8'));
            ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->Logon(): Excluding Folders (%s)", IMAP_EXCLUDED_FOLDERS));
        }

        // open the IMAP-mailbox
        $this->mbox = @imap_open($this->server, $username, $password, OP_HALFOPEN);
        $this->mboxFolder = "";

        if (!$this->mbox) {
            ZLog::Write(LOGLEVEL_ERROR, "BackendSyncphony->Logon(): can't connect: " . imap_last_error());
            return false;
        }

        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->Logon(): User '%s' is authenticated on IMAP", $username));
        $this->username = $username;
        $this->domain = $domain;
        $this->reloadFoldersFromIMAP();
        return true;
    }

    /**
     * Initialise things that need a device ID
     */
    public function initialise($password) {
        if (!$this->mbox)
            throw new FatalException('BackendSyncphony->initialise(): called without successful Logon()', 0, NULL, LOGLEVEL_FATAL);
        /* Syncphony logon */
        $user = $this->username;
        if (strpos($user, '@') === false)
            $user .= '@' . KOLAB_DOMAIN;
        if (!($devid = Request::GetDeviceID()))
            throw new FatalException('BackendSyncphony->initialise(): Request::GetDeviceID returned falsy value!', 0, NULL, LOGLEVEL_FATAL);
        $this->syncphony = new syncphony($user, $password, $devid,
          (0.0 + Request::GetProtocolVersion()));
        $this->syncphony->init();
    }

    /**
     * Logs off
     * Called before shutting down the request to close the IMAP connection
     * writes errors to the log
     *
     * @access public
     * @return boolean
     */
    public function Logoff() {
        if ($this->mbox) {
            // list all errors
            $errors = imap_errors();
            if (is_array($errors)) {
                foreach ($errors as $e) {
                    if (stripos($e, "fail") !== false) {
                        $level = LOGLEVEL_WARN;
                    }
                    else {
                        $level = LOGLEVEL_DEBUG;
                    }
                    ZLog::Write($level, "BackendSyncphony->Logoff(): IMAP said: " . $e);
                }
            }
            @imap_close($this->mbox);
            ZLog::Write(LOGLEVEL_DEBUG, "BackendSyncphony->Logoff(): IMAP connection closed");
        }
        $this->SaveStorages();
    }

    /**
     * Sends an e-mail
     * This messages needs to be saved into the 'sent items' folder
     *
     * @param SyncSendMail  $sm     SyncSendMail object
     *
     * @access public
     * @return boolean
     * @throws StatusException
     */
    // TODO implement $saveInSent = true
    public function SendMail($sm) {
        $parent = util_ifsetor($sm->source->folderid, false);
        $forward = $reply = (isset($sm->source->itemid) && $sm->source->itemid && $parent) ?
          $this->getImapIdFromServerId($parent, $sm->source->itemid) : false;
        //util_debugJ('SMBEG', array('forward/reply' => $reply));

        /* iPhone, iPad, iPod; Apple Mail or SecurePIM, we don’t care */
        $ios = preg_match('/^(iPhone|iP[ao]d)/', Request::GetDeviceType()) === 1;

        ZLog::Write(LOGLEVEL_DEBUG, sprintf("IMAPBackend->SendMail(): RFC822: %d bytes; forward-id: '%s' reply-id: '%s' parent-id: '%s' SaveInSent: '%s' ReplaceMIME: '%s' iOS: %s",
                                            strlen($sm->mime), Utils::PrintAsString($sm->forwardflag), Utils::PrintAsString($sm->replyflag),
                                            Utils::PrintAsString((isset($sm->source->folderid) ? $sm->source->folderid : false)),
                                            Utils::PrintAsString(($sm->saveinsent)), Utils::PrintAsString(isset($sm->replacemime)),
                                            $ios ? 'yes' : 'no'));

        if ($parent) {
            // convert parent folder id back to work on an imap-id
            $parent = $this->getImapIdFromFolderId($sm->source->folderid);
        }

        // by splitting the message in several lines we can easily grep later
        foreach (preg_split("/((\r)?\n)/", $sm->mime) as $rfc822line)
            ZLog::Write(LOGLEVEL_WBXML, "RFC822: ". $rfc822line);

        $mobj = new Mail_mimeDecode($sm->mime);
        $message = $mobj->decode(array('decode_headers' => false, 'decode_bodies' => true, 'include_bodies' => true, 'charset' => 'utf-8'));

        $repl_part0 = false;
        if (strpos(util_ifscalaror($message->headers['x-mailer'], '', ''), 'BlackBerry') !== false &&
          strpos(strtolower(util_ifscalaror($message->headers['content-type'], '', '')), 'multipart/mixed') !== false &&
          !isset($message->body) &&
          strpos(strtolower(util_ifscalaror($message->parts[0]->headers['content-type'], '', '')), 'text/html') !== false &&
          strpos(util_ifsetor($message->parts[0]->body, ''), '<body') !== false) {
            $charset = 'utf-8'; /* Mail_mimeDecode::decode converts there already */
            $charset = ' -assume_charset=' . $charset . ' -display_charset=' . $charset;
            ZLog::Write(LOGLEVEL_DEBUG, "BackendSyncphony->SendMail(): invoking lynx -dump -force_html -stdin -nomargins" . $charset);
            if (($body = shell_exec("printf '%s\n' '" .
              /* escapeshellarg is locale-dependent and thus broken */
              str_replace("'", "'\"'\"'", $message->parts[0]->body) .
              "' | lynx -dump -force_html -stdin -nomargins" . $charset))) {
                ZLog::Write(LOGLEVEL_DEBUG, "BackendSyncphony->SendMail(): replacing HTML first message part: ". print_r(array('msghdrs' => $message->headers, 'headers' => $message->parts[0]->headers, 'body' => $message->parts[0]->body), 1));
                $message->parts[0]->body = $body;
                $message->parts[0]->headers['content-type'] = str_ireplace('html',
                  'plain', preg_replace("/charset=([A-Za-z0-9-\"']+)/i",
                  "charset=\"utf-8\"", $message->headers['content-type']));
                $message->parts[0]->ctype_secondary = 'plain';
                if (util_ifsetor($message->parts[0]->ctype_parameters['charset']))
                    $message->parts[0]->ctype_parameters['charset'] = 'utf-8';
                $repl_part0 = $body . "\n";
            }
        }

        if (strpos(util_ifscalaror($message->headers['x-mailer'], '', ''), 'BlackBerry') !== false &&
          strpos(strtolower(util_ifscalaror($message->headers['content-type'], '', '')), 'text/html') !== false &&
          strpos(util_ifsetor($message->body, ''), '<body') !== false) {
            $charset = 'utf-8'; /* Mail_mimeDecode::decode converts there already */
            $charset = ' -assume_charset=' . $charset . ' -display_charset=' . $charset;
            ZLog::Write(LOGLEVEL_DEBUG, "BackendSyncphony->SendMail(): invoking lynx -dump -force_html -stdin -nomargins" . $charset);
            if (($body = shell_exec("printf '%s\n' '" .
              /* escapeshellarg is locale-dependent and thus broken */
              str_replace("'", "'\"'\"'", $message->body) .
              "' | lynx -dump -force_html -stdin -nomargins" . $charset))) {
                ZLog::Write(LOGLEVEL_DEBUG, "BackendSyncphony->SendMail(): replacing HTML body: ". print_r(array('headers' => $message->headers, 'body' => $message->body), 1));
                $message->body = $body . "\n";
                $message->headers['content-type'] = str_ireplace('html',
                  'plain', $message->headers['content-type']);
                $message->ctype_secondary = 'plain';
            }
        }

        $Mail_RFC822 = new Mail_RFC822();
        $toaddr = $ccaddr = $bccaddr = "";
        if(isset($message->headers["to"]))
            $toaddr = $this->parseAddr($Mail_RFC822->parseAddressList($message->headers["to"]));
        if(isset($message->headers["cc"]))
            $ccaddr = $this->parseAddr($Mail_RFC822->parseAddressList($message->headers["cc"]));
        if(isset($message->headers["bcc"]))
            $bccaddr = $this->parseAddr($Mail_RFC822->parseAddressList($message->headers["bcc"]));

        // save some headers when forwarding mails (content type & transfer-encoding)
        $headers = array();
        $forward_h_ct = "";
        $forward_h_cte = "";
        $envelopefrom = "";

        $use_orgbody = false;
        $is_smime = false;

        // clean up the transmitted headers
        // remove default headers because we are using imap_mail
        $gotfrom = false;
        $gotmsgid = false;
        $gotrefs = false; $parentrefs = false;
        $gotirt = false; $parentmsgid = false;
        $returnPathSet = false;
        $body_encoding = false;
        $org_boundary = false;
        //XXX these are already unique? what about duplicates?
        // at least, continuation lines are already handled
        foreach ($message->headers as $k => $v) {
            if ($k == "subject" || $k == "to" || $k == "cc" || $k == "bcc")
                continue;

            if ($k == 'x-mailer' && !trim($v))
                continue;

            if ($k == 'mime-version' && trim($v) == "1.0") {
                /* ensure correct capitalisation */
                $headers[] = 'MIME-Version: 1.0';
                continue;
            }

            if ($k == "content-type") {
                // do not touch an S/MIME message at all
                if (preg_match('!application/(x-)?pkcs7-mime!i', $v)) {
                    $is_smime = true;
                }
                if (preg_match('!multipart/signed!i', $v)) {
                    /* S/MIME signed (transparent), or maybe PGP/MIME? */
                    $is_smime = true;
                }

                // if the message is a multipart message, then we should use the sent body
                if (preg_match("!multipart/!i", $v)) {
                    $use_orgbody = true;
                    $org_boundary = $message->ctype_parameters["boundary"];
                }

                // set charset always to utf-8
                $v = preg_replace("/charset=([A-Za-z0-9-\"']+)/i", "charset=\"utf-8\"", $v);

                // save the original content-type header for the body part when forwarding
                if ($sm->forwardflag && !$use_orgbody) {
                    $forward_h_ct = $v;
                    continue;
                }
            }

            if ($k == "content-transfer-encoding") {
                // if the content was base64 encoded, encode the body again when sending
                switch (trim($v)) {
                case 'base64':
                    $body_encoding = 'base64';
                    break;
                case 'quoted-printable':
                    $body_encoding = 'qp';
                    break;
                case '8bit':
                case '7bit':
                default:
                    /* not handled atm */
                    break;
                }

                // save the original encoding header for the body part when forwarding
                if ($sm->forwardflag) {
                    $forward_h_cte = $v;
                    continue;
                }
            }

            // check if "from"-header is set
            if ($k == "from") {
                if (!trim($v))
                    continue;
                $gotfrom = true;
                $envelopefrom = $v;
            }

            // check if Message-ID, References and In-Reply-To headers are set
            if ($k == 'message-id')
                $gotmsgid = true;
            if ($k == 'references')
                $gotrefs = true;
            if ($k == 'in-reply-to')
                $gotirt = true;

            // check if "Return-Path"-header is set
            if ($k == "return-path") {
                $returnPathSet = true;
                if (!trim($v)) {
                    $v = $this->username;
                }
            }

            // improve RFC822 header casing in sent-out messages
            $headers[] = ucwords(strtolower($k), '-') . ': ' . $v;
        }

        // set "From" header if not set on the device
        $v = $this->username;
        if (!$gotfrom) {
            $headers[] = 'From: ' . $v;
        }
        /* set envelope sender */
        $envelopefrom = $v;

        // set "Return-Path" header if not set on the device
        if (!$returnPathSet) {
            $v = $this->username;
            $headers[] = 'Return-Path: ' . $v;
        }

        // set "Message-ID" header if not set on the device
        if (!$gotmsgid) {
            $v = util_make_msgid($this->username, KOLAB_DOMAIN);
            $headers[] = 'Message-ID: ' . $v;
        }

        // reply
        if ($sm->replyflag && $parent) {
            $this->imap_reopenFolder($parent);
            // receive entire mail (header + body) to decode body correctly
            $origmail = @imap_fetchheader($this->mbox, $reply, FT_UID) . @imap_body($this->mbox, $reply, FT_PEEK | FT_UID);
            if (!$origmail)
                throw new StatusException(sprintf("BackendSyncphony->SendMail(): Could not open message id '%s' in folder id '%s' to be replied: %s", $reply, $parent, imap_last_error()), SYNC_COMMONSTATUS_ITEMNOTFOUND);

            $mobj2 = new Mail_mimeDecode($origmail);
            $msg2 = $mobj2->decode(array('decode_headers' => false, 'decode_bodies' => true, 'include_bodies' => true, 'charset' => 'utf-8'));
            if (!$parentmsgid && util_ifsetor($msg2->headers['message-id']))
                $parentmsgid = $msg2->headers['message-id'];
            if (!$parentrefs && util_ifsetor($msg2->headers['references']))
                $parentrefs = $msg2->headers['references'];

            // receive only body; Apple Mail *and* SecurePIM attach the orig body themselves
            $reply_body = $ios ? '' : $this->getPlainBody($msg2);

            // unset mimedecoder & origmail - free memory
            unset($msg2);
            unset($mobj2);
            unset($origmail);
        } else
            $reply_body = "";

        // if this is a multipart message with a boundary, we must use the original body
        if ($ios && $is_smime) {
            // do not touch messsage at all
        } elseif ($use_orgbody) {
            list(,$body) = $mobj->_splitBodyHeader($sm->mime);
            /* text/plain can pass unhindered; we need this for replies */
            if ($repl_part0 === false && ($reply_body) &&
              strpos(strtolower(util_ifscalaror($message->parts[0]->headers['content-type'], '', '')), 'text/plain') !== false) {
                /* "replace" body with itself (in UTF-8) */
                /* in order to add the reply body inline, further below */
                $repl_part0 = $message->parts[0]->body;
            }
            if ($repl_part0 !== false) {
                /* handle replies: add reply body inline */
                $repl_part0 .= $reply_body;
                /* HTML -> text conversion for BlackBerry */
                $new_part0 = array();
                $old_part0 = $mobj->_splitBodyHeader($message->parts[0]->raw);
                foreach (preg_split("/((\r)?\n)/", $old_part0[0]) as $hdrline) {
                    if (preg_match("/^content-type:/i", $hdrline)) {
                        $hdrline = str_ireplace("text/html", "text/plain",
                          preg_replace("/charset=([A-Za-z0-9-\"']+)/i",
                          "charset=\"utf-8\"", $hdrline));
                    } elseif (preg_match("/^content-transfer-encoding:/i", $hdrline)) {
                        continue;
                    }
                    $new_part0[] = $hdrline;
                }
                $new_part0[] = "Content-Transfer-Encoding: quoted-printable";
                $new_part0 = trim(implode("\n", $new_part0)) . "\n\n" .
                  quoted_printable_encode(trim(implode("\n", util_split_newlines($repl_part0)))) . "\n";
                ZLog::Write(LOGLEVEL_DEBUG, "BackendSyncphony->SendMail(): before repl_part0: <".$body.">");
                $body = str_replace($message->parts[0]->raw,
                  util_sanitise_multiline_submission($new_part0), $body);
                ZLog::Write(LOGLEVEL_DEBUG, "BackendSyncphony->SendMail(): after repl_part0 : <".$body.">");
            } else if ($reply_body) {
                /* attach reply body since message body is not plaintext */
                $body = str_replace("\r\n", "\n", $body);
                $body = preg_split("/^--" . preg_quote($org_boundary, "/") . "\n/m", $body);
                array_splice($body, 2, 0, "MIME-Version: 1.0\n" .
                  "Content-Type: text/plain; charset=\"utf-8\"\n" .
                  "Content-Transfer-Encoding: quoted-printable\n" .
                  "\n" .
                  quoted_printable_encode(trim(implode("\n", util_split_newlines($reply_body)))) . "\n" .
                  "\n");
                $body = implode("--" . $org_boundary . "\n", $body);
            }
        } else if ($is_smime && !$sm->forwardflag) {
            /*
             * nothing to do here: should be the same as the else block
             * below, except the body is replaced a couple of lines
             * below, and we absolutely MUST NOT change the Content-Type
             * header either, so nothing is actually left, except of
             * course to note that S/MIME and forwarding is unhandled
             */
        } else {
            /* use message body text; add reply body inline */
            $body = $this->getPlainBody($message) . $reply_body;
            /* fixup Content-Type header */
            $found = false;
            $s = 'content-type:';
            foreach ($headers as $k => $v) {
                if (strncasecmp($v, $s, strlen($s)))
                    continue;
                $found = $k;
                break;
            }
            $s = 'Content-Type: text/plain; charset="utf-8"';
            if ($found !== false)
                $headers[$found] = $s;
            else
                $headers[] = $s;
        }

        /* match original eMail body encoding */
        if (!$sm->forwardflag && !$use_orgbody) {
            $body = ($body_encoding == 'qp' ? quoted_printable_encode(util_sanitise_multiline_submission($body)) :
              ($body_encoding == 'base64' ? chunk_split(base64_encode($body)) :
              $body));
        }

        if ($is_smime && !$sm->forwardflag) {
            /* just use original body, period. */
            list(,$body) = $mobj->_splitBodyHeader($sm->mime);
            /* handle line wrapping for BlackBerry, since I am so nice */
            if ($body_encoding == 'base64') {
                $x = chunk_split(base64_encode(base64_decode($body)));
                $body = $x ? $x : $body;
            }
            /*
             * we drop $reply_body (message body we replied to) completely,
             * here, because changing the message body breaks cryptographic
             * integrity (XXX think about mangling the two messages into a
             * bigger one, keeping each part)
             *
             * XXX don't handle Forward with S/MIME yet
             */
        }

        // forward
        if ($sm->forwardflag && $parent) {
            $this->imap_reopenFolder($parent);
            // receive entire mail (header + body)
            $origmail = @imap_fetchheader($this->mbox, $forward, FT_UID) . @imap_body($this->mbox, $forward, FT_PEEK | FT_UID);

            if (!$origmail)
                throw new StatusException(sprintf("BackendSyncphony->SendMail(): Could not open message id '%s' in folder id '%s' to be forwarded: %s", $forward, $parent, imap_last_error()), SYNC_COMMONSTATUS_ITEMNOTFOUND);

            $mobj2 = new Mail_mimeDecode($origmail);
            $msg2 = $mobj2->decode(array('decode_headers' => false, 'decode_bodies' => true, 'include_bodies' => true, 'charset' => 'utf-8'));
            if (!$parentmsgid && util_ifsetor($msg2->headers['message-id']))
                $parentmsgid = $msg2->headers['message-id'];
            if (!$parentrefs && util_ifsetor($msg2->headers['references']))
                $parentrefs = $msg2->headers['references'];

            // contrib - chunk base64 encoded body
            $body = ($body_encoding == 'qp' ? quoted_printable_encode(util_sanitise_multiline_submission($body)) :
              ($body_encoding == 'base64' ? chunk_split(base64_encode($body)) :
              $body));
            //use original boundary if it's set
            $boundary = ($org_boundary) ? $org_boundary : false;
            // build a new mime message, forward entire old mail as file
            $gotmime = false;
            if (stripos("\n" . implode("\n", $headers), "\nmime-version:") === false)
                $headers[] = 'MIME-Version: 1.0';
            list($aheader, $body) = $this->mail_attach("weitergeleitete_Nachricht.eml", strlen($origmail), $origmail, $body, $forward_h_ct, $forward_h_cte, $boundary, "message/rfc822");
            // add boundary headers
            $headers[] = $aheader;

            // unset mimedecoder & origmail - free memory
            unset($msg2);
            unset($mobj2);
            unset($origmail);
        }

        // more debugging, also, fixing Subject encoding
        mb_internal_encoding("UTF-8");
        $xdecode = function ($str) {
            if (($s = strval($str)) === '')
                return '';

            if (preg_match('/[^\x01-\x7E]/', $s)) {
                $s = util_fixutf8($s);
                ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->SendMail(): Subject in raw UTF-8: %s", $s));
            } elseif (($c = strval(iconv_mime_decode($s, 1, 'UTF-8'))) === '' &&
              ($c = strval(mb_decode_mimeheader($s))) === '') {
                $s = util_fixutf8($s);
                ZLog::Write(LOGLEVEL_WARN, "iconv_mime_decode and mb_decode_mimeheader($s) failed");
            } else {
                $s = util_fixutf8($c);
            }
            return is_string($str) && ($s === $str) ? $str : $s;
        };
        ZLog::Write(LOGLEVEL_DEBUG, "BackendSyncphony->SendMail(): parsed message: ". print_r($message,1));
        if (!isset($message->headers["subject"])) {
            ZLog::Write(LOGLEVEL_DEBUG, "BackendSyncphony->SendMail(): subject: no subject set. Set to dummy.");
            $message->headers["subject"] = '(no subject)';
        } elseif (is_array($message->headers["subject"])) {
            $message->headers["subject"] = implode('│', array_map($xdecode, $message->headers["subject"]));
            ZLog::Write(LOGLEVEL_DEBUG, "BackendSyncphony->SendMail(): subject: multiple subjects set. Set to: {$message->headers["subject"]}");
        } else {
            $message->headers["subject"] = $xdecode($message->headers["subject"]);
            ZLog::Write(LOGLEVEL_DEBUG, "BackendSyncphony->SendMail(): subject: {$message->headers["subject"]}");
        }
        $message->headers["subject"] = trim(mb_encode_mimeheader(trim($message->headers["subject"]),
          'UTF-8', 'Q', "\015\012", strlen('Subject: ')));

        // add "In-Reply-To" and "References" headers if missing
        if ($parentmsgid) {
            if (!$gotirt) {
                $headers[] = 'In-Reply-To: ' . $parentmsgid;
            }
            if (!$gotrefs) {
                if (!$parentrefs) {
                    $headers[] = 'References: ' . $parentmsgid;
                } else {
                    $headers[] = 'References: ' . $parentrefs . "\n " .
                      $parentmsgid;
                }
            }
        }

        $headers[] = 'X-ZPAS: ' . ZPAS_VERSION;
        if (!empty($ccaddr))
            $headers[] = "Cc: $ccaddr";
        if (!empty($bccaddr))
            $headers[] = "Bcc: $bccaddr";

        // ensure CRLF line endings
        $body = trim(util_sanitise_multiline_submission($body)) . "\015\012";
        $headerstr = trim(util_sanitise_multiline_submission($headers));

        if ($envelopefrom)
            $envelopefrom = '-f' . escapeshellarg($envelopefrom);
        util_debugJ('mail', array(
            '1toaddr' => $toaddr,
            '2subject' => $message->headers["subject"],
            '3body' => $body,
            '4headers' => $headers,
            '5envelopefrom' => $envelopefrom,
          ));
        unset($headers);
        $send = @mail(str_replace("\r\n", "\n", $toaddr),
          str_replace("\r\n", "\n", $message->headers["subject"]),
          str_replace("\r\n", "\n", $body),
          str_replace("\r\n", "\n", $headerstr), $envelopefrom);

        // email sent?
        if (!$send)
            throw new StatusException(sprintf("BackendSyncphony->SendMail(): The email could not be sent. Last IMAP-error: %s", imap_last_error()), SYNC_COMMONSTATUS_MAILSUBMISSIONFAILED);

        // add message to the sent folder

        // build complete headers
        $headerstr = 'To: ' . $toaddr . "\015\012" . 'Subject: ' . $message->headers["subject"] . "\015\012" . $headerstr;
        ZLog::Write(LOGLEVEL_DEBUG, "BackendSyncphony->SendMail(): complete headers: $headerstr");

        if (!$this->addSentMessage(util_ifsetor($this->sk_mailboxes['sent'],
          'sent-mail'), $headerstr, $body)) {
            ZLog::Write(LOGLEVEL_ERROR, "BackendSyncphony->SendMail(): The email could not be saved to Sent Items folder. Check your configuration.");
        }

        return $send;
    }

    /**
     * Returns the waste basket
     *
     * @access public
     * @return string
     */
    public function GetWasteBasket() {
        if (!isset($this->sk_mailboxes['trash']))
            return NULL;

        return $this->convertImapId($this->sk_mailboxes['trash']);
    }

    /**
     * Returns the content of the named attachment as stream. The passed attachment identifier is
     * the exact string that is returned in the 'AttName' property of an SyncAttachment.
     * Any information necessary to find the attachment must be encoded in that 'attname' property.
     * Data is written directly (with print $data;)
     *
     * @param string        $attname
     *
     * @access public
     * @return SyncItemOperationsAttachment
     * @throws StatusException
     */
    public function GetAttachmentData($attname) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->GetAttachmentData('%s')", $attname));

        $mparts = explode(':', $attname);
        $mparts[] = '';
        list($folderid, $id, $part, $newpart) = $mparts;

        if (!$folderid || !$id ||
          ($newpart === '' && !$part && $part !== '0') ||
          (($part === '!' || $part === 'i') && !$newpart && $newpart !== '0'))
            throw new StatusException(sprintf("BackendSyncphony->GetAttachmentData('%s'): Error, attachment name key can not be parsed", $attname), SYNC_ITEMOPERATIONSSTATUS_INVALIDATT);

        // convert back to work on an imap-id
        $folderImapid = $this->getImapIdFromFolderId($folderid);

        $this->imap_reopenFolder($folderImapid);
        $mail = @imap_fetchheader($this->mbox, $id, FT_UID) . @imap_body($this->mbox, $id, FT_PEEK | FT_UID);

        $mobj = new Mail_mimeDecode($mail);
        $message = $mobj->decode(array('decode_headers' => true, 'decode_bodies' => true, 'include_bodies' => true, 'charset' => 'utf-8'));

        if ($part === '!') {
            /* new algorithm, MIME attachment */

            $this->flattenMessage($message);
            $thepart = $message->linear_attachments[$newpart];
            $part = "MIME/$newpart";
        } elseif ($part === 'i') {
            /* new algorithm, iMip attachment */

            require_once('/usr/share/simkolab/bb/backend/syncphony/vobject.php');
            $newpart = (int)$newpart;
            $this->flattenMessage($message);
            if (count($message->linear_iMip) != 1)
                throw new StatusException(sprintf("BackendSyncphony->GetAttachmentData('%s'): Error, requested message does not have one iMip invitation, attachment cannot be accessed", $attname), SYNC_ITEMOPERATIONSSTATUS_INVALIDATT);
            if (($v = vobject_parse($message->linear_iMip[0], $message->headers)) === false)
                throw new StatusException(sprintf("BackendSyncphony->GetAttachmentData('%s'): Error, requested message does not have a valid iMip invitation, attachment cannot be accessed", $attname), SYNC_ITEMOPERATIONSSTATUS_INVALIDATT);
            if (vobject_check_iMip($v) !== true)
                throw new StatusException(sprintf("BackendSyncphony->GetAttachmentData('%s'): Error, requested iMip message is not an invitation, attachment cannot be accessed", $attname), SYNC_ITEMOPERATIONSSTATUS_INVALIDATT);
            if (!isset($v->VEVENT->ATTACH))
                throw new StatusException(sprintf("BackendSyncphony->GetAttachmentData('%s'): Error, requested iMip invitation has no attachments", $attname), SYNC_ITEMOPERATIONSSTATUS_INVALIDATT);
            if (!isset($v->VEVENT->ATTACH[$newpart]))
                throw new StatusException(sprintf("BackendSyncphony->GetAttachmentData('%s'): Error, requested iMip invitation attachment cannot be found", $attname), SYNC_ITEMOPERATIONSSTATUS_INVALIDATT);
            $thepart = new stdClass();
            $thepart->body = strval($v->VEVENT->ATTACH[$newpart]);
            $i = false;
            if (isset($v->VEVENT->ATTACH[$newpart]['FMTTYPE'])) {
                if (preg_match('~^([a-zA-Z0-9][a-zA-Z0-9!#$&^_.+-]{0,126})/([a-zA-Z0-9][a-zA-Z0-9!#$&^_.+-]{0,126})$~',
                  $v->VEVENT->ATTACH[$newpart]['FMTTYPE'], $i)) {
                    $thepart->ctype_primary = $i[1];
                    $thepart->ctype_secondary = $i[2];
                } elseif (preg_match('~^([a-zA-Z0-9][a-zA-Z0-9!#$&^_.+-]{0,126})$~',
                  $v->VEVENT->ATTACH[$newpart]['FMTTYPE'], $i)) {
                    $thepart->ctype_primary = $i[1];
                    $thepart->ctype_secondary = '';
                }
            }
            $part = "iMip/$newpart";
        } else {
            /* old algorithm */

            //trying parts
            $mparts = $message->parts;
            for ($i = 0; $i < count($mparts); $i++) {
                $auxpart = $mparts[$i];
                //recursively add parts
                if($auxpart->ctype_primary == "multipart" && ($auxpart->ctype_secondary == "mixed" || $auxpart->ctype_secondary == "alternative" || $auxpart->ctype_secondary == "related")) {
                    foreach ($auxpart->parts as $spart)
                        $mparts[] = $spart;
                }
            }

            $thepart = $mparts[$part];
            $part = "old/$part";
        }

        if (!isset($thepart) || !isset($thepart->body))
            throw new StatusException(sprintf("BackendSyncphony->GetAttachmentData('%s'): Error, requested part key can not be found: '%s'", $attname, $part), SYNC_ITEMOPERATIONSSTATUS_INVALIDATT);
        if (IMAP_ATTACHMENT_MAXSIZE && strlen($thepart->body) > IMAP_ATTACHMENT_MAXSIZE)
            throw new StatusException(sprintf("BackendSyncphony->GetAttachmentData('%s'): Part '%s' size %d longer than hard limit %d", $attname, $part, strlen($thepart->body), IMAP_ATTACHMENT_MAXSIZE), SYNC_ITEMOPERATIONSSTATUS_INVALIDATT);

        // unset mimedecoder & mail
        unset($mobj);
        unset($mail);

        $attachment = new SyncItemOperationsAttachment();
        $attachment->data = StringStreamWrapper::Open($thepart->body);
        if (util_ifscalaror($thepart->ctype_primary)) {
            $attachment->contenttype = strval($thepart->ctype_primary);
            if (util_ifscalaror($thepart->ctype_secondary))
                $attachment->contenttype .= '/' . $thepart->ctype_secondary;
        }

        unset($mparts);
        unset($message);

        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->GetAttachmentData contenttype %s", $attachment->contenttype));

        return $attachment;
    }

    /**
     * Indicates if the backend has a ChangesSink.
     * A sink is an active notification mechanism which does not need polling.
     * The IMAP backend simulates a sink by polling status information of the folder
     *
     * @access public
     * @return boolean
     */
    public function HasChangesSink() {
        $this->sinkfolders = array();
        $this->sinkstates_imap = array();
        $this->sinkstates_pim = array();
        return true;
    }

    /**
     * The folder should be considered by the sink.
     * Folders which were not initialized should not result in a notification
     * of IBacken->ChangesSink().
     *
     * @param string        $folderid
     *
     * @access public
     * @return boolean      false if found can not be found
     */
    public function ChangesSinkInitialize($folderid) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("IMAPBackend->ChangesSinkInitialize(): folderid '%s'", $folderid));

        if (strpos($folderid, "VIRTUAL") !== false) {
            switch ($folderid) {
            case 'VIRTUAL/calendar':
            case 'VIRTUAL/contacts':
                $this->sinkfolders[] = array(
                    'id' => $folderid,
                    'type' => 'PIM',
                  );
                return true;
            default:
                util_debugJ('unknown virtual folder');
                return false;
            }
        }

        $imapid = $this->getImapIdFromFolderId($folderid);

        if ($imapid) {
            $this->sinkfolders[] = array(
                'imapid' => $imapid,
                'type' => 'imap',
              );
            return true;
        }

        return false;
    }

    /**
     * The actual ChangesSink.
     * For max. the $timeout value this method should block and if no changes
     * are available return an empty array.
     * If changes are available a list of folderids is expected.
     *
     * @param int           $timeout        max. amount of seconds to block
     *
     * @access public
     * @return array
     */
    public function ChangesSink($timeout = 30) {
        $notifications = array();
        $stopat = time() + $timeout - 1;

        while ($stopat > time() && empty($notifications)) {
            foreach ($this->sinkfolders as $sinkfolder) {
                if ($sinkfolder['type'] == 'PIM') {
                    switch ($sinkfolder['id']) {
                    case 'VIRTUAL/calendar':
                        $newstate = $this->syncphony->checkStatus('Event');
                        break;
                    case 'VIRTUAL/contacts':
                        $newstate = $this->syncphony->checkStatus('Contact');
                        break;
                    }
                    if (!isset($this->sinkstates_pim[$sinkfolder['id']]))
                        $this->sinkstates_pim[$sinkfolder['id']] = $newstate;
                    elseif ($newstate != $this->sinkstates_pim[$sinkfolder['id']]) {
                        $notifications[] = $sinkfolder['id'];
                        $this->sinkstates_pim[$sinkfolder['id']] = $newstate;
                    }
                    continue;
                } elseif ($sinkfolder['type'] != 'imap')
                    continue;
                $imapid = $sinkfolder['imapid'];

                $this->imap_reopenFolder($imapid);

                // courier-imap only cleares the status cache after checking
                @imap_check($this->mbox);

                $status = @imap_status($this->mbox, $this->server . $imapid, SA_ALL);
                if (!$status) {
                    ZLog::Write(LOGLEVEL_WARN, sprintf("ChangesSink: could not stat folder '%s': %s ", $this->getFolderIdFromImapId($imapid), imap_last_error()));
                }
                else {
                    $newstate = "M:". $status->messages ."-R:". $status->recent ."-U:". $status->unseen;

                    if (! isset($this->sinkstates_imap[$imapid]) )
                        $this->sinkstates_imap[$imapid] = $newstate;

                    if ($this->sinkstates_imap[$imapid] != $newstate) {
                        $notifications[] = $this->getFolderIdFromImapId($imapid);
                        $this->sinkstates_imap[$imapid] = $newstate;
                    }
                }
            }

            if (empty($notifications))
                sleep(9);
        }

        return $notifications;
    }


    /**----------------------------------------------------------------------------------------------------------
     * implemented DiffBackend methods
     */


    /**
     * Returns a list (array) of folders.
     *
     * @access public
     * @return array/boolean        false if the list could not be retrieved
     */
    public function GetFolderList() {
        /*-
         * each item has
         * - 'id' (for GetHierarchy and diff backend)
         * - 'mod' (for diff backend)
         */
        $folders = array();

        /* add virtual folders for PIM data */
        foreach (array(
            'VIRTUAL/calendar',
            'VIRTUAL/contacts',
          ) as $v) {
            $folders[] = array(
                'id' => $v,
                'mod' => $v,
                'parent' => 0,
              );
        }

        /* add real folders from IMAP */
        if (!$this->sk_mailboxes['folders'])
            return false;
        foreach ($this->sk_mailboxes['folders'] as $k => $v) {
            $folders[] = array(
                'id' => $this->convertImapId($k),
                'mod' => $v['displayname'],
                'parent' => $v['parent'] === false ? '0' :
                  $this->convertImapId($v['parent']),
                '~imap' => $v,
              );
        }

        util_debugJ($folders);

        return $folders;
    }

    /**
     * Returns an actual SyncFolder object
     *
     * @param string        $id           id of the folder
     *
     * @access public
     * @return object       SyncFolder with information
     */
    public function GetFolder($id) {
        $folder = new SyncFolder();
        $folder->serverid = $id;

        if ($id == 'VIRTUAL/calendar') {
            $folder->parentid = "0";
            $folder->displayname = "Calendar";
            $folder->type = SYNC_FOLDER_TYPE_APPOINTMENT;
            return $folder;
        } elseif ($id == 'VIRTUAL/contacts') {
            $folder->parentid = "0";
            $folder->displayname = "Contacts";
            $folder->type = SYNC_FOLDER_TYPE_CONTACT;
            return $folder;
        }

        // convert back to work on an imap-id
        $imapid = $this->getImapIdFromFolderId($id);

        if (!$imapid || !isset($this->sk_mailboxes['folders'][$imapid])) {
            util_debugJ('folder not found', $imapid);
            return false;
        }

        $fld = $this->sk_mailboxes['folders'][$imapid];

        $folder->parentid = $fld['parent'] === false ? '0' :
          $this->convertImapId($fld['parent']);
        $folder->displayname = $fld['displayname'];
        $folder->type = $fld['type'];

        return $folder;
    }

    /**
     * Returns folder stats. An associative array with properties is expected.
     *
     * @param string        $id             id of the folder
     *
     * @access public
     * @return array
     */
    public function StatFolder($id) {
        $folder = $this->GetFolder($id);

        $stat = array();
        $stat["id"] = $id;
        $stat["parent"] = $folder->parentid;
        $stat["mod"] = $folder->displayname;

        return $stat;
    }

    /**
     * Creates or modifies a folder
     * The folder type is ignored in IMAP, as all folders are Email folders
     *
     * @param string        $folderid       id of the parent folder
     * @param string        $oldid          if empty -> new folder created, else folder is to be renamed
     * @param string        $displayname    new folder name (to be created, or to be renamed to)
     * @param int           $type           folder type
     *
     * @access public
     * @return boolean                      status
     * @throws StatusException              could throw specific SYNC_FSSTATUS_* exceptions
     *
     */
    public function ChangeFolder($folderid, $oldid, $displayname, $type) {
        ZLog::Write(LOGLEVEL_INFO, sprintf("BackendSyncphony->ChangeFolder('%s','%s','%s','%s')", $folderid, $oldid, $displayname, $type));

        if ($folderid === '0') {
            // work around a BlackBerry bug
            $folderid = 'INBOX';
        } else {
            // resolve AS name of parent folder into IMAP name
            $folderid = $this->getImapIdFromFolderId($folderid);
        }

        // go to parent mailbox
        $this->imap_reopenFolder($folderid);

        // build name for new mailboxBackendMaildir
        $newname = $folderid . $this->serverdelimiter .
          mb_convert_encoding($displayname, 'UTF7-IMAP', 'UTF-8');

        $csts = false;
        // if $id is set => rename mailbox, otherwise create
        if ($oldid) {
            // rename doesn't work properly with IMAP
            // the activesync client doesn't support a 'changing ID'
            // TODO this would be solved by implementing hex ids (Mantis #459)
            //$csts = imap_renamemailbox($this->mbox, $this->server . XXX imap utf-7 encode(str_replace(".", $this->serverdelimiter, $oldid)), $this->server . $newname);
        }
        else {
            $csts = @imap_createmailbox($this->mbox, $this->server . $newname);
        }
        if ($csts) {
            $this->reloadFoldersFromIMAP();
            return $this->StatFolder($this->convertImapId($newname));
        }
        else
            return false;
    }

    /**
     * Deletes a folder
     *
     * @param string        $id
     * @param string        $parent         is normally false
     *
     * @access public
     * @return boolean                      status - false if e.g. does not exist
     * @throws StatusException              could throw specific SYNC_FSSTATUS_* exceptions
     *
     */
    public function DeleteFolder($id, $parentid){
        // TODO implement
        return false;
    }

    /**
     * Returns a list (array) of messages
     *
     * @param string        $folderid       id of the parent folder
     * @param long          $cutoffdate     timestamp in the past from which on messages should be returned
     *
     * @access public
     * @return array/false  array with messages or false if folder is not available
     */
    public function GetMessageList($folderid, $cutoffdate) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->GetMessageList('%s','%s')", $folderid, $cutoffdate));

        if (strpos($folderid, "VIRTUAL") !== false) {
            /* PIM data */
            if (strpos($folderid, "calendar") !== false)
                return $this->syncphony->getMessageList('calendar', 'Event');
            if (strpos($folderid, "contacts") !== false)
                return $this->syncphony->getMessageList('contact', 'Contact');
            return array();
        }

        $folderimap = $this->getImapIdFromFolderId($folderid);

        if ($folderimap == false)
            throw new StatusException("Folderid not found in cache", SYNC_STATUS_FOLDERHIERARCHYCHANGED);

        $messages = array();
        $this->imap_reopenFolder($folderimap, true);

        if (!imap_num_msg($this->mbox)) {
            /* no messages, avoid log warning */
            return $messages;
        }

        $sequence = "1:*";
        if ($cutoffdate > 0) {
            $search = @imap_search($this->mbox, "SINCE ". date("d-M-Y", $cutoffdate));
            if ($search !== false)
                $sequence = implode(",", $search);
        }
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->GetMessageList(): searching with sequence '%s'", $sequence));
        $overviews = @imap_fetch_overview($this->mbox, $sequence);

        if (!$overviews || !is_array($overviews)) {
            ZLog::Write(LOGLEVEL_WARN, sprintf("BackendSyncphony->GetMessageList('%s','%s'): Failed to retrieve overview: %s", $folderid, $cutoffdate, imap_last_error()));
            return $messages;
        }

        foreach ($overviews as $overview) {
            $date = "";
            $vars = get_object_vars($overview);
            if (array_key_exists("date", $vars)) {
                // message is out of range for cutoffdate, ignore it
                if ($this->cleanupDate($overview->date) < $cutoffdate)
                    continue;
                $date = $overview->date;
            }

            // cut of deleted messages
            if (array_key_exists("deleted", $vars) && $overview->deleted)
                continue;

            if (array_key_exists("uid", $vars)) {
                $message = array();
                $message["mod"] = $date;
                $message["id"] = $this->getServerIdFromImapId($folderid,
                  $overview->uid);
                // 'seen' aka 'read' is the only flag we want to know about
                $message["flags"] = 0;

                if (array_key_exists("seen", $vars) && $overview->seen)
                    $message["flags"] = 1;

                if ($message["id"])
                    array_push($messages, $message);
                else
                    util_debugJ('ERR', 'skipping message', $message);
            }
        }
        return $messages;
    }

    /*
     * Helper function to check whether we want to replace the message
     * body because it’s an event invitation, an encrypted message, or
     * something like that.
     *
     * $mail is the original message, $mdec the decoded message object
     * returns false or the replacement body text
     */
    function check_for_body_replacement(&$mail, &$mdec) {
        /* retrieve all message parts to work on */
        $parts = array();
        if (($have_body = isset($mdec->body)))
            $parts[] = $mdec->body;
        $have_part0 = false;
        $have_partn = false;
        if (isset($mdec->parts)) {
            $have_part0 = isset($mdec->parts[0]->body);
            foreach ($mdec->parts as $part)
                /* non-recursively here */
                if (isset($part->body)) {
                    $parts[] = $part->body;
                    $have_partn = true;
                }
        }
        if (!$have_body && !$have_part0 && !$have_partn) {
            /* split the message to get the actual body lines */
            $body = util_split_newlines($mail);
            while (array_shift($body))
                /* nothing */;
            $parts[] = trim(implode("\n", $body));
        }

        /* none of the above */
        return false;
    }

    /**
     * Returns the actual SyncXXX object type.
     *
     * @param string            $folderid           id of the parent folder
     * @param string            $id                 id of the message
     * @param ContentParameters $contentparameters  parameters of the requested message (truncation, mimesupport etc)
     *
     * @access public
     * @return object/false     false if the message could not be retrieved
     */
    public function GetMessage($folderid, $id, $contentparameters) {
        if (IMAP_TRUNCATION_LIMIT === true) {
            $truncsize = Utils::GetTruncSize($contentparameters->GetTruncation());
        } elseif (IMAP_TRUNCATION_LIMIT === false) {
            $truncsize = 2147483647;
        } else {
            $truncsize = (int)(IMAP_TRUNCATION_LIMIT);
        }
        $mimesupport = $contentparameters->GetMimeSupport();
        $bodypreference = $contentparameters->GetBodyPreference();
        $Mail_RFC822 = new Mail_RFC822();
        util_debugJ(array('truncsize'=>$truncsize,'mimesupport'=>$mimesupport,'bodypreference'=>$bodypreference));

        if (strpos($folderid, "VIRTUAL") !== false) {
            /* PIM data */
            if (strpos($folderid, "calendar") !== false)
                return $this->syncphony->getSyncItem('Event', $id);
            if (strpos($folderid, "contacts") !== false)
                return $this->syncphony->getSyncItem('Contact', $id);
            return false;
        }

        $folderImapid = $this->getImapIdFromFolderId($folderid);

        // Get flags, etc
        $stat = $this->StatMessage($folderid, $id);
        $iuid = $this->getImapIdFromServerId($folderid, $id);
        if (!$stat || !$iuid)
            return false;

        $this->imap_reopenFolder($folderImapid);
        $mail = @imap_fetchheader($this->mbox, $iuid, FT_UID) . @imap_body($this->mbox, $iuid, FT_PEEK | FT_UID);

        $mobj = new Mail_mimeDecode($mail);
        $message = $mobj->decode(array('decode_headers' => true, 'decode_bodies' => true, 'include_bodies' => true, 'charset' => 'utf-8'));

        $output = new SyncMail();

        //Select body type preference
        $bpReturnType = SYNC_BODYPREFERENCE_PLAIN;
        if ($bodypreference !== false) {
            $bpReturnType = Utils::GetBodyPreferenceBestMatch($bodypreference);
        }
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->GetMessage - getBodyPreferenceBestMatch: %d", $bpReturnType));

        // if “the device supports MIME (iPhone) and doesn't really understand HTML”
        if (in_array(SYNC_BODYPREFERENCE_MIME, $bodypreference))
            $bpReturnType = SYNC_BODYPREFERENCE_MIME;

        $plainBody = $this->getPlainBody($message);

        $body_replaced = false;
        if (($body = $this->check_for_body_replacement($mail, $message)) !== false) {
            $body_replaced = true;
            $plainBody = util_sanitise_multiline_submission(trim($body));
        }

        $output->messageclass = 'IPM.Note';
        $ct = strtolower(util_ifscalaror($message->headers['content-type'], '', ''));
        $is_smime = false;
        if (preg_match('!application/(x-)?pkcs7-mime!', $ct)) {
            /* S/MIME encrypted or signed opaque */
            $output->messageclass = 'IPM.Note.SMIME';
            $is_smime = true;
        } elseif (strpos($ct, 'multipart/signed') !== false &&
          preg_match('!application/(x-)?pkcs7-signature!', $ct)) {
            /* S/MIME signed transparent */
            $output->messageclass = 'IPM.Note.SMIME.MultipartSigned';
            $is_smime = true;
        }
        /*
         * S/MIME does not use multipart/encrypted, although PGP/MIME does;
         * probably, it also should use “IPM.Note.SMIME”; deferred because
         * neither PGP/MIME nor Inline PGP are specified for ActiveSync; we
         * would also need to prepare the PGP messages for it to work…
         */

        if ($is_smime || /* PGP/MIME or whatever else */
          strpos($ct, 'multipart/signed') !== false ||
          strpos($ct, 'multipart/encrypted') !== false ||
          /*XXX TODO: check for Inline PGP */ false) {
            /* required to be used for all crypto messages */
            $bpReturnType = SYNC_BODYPREFERENCE_MIME;
        }

        if ($bpReturnType != SYNC_BODYPREFERENCE_MIME) {
            /* kludge: force certain messages to MIME */
            if (isset($message->parts) &&
              /* except on Android which doesn’t handle MIME, client-side */
              Request::GetDeviceType() != 'Android') {
                $this->flattenMessage($message);
                /* those that contain message/rfc822 parts */
                if ($message->linear_contains_rfc822)
                    $bpReturnType = SYNC_BODYPREFERENCE_MIME;
            }
        }

        /* check for iMip */
        $has_iMip = false;
        $v = false;
        if ($bpReturnType != SYNC_BODYPREFERENCE_MIME) {
            $this->flattenMessage($message);
            if (count($message->linear_iMip) == 1) {
                /* yes! exactly one. */
                require_once('/usr/share/simkolab/bb/backend/syncphony/vobject.php');
                if (($v = vobject_parse($message->linear_iMip[0], $message->headers)) !== false)
                    $has_iMip = true;
            }
        }
        $has_iMip = $has_iMip ? vobject_check_iMip($v) : false;
        $iMip_attach = array();

        if ($has_iMip === true) {
            $output->messageclass = 'IPM.Schedule.Meeting.Request';

            $s = 'Termineinladung';
            if (isset($v->VEVENT->SEQUENCE) &&
              ($x = strval($v->VEVENT->SEQUENCE) - 1) > 0)
                $s .= " ($x. Update)";
            if (isset($v->VEVENT->ORGANIZER) && ($v->VEVENT->ORGANIZER)) {
                $x = vobject_vis_caladdress($v->VEVENT->ORGANIZER, false);
                $s .= ' von ' . $x[0];
            }
            $x = array(
                isset($v->VEVENT->DTSTAMP) ? vobject_convert_dt($v->VEVENT->DTSTAMP, $v) : '',
                isset($v->VEVENT->CREATED) ? vobject_convert_dt($v->VEVENT->CREATED, $v) : '',
                isset($v->VEVENT->{'LAST-MODIFIED'}) ? vobject_convert_dt($v->VEVENT->{'LAST-MODIFIED'}, $v) : '',
              );
            if ($x[0])
                $s .= ', gesendet am ' . $x[0];
            if ($x[1] && $x[1] != $x[0])
                $s .= ', Termin erstellt am ' . $x[1];
            if ($x[2] && $x[2] != $x[1] && $x[2] != $x[0])
                $s .= ', Terminzeitpunkt zuletzt geändert am ' . $x[2];
            if (isset($v->VEVENT->SUMMARY) && ($v->VEVENT->SUMMARY))
                $s .= "\nBetreff: " . $v->VEVENT->SUMMARY;
            if (isset($v->VEVENT->DTSTART))
                $s .= "\nVon: " . vobject_convert_dt($v->VEVENT->DTSTART, $v);
            else
                /* downgrade: missing required field */
                $output->messageclass = 'IPM.Schedule.Meeting';
            if (isset($v->VEVENT->DTEND))
                $s .= "\nBis: " . vobject_convert_dt($v->VEVENT->DTEND, $v, true);
            elseif (isset($v->VEVENT->DURATION) && isset($v->VEVENT->DTSTART))
                $s .= "\nBis: " . vobject_convert_dt($v->VEVENT->DTSTART, $v, true,
                  $v->VEVENT->DURATION->getDateInterval());
            elseif (isset($v->VEVENT->DURATION) && ($v->VEVENT->DURATION))
                $s .= "\nDauer: " . $v->VEVENT->DURATION;
            if (isset($v->VEVENT->LOCATION) && ($v->VEVENT->LOCATION))
                $s .= "\nOrt: " . $v->VEVENT->LOCATION;
            if (isset($v->VEVENT->GEO)) {
                $x = $v->VEVENT->GEO->getParts();
                if (($ns = $x[0]) < 0) {
                    $ns = -$ns;
                    $sns = 'S';
                } else
                    $sns = 'N';
                $tns = (int)$ns;
                $ns = ($ns - $tns) * 60 + 0.0005;
                if (($we = $x[1]) < 0) {
                    $we = -$we;
                    $swe = 'W';
                } else
                    $swe = 'E';
                $twe = (int)$we;
                $we = ($we - $twe) * 60 + 0.0005;
                $x = sprintf('%s %02d° %06.3F %s %03d° %06.3F - ' .
                  'http://www.openstreetmap.org/?mlat=%f&mlon=%f',
                  $sns, $tns, $ns, $swe, $twe, $we,
                  $x[0], $x[1]);
                $s .= "\nGeo: $x";
            }
            if (isset($v->VEVENT->STATUS) && ($v->VEVENT->STATUS)) {
                $x = $v->VEVENT->STATUS;
                switch (strtoupper($x)) {
                case 'TENTATIVE':
                    $x = 'Termin findet vielleicht statt';
                    break;
                case 'CONFIRMED':
                    $x = 'Termin findet definitiv statt';
                    break;
                case 'CANCELLED':
                    $x = 'Termin wurde abgesagt';
                    break;
                }
                $s .= "\nStatus: " . $x;
            }
            if (isset($v->VEVENT->RESOURCES) && ($v->VEVENT->RESOURCES))
                $s .= "\nRessourcen: " .
                  vobject_nicelist($v->VEVENT->RESOURCES);
            if (isset($v->VEVENT->PRIORITY) && ($v->VEVENT->PRIORITY))
                $s .= "\nPriorität: " . $v->VEVENT->PRIORITY .
                  ' (1 = höchste; 9 = niedrigste)';
            if (isset($v->VEVENT->URL) && ($v->VEVENT->URL))
                $s .= "\nLink: " . $v->VEVENT->URL;
            if (isset($v->VEVENT->DESCRIPTION) && ($v->VEVENT->DESCRIPTION))
                $s .= "\nBeschreibung:\n" .
                  util_linequote($v->VEVENT->DESCRIPTION, '| ');
            if (isset($v->VEVENT->ATTENDEE) && ($v->VEVENT->ATTENDEE))
                $s .= "\nTeilnehmer:\n" .
                  util_linequote(vobject_vis_caladdress($v->VEVENT->ATTENDEE,
                  true), '- ');
            if (isset($v->VEVENT->TRANSP) && ($v->VEVENT->TRANSP))
                $s .= "\nFree/Busy: " .
                  ($v->VEVENT->TRANSP == 'OPAQUE' ? 'beschäftigt' :
                  ($v->VEVENT->TRANSP == 'TRANSPARENT' ? 'frei' :
                  ('(' . $v->VEVENT->TRANSP . ')')));
            if (isset($v->VEVENT->{'RELATED-TO'}) && ($v->VEVENT->{'RELATED-TO'}))
                $s .= "\nRelation zu:" .
                  util_linequote($v->VEVENT->{'RELATED-TO'}, '* ', true);
            if (isset($v->VEVENT->CLASS) && ($v->VEVENT->CLASS))
                $s .= "\nKlasse: " . $v->VEVENT->CLASS;
            if (isset($v->VEVENT->CATEGORIES) && ($v->VEVENT->CATEGORIES))
                $s .= "\nKategorien: " .
                  vobject_nicelist($v->VEVENT->CATEGORIES);
            if (isset($v->VEVENT->CONTACT) && ($v->VEVENT->CONTACT))
                $s .= "\nBitte kontaktieren Sie bei Rückfragen:" .
                  util_linequote($v->VEVENT->CONTACT, '- ', true);
            if (isset($v->VEVENT->COMMENT) && ($v->VEVENT->COMMENT))
                $s .= "\nBemerkungen des Einladenden direkt an Sie:\n" .
                  util_linequote($v->VEVENT->COMMENT, '| ');
            if (isset($v->VEVENT->{'REQUEST-STATUS'}) && ($v->VEVENT->{'REQUEST-STATUS'}))
                $s .= "\nAnfragestatus:\n" .
                  util_linequote($v->VEVENT->{'REQUEST-STATUS'}, '* ');
            if (isset($v->VEVENT->ATTACH)) {
                $s .= "\nDer Einladende hat Dateien angehängt:";
                $x = 0;
                while ($x < count($v->VEVENT->ATTACH)) {
                    /* do not use util_ifsetor() here */
                    /* see comments on that function for why */
                    /* we must not create ['VALUE'] = NULL if unset */
                    if (isset($v->VEVENT->ATTACH[$x]['VALUE']) &&
                      $v->VEVENT->ATTACH[$x]['VALUE'] == 'BINARY') {
                        $s .= sprintf("\n%d. %s (%d KiB)", $x + 1, (
                          (isset($v->VEVENT->ATTACH[$x]['X-LABEL']) &&
                          ($v->VEVENT->ATTACH[$x]['X-LABEL'])) ?
                          ('Datei: "' . $v->VEVENT->ATTACH[$x]['X-LABEL'] . '"') :
                          'namenslose Datei'),
                          (int)((strlen($v->VEVENT->ATTACH[$x]) + 1023) / 1024));
                        $iMip_attach[] = $x;
                    } else
                        $s .= "\n" . ($x + 1) . '. Link: ' .
                          $v->VEVENT->ATTACH[$x];
                    ++$x;
                }
            }
            $mis = 'Aufgrund fehlender Angaben';

            $is_recurring = false;
            if (isset($v->VEVENT->EXDATE) || isset($v->VEVENT->RDATE) ||
              isset($v->VEVENT->RRULE) || isset($v->VEVENT->{'RECURRENCE-ID'}))
                $is_recurring = true;

            /* check whether we can sync this at all */
            if ($output->messageclass == 'IPM.Schedule.Meeting.Request' &&
              /* does not affect them */ !$is_recurring) {
                $endtime = isset($v->VEVENT->DTEND) ?
                  vobject_dt2timet($v->VEVENT->DTEND, $v) :
                  (isset($v->VEVENT->DURATION) ?
                  vobject_dt2timet($v->VEVENT->DTSTART, $v,
                  $v->VEVENT->DURATION->getDateInterval()) : NULL);
                if ($endtime === NULL)
                    $endtime = vobject_dt2timet($v->VEVENT->DTSTART, $v);
                /* the “else” here is bogus, but that’s what kolab-ws does */
                else if (!$v->VEVENT->DTSTART->hasTime())
                    /* all-day event, close enough even if DST switch due to grace */
                    $endtime += 86400;
                if ($endtime < ($this->syncphony->currentTimestamp -
                  (DEFAULT_SYNC_WINDOW_PAST_DAYS * 86400) - /* grace */ 10000)) {
                    /* downgrade: too old to be synced */
                    $output->messageclass = 'IPM.Schedule.Meeting';
                    $mis = 'Weil der Termin zu weit in der Vergangenheit liegt';
                }
            }
            /* future days thankfully currently ignored by kolab-ws */

            /* missing: http://tools.ietf.org/html/rfc5545#section-3.8.5 */
            if ($is_recurring) {
                /* downgrade: recurrence not supported yet */
                $output->messageclass = 'IPM.Schedule.Meeting';
                $mis = 'Weil wiederkehrende Termine noch nicht synchronisierbar sind';
            }
            /* ignored: http://tools.ietf.org/html/rfc5545#section-3.6.6 */
            $plainBody = $s . "\n\n" .
              ($output->messageclass === 'IPM.Schedule.Meeting.Request' ?
              "Alternativ können Sie die Einladung auch" :
              ($mis . " können Sie die Einladung nur")) .
              " im Kolab-Client einsehen und bearbeiten.\n" .
              ($plainBody ? (
              "\n------------------------------------------------------------------\n\n" .
              $plainBody) : '');
        } elseif ($has_iMip !== false) {
            if ($has_iMip[0])
                $output->messageclass = $has_iMip[0];
            $plainBody = "Sehr geehrter Nutzer,\n\n" .
              "es ist " . $has_iMip[1] . "\nfür Sie eingegangen.\n" .
              "Sie können die Mitteilung im Kolab-Client einsehen und bearbeiten.\n" .
              ($plainBody ? (
              "\n------------------------------------------------------------------\n\n" .
              $plainBody) : '');
        }

        /* BlackBerry handles iMip natively */
        if ($has_iMip && (Request::GetDeviceType() == 'BlackBerry')) {
            /* send the MIME message so it can do that */
            $bpReturnType = SYNC_BODYPREFERENCE_MIME;
            /* the messageclass is set above, plainBody is ignored for MIME */
        }

        if (!$plainBody) {
            /* no msg body at all? send the raw MIME! */
            $bpReturnType = SYNC_BODYPREFERENCE_MIME;
        }

        if ($bpReturnType == SYNC_BODYPREFERENCE_RTF) {
            ZLog::Write(LOGLEVEL_DEBUG, "BackendSyncphony->GetMessage RTF Format NOT CHECKED");
            $bpReturnType = SYNC_BODYPREFERENCE_PLAIN;
        }

        if ($bpReturnType == SYNC_BODYPREFERENCE_MIME) {
            /* possibly reset $body_replaced if $bpReturnType changed */
            $body_replaced = false;

            /* harmonise capitalisation */
            $mail = preg_replace("#(?<=\n)mime-version:[\t ]*1.0(?=\r|\n)#i",
                'MIME-Version: 1.0', $mail, 1);
        }

        if (Request::GetProtocolVersion() >= 12.0) {
            $output->asbody = new SyncBaseBody();

            switch ($bpReturnType) {
            case SYNC_BODYPREFERENCE_PLAIN:
                $data = $plainBody;
                $output->nativebodytype = 1;
                break;
            case SYNC_BODYPREFERENCE_HTML:
                /* ugly, but some devices vomit if served plain */
                $data = '<html><head>' /* no title tag! */ . '</head><body>' .
                  Utils::ConvertTextToHtml($plainBody) . '</body></html>';
                $output->nativebodytype = 2;
                break;
            case SYNC_BODYPREFERENCE_MIME:
                $data = $mail;
                break;
            default:
                $data = "no message body for bpReturnType " . $bpReturnType;
                $bpReturnType = SYNC_BODYPREFERENCE_PLAIN;
                break;
            }

            Utils::CheckAndFixEncoding($data);
            $data = util_sanitise_multiline_submission(trim($data)) . "\015\012";
            $output->asbody->estimatedDataSize = strlen($data);

            // truncate body, if requested
            if ($output->asbody->estimatedDataSize > $truncsize &&
              $bpReturnType != SYNC_BODYPREFERENCE_MIME) {
                $data = Utils::Utf8_truncate($data, $truncsize, ($bpReturnType == SYNC_BODYPREFERENCE_HTML));
                $output->asbody->truncated = 1;
            }

            $output->asbody->data = StringStreamWrapper::Open($data, ($bpReturnType == SYNC_BODYPREFERENCE_HTML));
            $output->asbody->type = $bpReturnType;

            $bpo = $contentparameters->BodyPreference($output->asbody->type);
            if (!$body_replaced && Request::GetProtocolVersion() >= 14.0 && $bpo->GetPreview()) {
                Utils::CheckAndFixEncoding($plainBody);
                $output->asbody->preview = Utils::Utf8_truncate(util_sanitise_multiline_submission(trim($plainBody)) . "\015\012", $bpo->GetPreview());
            }
        } else { // ASV_2.5
            $data = $bpReturnType == SYNC_BODYPREFERENCE_MIME ? $mail : $plainBody;
            Utils::CheckAndFixEncoding($data);
            $data = util_sanitise_multiline_submission(trim($data)) . "\015\012";
            $dlen = strlen($data);
            if ($dlen > $truncsize && $bpReturnType != SYNC_BODYPREFERENCE_MIME) {
                $data = Utils::Utf8_truncate($data, $truncsize);
                $dtrunc = 1;
            } else {
                $dtrunc = 0;
            }
            $data = StringStreamWrapper::Open($data);

            if ($bpReturnType == SYNC_BODYPREFERENCE_MIME) {
                /* can this even happen? pref seems unimplemented for 2.5 */
                $output->mimedata = $data;
                $output->mimetruncated = $dtrunc;
                $output->mimesize = $dlen;
            } else {
                $output->body = $data;
                if (($output->bodytruncated = $dtrunc))
                    $output->bodysize = $dlen;
            }
        }

        if (isset($message->headers["date"])) {
            if (is_array($message->headers["date"])) {
                ZLog::Write(LOGLEVEL_DEBUG,
                  "BackendSyncphony->GetMessage(): multiple Date headers, using first one: " .
                  implode('; ', $message->headers["date"]));
                $message->headers["date"] = $message->headers["date"][0];
            }
            $output->datereceived = $this->cleanupDate($message->headers["date"]);
        } else
            $output->datereceived = NULL;
        if (isset($message->headers["subject"])) {
            if (is_array($message->headers["subject"])) {
                $message->headers["subject"] = implode('|', $message->headers["subject"]);
                ZLog::Write(LOGLEVEL_DEBUG,
                  "BackendSyncphony->GetMessage(): multiple Subject headers, concatenating: " .
                  $message->headers["subject"]);
            }
            $output->subject = $message->headers["subject"];
        } else
            $output->subject = "";
        Utils::CheckAndFixEncoding($output->subject);

        if (isset($message->headers["from"])) {
            $user = $message->headers["from"];
            while (is_array($user)) {
                $address = array_shift($user);
                ZLog::Write(LOGLEVEL_DEBUG,
                  "BackendSyncphony->GetMessage(): multiple From headers, using first (" .
                  print_r($address, true) . "), dropping remaining " .
                  print_r($user, true));
                $user = $address;
            }
            $address = $Mail_RFC822->parseAddressList($user);
            $addr = $address[0];
            $address = $addr->mailbox . "@" . $addr->host;
            $user = trim($addr->personal, " \t\n\r\0\x0B\"");
            if ($user && trim($address, " \t\n\r\0\x0B\"") != $user)
                $address = '"' . $user . '" <' . $address . '>';
            $output->from = $address;
            Utils::CheckAndFixEncoding($output->from);
        } else
            $output->from = NULL;
        $output->read = $stat["flags"];

        if ($body_replaced) {
            $user = $this->username;
            if ($user && (strpos($user, '@') === false))
                $user .= '@' . KOLAB_DOMAIN;
            $output->from = util_fixutf8($user);
        }

        if (isset($message->headers["thread-topic"]) &&
          !is_array($message->headers["thread-topic"])) {
            $output->threadtopic = util_fixutf8($message->headers["thread-topic"]);
        } else {
            $output->threadtopic = preg_replace('/^((re|fwd?|aw|wg|antw\.?|antwort|wtglt\.|wtrlt|auto)(\^[0-9]+|\[[0-9]+])?:\s*|\[[a-zA-Z0-9_-]+]\s+)+/i',
              '', $output->subject);
        }

        // Language Code Page ID: http://msdn.microsoft.com/en-us/library/windows/desktop/dd317756%28v=vs.85%29.aspx
        $output->internetcpid = INTERNET_CPID_UTF8;
        if (Request::GetProtocolVersion() >= 12.0) {
            $output->contentclass = "urn:content-classes:message";
        }

        $toaddr = $ccaddr = $replytoaddr = array();
        if(isset($message->headers["to"]))
            $toaddr = $Mail_RFC822->parseAddressList($message->headers["to"]);
        if(isset($message->headers["cc"]))
            $ccaddr = $Mail_RFC822->parseAddressList($message->headers["cc"]);
        if(isset($message->headers["reply-to"]))
            $replytoaddr = $Mail_RFC822->parseAddressList($message->headers["reply-to"]);

        $output->to = array();
        $output->cc = array();
        $output->reply_to = array();
        foreach (array("to" => $toaddr, "cc" => $ccaddr, "reply_to" => $replytoaddr) as $type => $addrlist) {
            foreach ($addrlist as $addr) {
                $address = $addr->mailbox . "@" . $addr->host;
                $name = trim($addr->personal ? : $address, " \t\n\r\0\x0B\"");

                if ($type == "to")
                    $output->displayto = (isset($output->displayto) ?
                      ($output->displayto . ";") : "") . $name;

                if ($name && trim($address, " \t\n\r\0\x0B\"") == $name)
                    $fulladdr = $address;
                else
                    $fulladdr = '"' . $name . '" <' . $address . '>';

                Utils::CheckAndFixEncoding($fulladdr);
                array_push($output->$type, $fulladdr);
            }
        }
        Utils::CheckAndFixEncoding($output->displayto);

        // convert mime-importance to AS-importance
        if (isset($message->headers["x-priority"]) && !is_array($message->headers["x-priority"])) {
            $mimeImportance = preg_replace("/\D+/", "", $message->headers["x-priority"]);
            //MAIL 1 - most important, 3 - normal, 5 - lowest
            //AS 0 - low, 1 - normal, 2 - important
            if ($mimeImportance > 3)
                $output->importance = 0;
            if ($mimeImportance == 3)
                $output->importance = 1;
            if ($mimeImportance < 3)
                $output->importance = 2;
        } else {
            $output->importance = 1;
        }

        /* attach iMip attachments */
        foreach ($iMip_attach as $x) {
            if (!isset($v->VEVENT->ATTACH[$x]['X-LABEL']) ||
              !($attname = $v->VEVENT->ATTACH[$x]['X-LABEL']))
                $attname = sprintf('unknown iMip attachment %d.bin', $x);
            $sz = strlen($v->VEVENT->ATTACH[$x]);
            if (IMAP_ATTACHMENT_MAXSIZE && $sz > IMAP_ATTACHMENT_MAXSIZE) {
                ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->GetMessage('%s', '%s') iMip attachment %d ('%s') size %d longer than hard limit %d, not announcing", $folderid, $id, $x, $attname, $sz, IMAP_ATTACHMENT_MAXSIZE));
                continue;
            }
            if (Request::GetProtocolVersion() >= 12.0) {
                if (!isset($output->asattachments) || !is_array($output->asattachments))
                    $output->asattachments = array();
                $attachment = new SyncBaseAttachment();
                $attachment->estimatedDataSize = $sz;
                $attachment->displayname = util_fixutf8($attname);
                $attachment->filereference = util_fixutf8($folderid . ':' . $iuid . ':i:' . $x);
                $attachment->method = 1; //Normal attachment
                $attachment->isinline = 0;
                array_push($output->asattachments, $attachment);
            } else { //ASV_2.5
                if (!isset($output->attachments) || !is_array($output->attachments))
                    $output->attachments = array();
                $attachment = new SyncAttachment();
                $attachment->attsize = $sz;
                $attachment->displayname = util_fixutf8($attname);
                $attachment->attname = util_fixutf8($folderid . ':' . $iuid . ':i:' . $x);
                $attachment->attmethod = 1;
                array_push($output->attachments, $attachment);
            }
        }

        // Attachments are also needed for MIME messages
        $this->flattenMessage($message);
        if (!$body_replaced && $message->linear_attachments) {
            for ($i = 0; $i < count($message->linear_attachments); ++$i) {
                $part = $message->linear_attachments[$i];
                $attname = util_ifscalaror($part->d_parameters['filename']);
                if (!$attname)
                    $attname = util_ifscalaror($part->ctype_parameters['name']);
                if (!$attname)
                    $attname = util_ifscalaror($part->headers['content-description']);
                if (!$attname)
                    $attname = sprintf('unknown MIME attachment %d.bin', $i);

                $sz = util_ifscalaror($part->d_parameters['size'], 0, 0);
                if (IMAP_ATTACHMENT_MAXSIZE && $sz > IMAP_ATTACHMENT_MAXSIZE) {
                    ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->GetMessage('%s', '%s') attachment %d ('%s') %s size %d longer than hard limit %d, not announcing", $folderid, $id, $i, $attname, 'projected', $sz, IMAP_ATTACHMENT_MAXSIZE));
                    continue;
                }
                if (isset($part->body) && ($sz = strlen($part->body)) && IMAP_ATTACHMENT_MAXSIZE && $sz > IMAP_ATTACHMENT_MAXSIZE) {
                    ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->GetMessage('%s', '%s') attachment %d ('%s') %s size %d longer than hard limit %d, not announcing", $folderid, $id, $i, $attname, 'bodily', $sz, IMAP_ATTACHMENT_MAXSIZE));
                    continue;
                }

                if (Request::GetProtocolVersion() >= 12.0) {
                    if (!isset($output->asattachments) || !is_array($output->asattachments))
                        $output->asattachments = array();

                    $attachment = new SyncBaseAttachment();
                    $attachment->estimatedDataSize = $sz;
                    $attachment->displayname = util_fixutf8($attname);
                    $attachment->filereference = util_fixutf8($folderid . ":" . $iuid . ":!:" . $i);
                    $attachment->method = 1; //Normal attachment
                    $attachment->contentid = util_fixutf8(str_replace("<", "", str_replace(">", "", util_ifscalaror($part->headers['content-id'], '', ''))));
                    $attachment->isinline = util_ifsetor($part->disposition) == 'inline' ? 1 : 0;
                    array_push($output->asattachments, $attachment);
                } else { //ASV_2.5
                    if (!isset($output->attachments) || !is_array($output->attachments))
                        $output->attachments = array();

                    $attachment = new SyncAttachment();
                    $attachment->attsize = $sz;
                    $attachment->displayname = util_fixutf8($attname);
                    $attachment->attname = util_fixutf8($folderid . ":" . $iuid . ":!:" . $i);
                    $attachment->attmethod = 1;
                    $attachment->attoid = util_fixutf8(str_replace("<", "", str_replace(">", "", util_ifscalaror($part->headers['content-id'], '', ''))));
                    array_push($output->attachments, $attachment);
                }
            }
        }

        // attach MeetingRequest
        if ($output->messageclass === 'IPM.Schedule.Meeting.Request') {
            /*-
             * fields guaranteed to exist here: METHOD, VEVENT->
             * UID DTSTART
             */
            $s = new SyncMeetingRequest();
            $s->dtstamp = isset($v->VEVENT->DTSTAMP) ?
              vobject_dt2timet($v->VEVENT->DTSTAMP, $v) : NULL;
            $s->alldayevent = $v->VEVENT->DTSTART->hasTime() ? 0 : 1;
            $tstart = vobject_dt2dt($v->VEVENT->DTSTART, $v);
            $tend = isset($v->VEVENT->DTEND) ?
              vobject_dt2dt($v->VEVENT->DTEND, $v) :
              (isset($v->VEVENT->DURATION) ?
              vobject_dt2dt($v->VEVENT->DTSTART, $v,
              $v->VEVENT->DURATION->getDateInterval()) : NULL);
            if ($s->alldayevent) {
                $s->starttime = syncphony::renderDatestamp($tstart);
                $s->endtime = syncphony::renderDatestamp($tend, 1);
            } else {
                $s->starttime = syncphony::renderTimestamp($tstart);
                $s->endtime = syncphony::renderTimestamp($tend);
            }
            /*XXX >0 when recurring event! required field! */
            $s->instancetype = 0;
            $x = '';
            if (isset($v->VEVENT->GEO)) {
                $x = $v->VEVENT->GEO->getParts();
                if (($ns = $x[0]) < 0) {
                    $ns = -$ns;
                    $sns = 'S';
                } else
                    $sns = 'N';
                $tns = (int)$ns;
                $ns = ($ns - $tns) * 60 + 0.0005;
                if (($we = $x[1]) < 0) {
                    $we = -$we;
                    $swe = 'W';
                } else
                    $swe = 'E';
                $twe = (int)$we;
                $we = ($we - $twe) * 60 + 0.0005;
                $x = sprintf('%s %02d° %06.3F %s %03d° %06.3F - ' .
                  'http://www.openstreetmap.org/?mlat=%f&mlon=%f',
                  $sns, $tns, $ns, $swe, $twe, $we,
                  $x[0], $x[1]);
            }
            if (isset($v->VEVENT->LOCATION) && ($v->VEVENT->LOCATION))
                $x = ($x ? ($x . ' - ') : '') . $v->VEVENT->LOCATION;
            if ($x)
                $s->location = util_fixutf8($x);
            if (isset($v->VEVENT->ORGANIZER) && ($v->VEVENT->ORGANIZER)) {
                $x = vobject_vis_caladdress($v->VEVENT->ORGANIZER, false);
                $s->organizer = util_fixutf8($x[0]);
            }
            // optional recurrenceid (time_t)
            /* we do not import reminder from external sources */
            $s->responserequested = 1;
            if (isset($v->VEVENT->ORGANIZER) && util_emailcase($this->username) ==
              util_emailcase(trim(str_ireplace('mailto:', '', strval($v->VEVENT->ORGANIZER)))))
                /* we are the organiser */
                $s->responserequested = 0;
            // optional recurrences (container)
            $x = strtolower(isset($v->VEVENT->CLASS) ? $v->VEVENT->CLASS : '');
            $s->sensitivity = (!$x || $x == 'public') ? 0 :
              ($x == 'confidential' ? 3 : 2);
            $x = strtolower(isset($v->VEVENT->TRANSP) ? $v->VEVENT->TRANSP : '');
            $s->busystatus = $x == 'transparent' ? 0 : 2;
            $x = vobject_dt2tz($v->VEVENT->DTSTART, $v);
            $s->timezone = tz2as($x, $s->starttime);
            $s->globalobjid = base64_encode(Utils::GetOLUidFromICalUid($v->VEVENT->UID));
            if (Request::GetProtocolVersion() >= 14.0)
                $s->disallownewtimeproposal = 0;

            /* check for error returns on required fields */
            if ($s->starttime === NULL || $s->dtstamp === NULL ||
              $s->endtime === NULL)
                /* revert to non-invite */
                $output->messageclass = 'IPM.Schedule.Meeting';
            else
                /* actually add the invite */
                $output->meetingrequest = $s;
        }

        // unset mimedecoder & mail
        unset($mobj);
        unset($mail);
        return $output;
    }

    /**
     * Ensure $m->linear_* contains parts flattened
     */
    public function flattenMessage(&$m) {
        if (isset($m->linear_contains_rfc822))
            return;
        $m->linear_contains_rfc822 = false;
        $m->linear_body_parts = array();
        $m->linear_attachments = array();
        $m->linear_iMip = array();
        $this->flattenMessagePart($m, $m);
    }
    protected function flattenMessagePart(&$m, &$part) {
        $ct = sprintf('%s/%s',
          util_ifscalaror($part->ctype_primary, '', ''),
          util_ifscalaror($part->ctype_secondary, '', ''));
        /* fall through the switch to handle as single attachment */
        switch ($ct) {

        /* single plaintext body */
        case '/':
        case 'text/':
        case 'text/plain':
            if (strtolower(util_ifsetor($part->disposition, '')) != 'attachment') {
                if (isset($part->body))
                    $m->linear_body_parts[] = array('plain' => $part->body);
                return;
            }
            /* single attachment */
            break;

        /* single HTML body */
        case 'text/html':
            if (strtolower(util_ifsetor($part->disposition, '')) != 'attachment') {
                if (isset($part->body))
                    $m->linear_body_parts[] = array('html' => $part->body);
                return;
            }
            /* single attachment */
            break;

        /* single iMip message */
        case 'text/calendar':
            if (isset($part->body))
                $m->linear_iMip[] = $part->body;
            return;

        /* mixed plaintext, HTML, rest ignored */
        case 'multipart/alternative':
        /* mixed plaintext, HTML, rest as attachments */
        case 'multipart/parallel':
            if (util_ifsetor($part->body)) {
                if (!isset($part->parts) || !is_array($part->parts)) {
                    /* fall through to single attachment */
                    break;
                }
                ZLog::Write(LOGLEVEL_DEBUG,
                  sprintf('Unexpected body in %s part in MID %s',
                  $ct, util_ifsetor($m->headers['message-id'], '(unknown)')));
            }
            if (!isset($part->parts) || !is_array($part->parts))
                return;
            $bparts = array();
            $oparts = array();
            $mparts = array();
            foreach ($part->parts as $spart) {
                if (strtolower(util_ifsetor($spart->disposition, '')) == 'attachment') {
                    $oparts[] = $spart;
                    continue;
                }
                switch (sprintf('%s/%s',
                  util_ifscalaror($spart->ctype_primary, '', ''),
                  util_ifscalaror($spart->ctype_secondary, '', ''))) {
                case '/':
                case 'text/':
                case 'text/plain':
                    if (isset($bparts['plain'])) {
                        /* multiple plain bodies, ignore */
                        goto handle_as_multipart_mixed;
                    }
                    if (isset($spart->body))
                        $bparts['plain'] = $spart;
                    break;
                case 'text/html':
                    if (isset($bparts['html'])) {
                        /* multiple HTML bodies, ignore */
                        goto handle_as_multipart_mixed;
                    }
                    if (isset($spart->body))
                        $bparts['html'] = $spart;
                    break;
                case 'text/calendar':
                    if (isset($spart->body))
                        $mparts[] = $spart;
                    break;
                default:
                    if ($ct == 'multipart/parallel')
                        $oparts[] = $spart;
                    break;
                }
            }

            /* either plain or HTML set, or iMip? if not… */
            if (!$bparts && !$mparts)
                goto handle_as_multipart_mixed;

            /* put plain and/or HTML parts */
            $bpart = array();
            foreach ($bparts as $x => $spart)
                $bpart[$x] = $spart->body;
            $m->linear_body_parts[] = $bpart;

            /* put iMip parts */
            foreach ($mparts as $spart)
                $m->linear_iMip[] = $spart->body;

            /* put other parts, if any */
            foreach ($oparts as $spart)
                $this->flattenMessagePart($m, $spart);

            return;

        /* multiple parts */
        case 'multipart/digest':
        case 'multipart/mixed':
        case 'multipart/related':
        case 'multipart/signed':
 handle_as_multipart_mixed:
            if (util_ifsetor($part->body)) {
                if (!isset($part->parts) || !is_array($part->parts)) {
                    /* fall through to single attachment */
                    break;
                }
                ZLog::Write(LOGLEVEL_DEBUG,
                  sprintf('Unexpected body in %s part in MID %s',
                  $ct, util_ifsetor($m->headers['message-id'], '(unknown)')));
            }
            if (isset($part->parts) && is_array($part->parts))
                foreach ($part->parts as $spart)
                    $this->flattenMessagePart($m, $spart);
            return;

        /* single attachment */
        case 'multipart/encrypted':
            break;

        /* complete eMail */
        case 'message/rfc822':
            $m->linear_contains_rfc822 = true;
            goto handle_as_multipart_mixed;

        /* weird */
        case 'message/partial':
        case 'message/external-body':
            ZLog::Write(LOGLEVEL_INFO,
              sprintf('Unhandled message subpart %s in MID %s, ignoring',
              $ct, util_ifsetor($m->headers['message-id'], '(unknown)')));
            return;

        /* everything else is a single attachment */
        }
        $m->linear_attachments[] = $part;
    }

    /**
     * Returns message stats, analogous to the folder stats from StatFolder().
     *
     * @param string        $folderid       id of the folder
     * @param string        $id             id of the message
     *
     * @access public
     * @return array/boolean
     */
    public function StatMessage($folderid, $id) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->StatMessage('%s','%s')", $folderid, $id));
        if (strpos($folderid, "VIRTUAL") !== false) {
            /* PIM data */
            return syncphony::getStatsFromSyncphonyID($id);
        }
        $folderImapid = $this->getImapIdFromFolderId($folderid);
        $iuid = $this->getImapIdFromServerId($folderid, $id);

        $this->imap_reopenFolder($folderImapid);
        $overview = @imap_fetch_overview($this->mbox, $iuid, FT_UID);

        if (!$iuid || !$overview) {
            ZLog::Write(LOGLEVEL_WARN, sprintf("BackendSyncphony->StatMessage('%s','%s'): Failed to retrieve overview: %s", $folderid, $id, imap_last_error()));
            return false;
        }

        // check if variables for this overview object are available
        $vars = get_object_vars($overview[0]);

        // without uid it's not a valid message
        if (!array_key_exists('uid', $vars))
            return false;

        $entry = array();
        $entry["mod"] = array_key_exists("date", $vars) ? $overview[0]->date : "";
        $entry["id"] = $this->getServerIdFromImapId($folderid, $overview[0]->uid);
        // 'seen' aka 'read' is the only flag we want to know about
        $entry["flags"] = 0;

        if (array_key_exists('seen', $vars) && $overview[0]->seen)
            $entry["flags"] = 1;

        if (!$entry["id"]) {
            util_debugJ('ERR', 'skipping message', $entry);
            $entry = false;
        }

        return $entry;
    }

    /**
     * Called when a message has been changed on the mobile.
     * Added support for FollowUp flag
     *
     * @param string              $folderid            id of the folder
     * @param string              $id                  id of the message
     * @param SyncXXX             $message             the SyncObject containing a message
     * @param ContentParameters   $contentParameters
     *
     * @access public
     * @return array                        same return value as StatMessage()
     * @throws StatusException              could throw specific SYNC_STATUS_* exceptions
     */
    public function ChangeMessage($folderid, $id, $message, $contentParameters) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->ChangeMessage('%s','%s','%s')", $folderid, $id, get_class($message)));

        if (strpos($folderid, "VIRTUAL") !== false) {
            /* PIM data */
            if (strpos($folderid, "calendar") !== false)
                return $this->syncphony->changeItem('Event', $id, $message);
            elseif (strpos($folderid, "contacts") !== false)
                return $this->syncphony->changeItem('Contact', $id, $message);
            return $this->StatMessage($folderid, $id);
        }

        $iuid = $this->getImapIdFromServerId($folderid, $id);

        // TODO this could throw several StatusExceptions like e.g. SYNC_STATUS_OBJECTNOTFOUND, SYNC_STATUS_SYNCCANNOTBECOMPLETED

        // TODO SyncInterval check + ContentParameters
        // see https://jira.zarafa.com/browse/ZP-258 for details
        // before changing the message, it should be checked if the message is in the SyncInterval
        // to determine the cutoffdate use Utils::GetCutOffDate($contentparameters->GetFilterType());
        // if the message is not in the interval an StatusException with code SYNC_STATUS_SYNCCANNOTBECOMPLETED should be thrown

        if ($iuid && isset($message->flag)) {
            ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->ChangeMessage('Setting flag')"));

            $folderImapid = $this->getImapIdFromFolderId($folderid);

            $this->imap_reopenFolder($folderImapid);

            if (isset($message->flag->flagstatus) && $message->flag->flagstatus == 2) {
                ZLog::Write(LOGLEVEL_DEBUG, "Set On FollowUp -> IMAP Flagged");
                $status = @imap_setflag_full($this->mbox, $iuid, "\\Flagged", ST_UID);
            }
            else {
                ZLog::Write(LOGLEVEL_DEBUG, "Clearing Flagged");
                $status = @imap_clearflag_full($this->mbox, $iuid, "\\Flagged", ST_UID);
            }

            if ($status) {
                ZLog::Write(LOGLEVEL_DEBUG, "Flagged changed");
            }
            else {
                ZLog::Write(LOGLEVEL_DEBUG, "Flagged failed");
            }
        }

        return $this->StatMessage($folderid, $id);
    }

    /**
     * Changes the 'read' flag of a message on disk
     *
     * @param string              $folderid            id of the folder
     * @param string              $id                  id of the message
     * @param int                 $flags               read flag of the message
     * @param ContentParameters   $contentParameters
     *
     * @access public
     * @return boolean                      status of the operation
     * @throws StatusException              could throw specific SYNC_STATUS_* exceptions
     */
    public function SetReadFlag($folderid, $id, $flags, $contentParameters) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->SetReadFlag('%s','%s','%s')", $folderid, $id, $flags));
        $folderImapid = $this->getImapIdFromFolderId($folderid);

        // TODO SyncInterval check + ContentParameters
        // see https://jira.zarafa.com/browse/ZP-258 for details
        // before setting the read flag, it should be checked if the message is in the SyncInterval
        // to determine the cutoffdate use Utils::GetCutOffDate($contentparameters->GetFilterType());
        // if the message is not in the interval an StatusException with code SYNC_STATUS_OBJECTNOTFOUND should be thrown

        $this->imap_reopenFolder($folderImapid);
        if (!($iuid = $this->getImapIdFromServerId($folderid, $id)))
            return false;

        if ($flags == 0) {
            // set as "Unseen" (unread)
            $status = @imap_clearflag_full($this->mbox, $iuid, "\\Seen", ST_UID);
        } else {
            // set as "Seen" (read)
            $status = @imap_setflag_full($this->mbox, $iuid, "\\Seen", ST_UID);
        }

        return $status;
    }

    /**
     * Called when the user has requested to delete (really delete) a message
     *
     * @param string              $folderid             id of the folder
     * @param string              $id                   id of the message
     * @param ContentParameters   $contentParameters
     *
     * @access public
     * @return boolean                      status of the operation
     * @throws StatusException              could throw specific SYNC_STATUS_* exceptions
     */
    public function DeleteMessage($folderid, $id, $contentParameters) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->DeleteMessage('%s','%s')", $folderid, $id));
        if (strpos($folderid, "VIRTUAL") !== false) {
            /* PIM data */
            if (strpos($folderid, "calendar") !== false)
                return $this->syncphony->deleteItem($id, 'Event');
            if (strpos($folderid, "contacts") !== false)
                return $this->syncphony->deleteItem($id, 'Contact');
            return false;
        }
        $folderImapid = $this->getImapIdFromFolderId($folderid);
        if (!($iuid = $this->getImapIdFromServerId($folderid, $id)))
            return false;

        // TODO SyncInterval check + ContentParameters
        // see https://jira.zarafa.com/browse/ZP-258 for details
        // before deleting the message, it should be checked if the message is in the SyncInterval
        // to determine the cutoffdate use Utils::GetCutOffDate($contentparameters->GetFilterType());
        // if the message is not in the interval an StatusException with code SYNC_STATUS_OBJECTNOTFOUND should be thrown

        $this->imap_reopenFolder($folderImapid);
        $s1 = @imap_delete($this->mbox, $iuid, FT_UID);
        $s11 = @imap_setflag_full($this->mbox, $iuid, "\\Deleted", FT_UID);
        $s2 = @imap_expunge($this->mbox);

        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->DeleteMessage('%s','%s'): result: s-delete: '%s' s-expunge: '%s' setflag: '%s'", $folderid, $id, $s1, $s2, $s11));

        return ($s1 && $s2 && $s11);
    }

    /**
     * Called when the user moves an item on the PDA from one folder to another
     *
     * @param string              $folderid            id of the source folder
     * @param string              $id                  id of the message
     * @param string              $newfolderid         id of the destination folder
     * @param ContentParameters   $contentParameters
     *
     * @access public
     * @return string                       new message id
     * @throws StatusException              could throw specific SYNC_MOVEITEMSSTATUS_* exceptions
     */
    public function MoveMessage($folderid, $id, $newfolderid, $contentParameters) {
        ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->MoveMessage('%s','%s','%s')", $folderid, $id, $newfolderid));
        if ($newfolderid === '(null)') {
            $this->DeleteMessage($folderid, $id, $contentParameters);
            return '';
        }
        if (strpos($folderid, "VIRTUAL") !== false) {
            /* PIM data: moving out is the same as deleting */
            if (strpos($folderid, "calendar") !== false)
                $this->syncphony->deleteItem($id, 'Event');
            elseif (strpos($folderid, "contacts") !== false)
                $this->syncphony->deleteItem($id, 'Contact');
            return "";
        }
        $folderImapid = $this->getImapIdFromFolderId($folderid);
        $newfolderImapid = $this->getImapIdFromFolderId($newfolderid);
        if (!($iuid = $this->getImapIdFromServerId($folderid, $id)))
            return false;

        // TODO SyncInterval check + ContentParameters
        // see https://jira.zarafa.com/browse/ZP-258 for details
        // before moving the message, it should be checked if the message is in the SyncInterval
        // to determine the cutoffdate use Utils::GetCutOffDate($contentparameters->GetFilterType());
        // if the message is not in the interval an StatusException with code SYNC_MOVEITEMSSTATUS_INVALIDSOURCEID should be thrown

        $this->imap_reopenFolder($folderImapid);

        // TODO this should throw a StatusExceptions on errors like SYNC_MOVEITEMSSTATUS_SAMESOURCEANDDEST,SYNC_MOVEITEMSSTATUS_INVALIDSOURCEID,SYNC_MOVEITEMSSTATUS_CANNOTMOVE

        // read message flags
        $overview = @imap_fetch_overview($this->mbox, $iuid, FT_UID);

        if (!$overview)
            throw new StatusException(sprintf("BackendSyncphony->MoveMessage('%s','%s','%s'): Error, unable to retrieve overview of source message: %s", $folderid, $id, $newfolderid, imap_last_error()), SYNC_MOVEITEMSSTATUS_INVALIDSOURCEID);
        else {
            // get next UID for destination folder
            // when moving a message we have to announce through ActiveSync the new messageID in the
            // destination folder. This is a "guessing" mechanism as IMAP does not inform that value.
            // when lots of simultaneous operations happen in the destination folder this could fail.
            // in the worst case the moved message is displayed twice on the mobile.
            $destStatus = imap_status($this->mbox, $this->server . $newfolderImapid, SA_ALL);
            if (!$destStatus)
                throw new StatusException(sprintf("BackendSyncphony->MoveMessage('%s','%s','%s'): Error, unable to open destination folder: %s", $folderid, $id, $newfolderid, imap_last_error()), SYNC_MOVEITEMSSTATUS_INVALIDDESTID);

            $newid = $destStatus->uidnext;

            // move message
            $s1 = imap_mail_move($this->mbox, $iuid, $newfolderImapid, CP_UID);
            if (!$s1)
                throw new StatusException(sprintf("BackendSyncphony->MoveMessage('%s','%s','%s'): Error, copy to destination folder failed: %s", $folderid, $id, $newfolderid, imap_last_error()), SYNC_MOVEITEMSSTATUS_CANNOTMOVE);

            // delete message in from-folder
            $s2 = imap_expunge($this->mbox);

            // open new folder
            $stat = $this->imap_reopenFolder($newfolderImapid);
            if (!$stat)
                throw new StatusException(sprintf("BackendSyncphony->MoveMessage('%s','%s','%s'): Error, openeing the destination folder: %s", $folderid, $id, $newfolderid, imap_last_error()), SYNC_MOVEITEMSSTATUS_CANNOTMOVE);

            // remove all flags
            $s3 = @imap_clearflag_full($this->mbox, $newid, "\\Seen \\Answered \\Flagged \\Deleted \\Draft", FT_UID);
            $newflags = "";
            if ($overview[0]->seen) $newflags .= "\\Seen";
            if ($overview[0]->flagged) $newflags .= " \\Flagged";
            if ($overview[0]->answered) $newflags .= " \\Answered";
            $s4 = @imap_setflag_full($this->mbox, $newid, $newflags, FT_UID);

            ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->MoveMessage('%s','%s','%s'): result s-move: '%s' s-expunge: '%s' unset-Flags: '%s' set-Flags: '%s'", $folderid, $id, $newfolderid, Utils::PrintAsString($s1), Utils::PrintAsString($s2), Utils::PrintAsString($s3), Utils::PrintAsString($s4)));

            // return the new id "as string""
            return $this->getServerIdFromImapId($newfolderid, $newid);
        }
    }


    /**----------------------------------------------------------------------------------------------------------
     * protected IMAP methods
     */

    /**
     * Unmasks a hex folderid and returns the imap folder id
     *
     * @param string        $folderid       hex folderid generated by convertImapId()
     *
     * @access protected
     * @return string       imap folder id
     */
    protected function getImapIdFromFolderId($folderid) {
        if (strpos($folderid, "VIRTUAL") !== false) {
            util_debugJ(NULL, 'getImapIdFromFolderId: bogus caller, PIM folder', $folderid);
            return false;
        }

        $this->InitializePermanentStorage();

        if (isset($this->permanentStorage->fmFidFimap)) {
            if (isset($this->permanentStorage->fmFidFimap[$folderid])) {
                $imapId = $this->permanentStorage->fmFidFimap[$folderid];
                //ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->getImapIdFromFolderId('%s') = %s", $folderid, $imapId));
                return $imapId;
            }
            else {
                ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->getImapIdFromFolderId('%s') = %s", $folderid, 'not found'));
                return false;
            }
        }
        ZLog::Write(LOGLEVEL_WARN, sprintf("BackendSyncphony->getImapIdFromFolderId('%s') = %s", $folderid, 'not initialized!'));
        return false;
    }

    /**
     * Retrieves a hex folderid previousily masked imap
     *
     * @param string        $imapid         Imap folder id
     *
     * @access protected
     * @return string       hex folder id
     */
    protected function getFolderIdFromImapId($imapid) {
        $this->InitializePermanentStorage();

        if (isset($this->permanentStorage->fmFimapFid)) {
            if (isset($this->permanentStorage->fmFimapFid[$imapid])) {
                $folderid = $this->permanentStorage->fmFimapFid[$imapid];
                //ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->getFolderIdFromImapId('%s') = %s", $imapid, $folderid));
                return $folderid;
            }
            else {
                ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->getFolderIdFromImapId('%s') = %s", $imapid, 'not found'));
                return false;
            }
        }
        ZLog::Write(LOGLEVEL_WARN, sprintf("BackendSyncphony->getFolderIdFromImapId('%s') = %s", $imapid, 'not initialized!'));
        return false;
    }

    /**
     * Masks a imap folder id into a generated hex folderid
     * The method getFolderIdFromImapId() is consulted so that an
     * imapid always returns the same hex folder id
     *
     * @param string        $imapid         Imap folder id
     *
     * @access protected
     * @return string       hex folder id
     */
    protected function convertImapId($imapid) {
        $this->InitializePermanentStorage();

        // check if this imap id was converted before
        $folderid = $this->getFolderIdFromImapId($imapid);

        // nothing found, so generate a new id and put it in the cache
        if (!$folderid) {
            if (!isset($this->permanentStorage->fmFidFimap))
                $this->permanentStorage->fmFidFimap = array();

            // generate folderid and add it to the mapping
            do {
                $folderid = sprintf('%04x%04x', util_randnum(0xFFFF), util_randnum(0xFFFF));
                /* prevent accidentally hitting an existing one */
            } while (isset($this->permanentStorage->fmFidFimap[$folderid]));

            // folderId to folderImap mapping
            $a = $this->permanentStorage->fmFidFimap;
            $a[$folderid] = $imapid;
            $this->permanentStorage->fmFidFimap = $a;

            // folderImap to folderid mapping
            if (!isset($this->permanentStorage->fmFimapFid))
                $this->permanentStorage->fmFimapFid = array();

            $b = $this->permanentStorage->fmFimapFid;
            $b[$imapid] = $folderid;
            $this->permanentStorage->fmFimapFid = $b;
        }

        //ZLog::Write(LOGLEVEL_DEBUG, sprintf("BackendSyncphony->convertImapId('%s') = %s", $imapid, $folderid));

        return $folderid;
    }

    /**
     * Get message body as plaintext
     *
     * @param string        $message        mimedecode message(part)
     *
     * @access protected
     * @return string
     */
    protected function getPlainBody($message) {
        $this->flattenMessage($message);
        $s = '';
        foreach ($message->linear_body_parts as $bodypart) {
            if (util_ifsetor($bodypart['plain']))
                $s .= $bodypart['plain'] . "\n";
            elseif (util_ifsetor($bodypart['html']))
                $s .= Utils::ConvertHtmlToText($bodypart['html']) . "\n";
        }
        return util_sanitise_multiline_submission(trim($s));
    }

    /**
     * Helper to re-initialize the folder to speed things up
     * Remember what folder is currently open and only change if necessary
     *
     * @param string        $folderid       id of the folder
     * @param boolean       $force          re-open the folder even if currently opened
     *
     * @access protected
     * @return
     */
    protected function imap_reopenFolder($folderid, $force = false) {
        // to see changes, the folder has to be reopened!
           if ($this->mboxFolder != $folderid || $force) {
               $s = @imap_reopen($this->mbox, $this->server . $folderid);
               // TODO throw status exception
               if (!$s) {
                ZLog::Write(LOGLEVEL_WARN, "BackendSyncphony->imap_reopenFolder('%s'): failed to change folder: ", $folderid, implode(", ", imap_errors()));
                return false;
               }
            $this->mboxFolder = $folderid;
        }
    }


    /**
     * Build a multipart RFC822, embedding body and one file (for attachments)
     *
     * @param string        $filenm         name of the file to be attached
     * @param long          $filesize       size of the file to be attached
     * @param string        $file_cont      content of the file
     * @param string        $body           current body
     * @param string        $body_ct        content-type
     * @param string        $body_cte       content-transfer-encoding
     * @param string        $boundary       optional existing boundary
     * @param string        $file_contenttype optional content-type of the file
     *
     * @access protected
     * @return array        with [0] => $mail_header and [1] => $mail_body
     */
    protected function mail_attach($filenm, $filesize, $file_cont, $body, $body_ct, $body_cte, $boundary=false, $file_contenttype=false) {
        if (!$boundary) $boundary = strtoupper(md5(util_randnum() . uniqid($this->syncphony->currentTimestring)));

        //remove the ending boundary because we will add it at the end
        $body = str_replace("--$boundary--", "", $body);

        $mail_header = "Content-Type: multipart/mixed; boundary=\"$boundary\"";

        // build main body with the sumitted type & encoding from the pda
        if (strpos($body, "--$boundary") === 0) {
            $mail_body = $body;
        } else {
            $mail_body = $this->enc_multipart($boundary, $body, $body_ct, $body_cte);
        }
        $mail_body .= $this->enc_attach_file($boundary, $filenm, $filesize, $file_cont, $file_contenttype);

        $mail_body .= "--$boundary--\n\n";
        return array($mail_header, $mail_body);
    }

    /**
     * Helper for mail_attach()
     *
     * @param string        $boundary       boundary
     * @param string        $body           current body
     * @param string        $body_ct        content-type
     * @param string        $body_cte       content-transfer-encoding
     *
     * @access protected
     * @return string       message body
     */
    protected function enc_multipart($boundary, $body, $body_ct, $body_cte) {
        $mail_body = "This is a multi-part message in MIME format\n\n";
        $mail_body .= "--$boundary\n";
        $mail_body .= "Content-Type: $body_ct\n";
        $mail_body .= "Content-Transfer-Encoding: $body_cte\n\n";
        $mail_body .= "$body\n\n";

        return $mail_body;
    }

    /**
     * Helper for mail_attach()
     *
     * @param string        $boundary       boundary
     * @param string        $filenm         name of the file to be attached
     * @param long          $filesize       size of the file to be attached
     * @param string        $file_cont      content of the file
     * @param string        $content_type   optional content-type
     *
     * @access protected
     * @return string       message body
     */
    protected function enc_attach_file($boundary, $filenm, $filesize, $file_cont, $content_type = "") {
        if (!$content_type) $content_type = "text/plain";
        if (($iseml = ($content_type == "message/rfc822"))) {
            /* check that message is not 8BITMIME */
            $iseml = !preg_match("/[^\r\n\t -~]/", $file_cont);
        }
        $mail_body = "--$boundary\n";
        $mail_body .= "Content-Type: $content_type; name=\"$filenm\"\n";
        $mail_body .= "Content-Transfer-Encoding: " .
          ($iseml ? "7bit" : "base64") . "\n";
        $mail_body .= "Content-Disposition: " . ($iseml ? "inline" : "attachment") .
          "; filename=\"$filenm\"\n";
        $mail_body .= "Content-Description: $filenm\n\n";
        $mail_body .= ($iseml ? $file_cont :
          //contrib - chunk base64 encoded attachments
          chunk_split(base64_encode($file_cont))) . "\n\n";

        return $mail_body;
    }

    /**
     * Adds a message with seen flag to a specified folder (used for saving sent items)
     *
     * @param string        $folderid       id of the folder
     * @param string        $header         header of the message
     * @param long          $body           body of the message
     *
     * @access protected
     * @return boolean      status
     */
    protected function addSentMessage($folderid, $header, $body) {
        $header_body = trim($header) . "\015\012\015\012" . $body;
        $res = imap_append($this->mbox, $this->server . $folderid, $header_body, "\\Seen");
        if (!$res)
            util_logerr(LOGLEVEL_DEBUG, 'addSentMessage: imap_append(this->mbox, ' .
              minijson_encdbg($this->server . $folderid) . ', ' .
              minijson_encdbg($header_body) . ', \\Seen) failed: ' .
              minijson_encdbg(imap_errors()));
        return $res;
    }

    /**
     * Parses an mimedecode address array back to a simple "," separated string
     *
     * @param array         $ad             addresses array
     *
     * @access protected
     * @return string       mail address(es) string
     */
    protected function parseAddr($ad) {
        $addr_string = "";
        if (isset($ad) && is_array($ad)) {
            foreach ($ad as $addr) {
                if ($addr_string)
                    $addr_string .= ',';
                $addr_string .= $addr->mailbox . '@' . $addr->host;
            }
        }
        return $addr_string;
    }

    /**
     * Removes parenthesis (comments) from the date string because
     * strtotime returns false if received date has them
     *
     * @param string        $receiveddate   a date as a string
     *
     * @access protected
     * @return time_t or NULL
     */
    protected function cleanupDate($receiveddate) {
        $receiveddate = strtotime(preg_replace("/\(.*\)/", "", $receiveddate));
        if ($receiveddate === false) {
            ZLog::Write(LOGLEVEL_DEBUG, "Received date is false. Message might be broken.");
            return NULL;
        }

        return $receiveddate ? : 10;
    }

    /**
     * Indicates which AS version is supported by the backend.
     *
     * @access public
     * @return string       AS version constant
     */
    public function GetSupportedASVersion() {
        return ZPush::ASV_14;
    }

    /**
     * Processes a response to a meeting request.
     * CalendarID is a reference and has to be set if a new calendar item is created
     *
     * @param string        $requestid      id of the object containing the request
     * @param string        $folderid       id of the parent folder of $requestid
     * @param string        $response
     *
     * @access public
     * @return string       id of the created/updated calendar obj
     * @throws StatusException
     */
    public function MeetingResponse($requestid, $folderid, $response) {
        $parent = $this->getImapIdFromFolderId($folderid);

        if (!($stat = $this->StatMessage($folderid, $requestid)))
            throw new StatusException("MeetingResponse($requestid, $folderid, $response): unknown message", SYNC_MEETRESPSTATUS_INVALIDMEETREQ);

        switch ($response) {
        case 1:
        case 2:
            break;
        case 3:
            if (($w = $this->GetWasteBasket()))
                $this->MoveMessage($folderid, $requestid, $w, array());
            else
                $this->DeleteMessage($folderid, $requestid, array());
            return '';
            break;
        default:
            return false;
        }

        if (!($iuid = $this->getImapIdFromServerId($folderid, $requestid)))
            return false;

        $this->imap_reopenFolder($parent);
        $mail = @imap_fetchheader($this->mbox, $iuid, FT_UID) .
          @imap_body($this->mbox, $iuid, FT_PEEK | FT_UID);
        $mobj = new Mail_mimeDecode($mail);
        $message = $mobj->decode(array('decode_headers' => true, 'decode_bodies' => true, 'include_bodies' => true, 'charset' => 'utf-8'));

        $this->flattenMessage($message);
        if (count($message->linear_iMip) != 1)
            throw new StatusException("MeetingResponse($requestid, $folderid, $response): no iMip in message", SYNC_MEETRESPSTATUS_INVALIDMEETREQ);

        require_once('/usr/share/simkolab/bb/backend/syncphony/vobject.php');
        if (($v = vobject_parse($message->linear_iMip[0], $message->headers)) === false ||
          vobject_check_iMip($v) !== true)
            throw new StatusException("MeetingResponse($requestid, $folderid, $response): wrong iMip in message", SYNC_MEETRESPSTATUS_INVALIDMEETREQ);

        /* create SOAP event body */
        $soapBody = array();
        $x = array(
            isset($v->VEVENT->DTSTAMP) ? vobject_dt2dt($v->VEVENT->DTSTAMP, $v) : NULL,
            isset($v->VEVENT->CREATED) ? vobject_dt2dt($v->VEVENT->CREATED, $v) : NULL,
            isset($v->VEVENT->{'LAST-MODIFIED'}) ? vobject_dt2dt($v->VEVENT->{'LAST-MODIFIED'}, $v) : NULL,
          );
        $soapBody['event']['creationDate'] = syncphony::renderDateTime(
          $x[1] !== NULL ? $x[1] : ($x[2] !== NULL ? $x[2] : $x[0]));
        $soapBody['event']['lastModificationDate'] = syncphony::renderDateTime(
          $x[2] !== NULL ? $x[2] : ($x[0] !== NULL ? $x[0] : $x[1]));
        if (isset($v->VEVENT->DESCRIPTION) && ($v->VEVENT->DESCRIPTION))
            $soapBody['event']['body'] = strval($v->VEVENT->DESCRIPTION);
        $x = strtolower(isset($v->VEVENT->CLASS) ? strval($v->VEVENT->CLASS) : '');
        if ($x != 'private' && $x != 'confidential')
            $x = 'public';
        $soapBody['event']['sensitivity'] = $x;
        if (isset($v->VEVENT->SUMMARY) && ($v->VEVENT->SUMMARY))
            $soapBody['event']['summary'] = strval($v->VEVENT->SUMMARY);
        $x = '';
        if (isset($v->VEVENT->GEO)) {
            $x = $v->VEVENT->GEO->getParts();
            if (($ns = $x[0]) < 0) {
                $ns = -$ns;
                $sns = 'S';
            } else
                $sns = 'N';
            $tns = (int)$ns;
            $ns = ($ns - $tns) * 60 + 0.0005;
            if (($we = $x[1]) < 0) {
                $we = -$we;
                $swe = 'W';
            } else
                $swe = 'E';
            $twe = (int)$we;
            $we = ($we - $twe) * 60 + 0.0005;
            $x = sprintf('%s %02d° %06.3F %s %03d° %06.3F - ' .
              'http://www.openstreetmap.org/?mlat=%f&mlon=%f',
              $sns, $tns, $ns, $swe, $twe, $we,
              $x[0], $x[1]);
        }
        if (isset($v->VEVENT->LOCATION) && ($v->VEVENT->LOCATION))
            $x = ($x ? ($x . ' - ') : '') . $v->VEVENT->LOCATION;
        if ($x)
            $soapBody['event']['location'] = $x;
        if (isset($v->VEVENT->ORGANIZER) && ($v->VEVENT->ORGANIZER)) {
            $x = vobject_split_caladdress($v->VEVENT->ORGANIZER);
            if ($x[0][0])
                $soapBody['event']['organizerDisplayName'] = $x[0][0];
            $soapBody['event']['organizerSmtpAddress'] = $x[0][1];
        } else
            $soapBody['event']['organizerSmtpAddress'] = $this->username;
        if (isset($v->VEVENT->DTEND))
            $soapBody['event']['endDate'] = vobject_dt2timet($v->VEVENT->DTEND, $v);
        elseif (isset($v->VEVENT->DURATION) && isset($v->VEVENT->DTSTART))
            $soapBody['event']['endDate'] = vobject_dt2timet($v->VEVENT->DTSTART, $v,
              $v->VEVENT->DURATION->getDateInterval());
        if (isset($v->VEVENT->DTSTART)) {
            $soapBody['event']['allDay'] = $v->VEVENT->DTSTART->hasTime() ? 0 : 1;
            $soapBody['event']['startDate'] = vobject_dt2timet($v->VEVENT->DTSTART, $v);
            if ($soapBody['event']['allDay']) {
                /* eh, what? time_t for normal, string for all-day events? */
                $soapBody['event']['startDate'] = vobject_timet_day($soapBody['event']['startDate']);
                if (isset($soapBody['event']['endDate']))
                    $soapBody['event']['endDate'] = vobject_timet_day($soapBody['event']['endDate'], -1);
                else
                    $soapBody['event']['endDate'] = $soapBody['event']['startDate'];
            } elseif (!isset($soapBody['event']['endDate']))
                $soapBody['event']['endDate'] = $soapBody['event']['startDate'];
        }
        /* we do not import reminder from external sources */
        /* Integer alarm; */
        // missing: recurrence
        $soapBody['event']['attendee'] = array();
        if (isset($v->VEVENT->ATTENDEE) && ($v->VEVENT->ATTENDEE))
            foreach (vobject_split_caladdress($v->VEVENT->ATTENDEE) as $k => $w) {
                $x = array();
                if ($w[0])
                  $x['displayName'] = $w[0];
                $x['smtpAddress'] = $w[1];
                switch (strtoupper(isset($v->VEVENT->ATTENDEE[$k]['PARTSTAT']) ?
                  strval($v->VEVENT->ATTENDEE[$k]['PARTSTAT']) : '')) {
                case '':
                case 'NEEDS-ACTION':
                    $x['status'] = 'needs-action';
                    break;
                case 'ACCEPTED':
                    $x['status'] = 'accepted';
                    break;
                case 'DECLINED':
                    $x['status'] = 'declined';
                    break;
                case 'TENTATIVE':
                    $x['status'] = 'tentative';
                    break;
                case 'DELEGATED':
                    $x['status'] = 'delegated';
                    break;
                default:
                    $x['status'] = 'none';
                    break;
                }
                switch (strtoupper(isset($v->VEVENT->ATTENDEE[$k]['ROLE']) ?
                  strval($v->VEVENT->ATTENDEE[$k]['ROLE']) : '')) {
                case '':
                case 'REQ-PARTICIPANT':
                    $x['role'] = 'required';
                    break;
                case 'OPT-PARTICIPANT':
                    $x['role'] = 'optional';
                    break;
                case 'NON-PARTICIPANT':
                    $x['role'] = 'optional';
                    break;
                case 'CHAIR':
                    $x['role'] = 'required';
                    break;
                default:
                    $x['role'] = 'resource';
                    break;
                }
                $x['requestResponse'] = (isset($v->VEVENT->ATTENDEE[$k]['RSVP']) &&
                  strtoupper(strval($v->VEVENT->ATTENDEE[$k]['RSVP'])) == 'TRUE') ?
                  true : false;
                if (util_emailcase($x['smtpAddress']) == util_emailcase($this->username)) {
                    /* that’s us */
                    $x['status'] = $response == 1 ? 'accepted' : 'tentative';
                    $x['requestResponse'] = false;
                }
                array_push($soapBody['event']['attendee'], $x);
            }
        if (!count($soapBody['event']['attendee']))
            unset($soapBody['event']['attendee']);
        if (isset($v->VEVENT->TRANSP)) switch (strval($v->VEVENT->TRANSP)) {
        case 'TRANSPARENT':
            $soapBody['event']['showTimeAs'] = 'free';
            break;
        case 'OPAQUE':
        default:
            $soapBody['event']['showTimeAs'] = 'busy';
            break;
        }
        if (isset($v->VEVENT->CATEGORIES) && ($v->VEVENT->CATEGORIES)) {
            $soapBody['event']['categories'] = array();
            foreach ($v->VEVENT->CATEGORIES as $w)
                $soapBody['event']['categories'][] = implode(',', $w->getParts());
            $soapBody['event']['categories'] = implode(',', $soapBody['event']['categories']);
        }
        $soapBody['event']['colorLabel'] = "none";
        $soapBody['event']['uid'] = strval($v->VEVENT->UID);
        $soapBody['event']['productId'] = 'SimKolab ' . SIMKOLAB4_VERSION;
        $attfiles = array();
        if (isset($v->VEVENT->ATTACH)) {
            $soapBody['event']['linkAttachment'] = array();
            $x = 0;
            while ($x < count($v->VEVENT->ATTACH)) {
                if (isset($v->VEVENT->ATTACH[$x]['VALUE']) &&
                  $v->VEVENT->ATTACH[$x]['VALUE'] == 'BINARY') {
                    $attname = sprintf("att%d.bin", $x);
                    $attmime = isset($v->VEVENT->ATTACH[$x]['FMTTYPE']) ?
                      strval($v->VEVENT->ATTACH[$x]['FMTTYPE']) : '';
                    $attmime = $attmime ? $attmime : 'application/octet-stream';
                    $attfiles[$attname] = array(
                        'content' => strval($v->VEVENT->ATTACH[$x]),
                        'contentType' => $attmime,
                      );
                } else {
                    $attname = strval($v->VEVENT->ATTACH[$x]);
                    if ($attname)
                        $soapBody['event']['linkAttachment'][] = $attname;
                }
                ++$x;
            }
            if (count($attfiles))
                $soapBody['event']['inlineAttachment'] = array_keys($attfiles);
            if (!count($soapBody['event']['linkAttachment']))
                unset($soapBody['event']['linkAttachment']);
        }

        if (!($x = $this->syncphony->changeItem('Event',
          syncphony::checkItemExists(
          $this->syncphony->getStandardFolder('Event'),
          $soapBody['event']['uid']), NULL, $soapBody)))
            throw new StatusException("MeetingResponse($requestid, $folderid, $response): could not create calendar item", SYNC_MEETRESPSTATUS_INVALIDMEETREQ);

        if (count($attfiles) && ($kolabId = syncphony::getKolabIDFromSyncphonyID($x['id']))) {
            $folderId = $kolabId['folder_id'];
            $kolabId = $kolabId['id'];
            foreach ($attfiles as $attname => $attmime)
                $this->syncphony->setAttachment($folderId, $kolabId,
                  $attname, $this->syncphony->currentTimestring, $attmime);
        }
        if (($w = $this->GetWasteBasket()))
            $this->MoveMessage($folderid, $requestid, $w, array());
        else
            $this->DeleteMessage($folderid, $requestid, array());
        return $x['id'];
    }

    /**
     * Generates IMAP name from præfix, serverdelimiter and name.
     *
     * @param string $folder_name
     * @return string
     * @access private
     */
    private function create_name_folder($folder_name) {
        /* no præfix known */
        if (IMAP_FOLDER_PREFIX == '')
            return $folder_name;

        $lf = strtolower($folder_name);
        $li = strtolower(IMAP_FOLDER_INBOX);
        $lp = strtolower(IMAP_FOLDER_PREFIX);

        /* præfixless INBOX? */
        if (!IMAP_FOLDER_PREFIX_IN_INBOX && $lf == $li)
            return $folder_name;
        /* transition case: IMAP_FOLDER_PREFIX == IMAP_FOLDER_INBOX */
        if (IMAP_FOLDER_PREFIX_IN_INBOX && $lf == $li && $lp == $li) {
            ZLog::Write(LOGLEVEL_WARN, 'configuration error: IMAP_FOLDER_PREFIX = IMAP_FOLDER_INBOX but IMAP_FOLDER_PREFIX_IN_INBOX is set, ignoring');
            return $folder_name;
        }

        /* empty folder part? drop the delimiter then */
        if ($folder_name == '')
            return IMAP_FOLDER_PREFIX;

        return IMAP_FOLDER_PREFIX . $this->serverdelimiter . $folder_name;
    }

    /**
     * Reloads folders from IMAP, sets serverdelimiter.
     *
     * Sets these members:
     * + $this->serverdelimiter (usually '/', default '.')
     * + $this->sk_mailboxes = array(
     *      'sent' ?= imapid of sent-mail folder,
     *      'trash' ?= imapid of trash folder,
     *      'folders' = array(
     *          imapid* = array(
     *              'imapname' = imapid of this folder,
     *              'type' = SYNC_FOLDER_TYPE_*,
     *              'displayname' = UTF-8 displayname,
     *              'parent' = false (root) or imapid of parent,
     *          ),
     *      ),
     *   );
     * Note: parent imapid is guaranteed to exist in sk_mailboxes->folders.
     * If imap_getmailboxes failed, $this->sk_mailboxes['folders'] is an
     * empty array (falsy) and the other keys are unset.
     */
    protected function reloadFoldersFromIMAP() {
        $imaplst = @imap_getmailboxes($this->mbox, $this->server, "*");
        $this->serverdelimiter = is_array($imaplst) ?
          $imaplst[0]->delimiter : /* default */ '.';
        if (!is_array($imaplst)) {
            ZLog::Write(LOGLEVEL_WARN, 'BackendSyncphony->reloadFoldersFromIMAP: imap_list failed: ' . imap_last_error());
            $this->sk_mailboxes = array('folders' => array());
            return;
        }

        $sm = array();
        $ss = array();
        $sl = strlen($this->server);
        $qi = strtolower($this->create_name_folder(IMAP_FOLDER_INBOX));
        $qd = strtolower($this->create_name_folder(IMAP_FOLDER_DRAFT));
        $qs = strtolower($this->create_name_folder(IMAP_FOLDER_SENT));
        $qt = strtolower($this->create_name_folder(IMAP_FOLDER_TRASH));
        $qj = strtolower($this->create_name_folder(IMAP_FOLDER_SPAM));
        $qa = strtolower($this->create_name_folder(IMAP_FOLDER_ARCHIVE));

        /* filter all mailboxen, gather basic info */
        foreach ($imaplst as $fld) {
            /*
             * This is the perfect place to filter out PIM source folders,
             * cf. [#839]
             */

            $ni = substr($fld->name, $sl);
            $nl = strtolower($ni);
            $excluded = false;
            foreach ($this->excludedFolders as $x) {
                if (strpos($nl, $x) !== false) {
                    $excluded = $x;
                    break;
                }
            }
            if ($excluded !== false) {
                ZLog::Write(LOGLEVEL_DEBUG, sprintf("Pattern: <%s> found, excluding folder: '%s'",
                  $x, $ni));
                continue;
            }

            $sf = array(
                'imapname' => $ni,
                'type' => SYNC_FOLDER_TYPE_USER_MAIL,
              );
            /* order important */
            if ($nl == $qi) {
                $sf['displayname'] = 'INBOX';
                $sf['parent'] = false;
                $sf['type'] = SYNC_FOLDER_TYPE_INBOX;
                $ss['inbox'] = $ni;
            } elseif ($nl == $qs) {
                $sf['displayname'] = 'Sent';
                $sf['parent'] = false;
                $sf['type'] = SYNC_FOLDER_TYPE_SENTMAIL;
                $ss['sent'] = $ni;
            } elseif ($nl == $qt) {
                $sf['displayname'] = 'Trash';
                $sf['parent'] = false;
                $sf['type'] = SYNC_FOLDER_TYPE_WASTEBASKET;
                $ss['trash'] = $ni;
            } elseif ($nl == $qa) {
                $sf['displayname'] = 'Archive';
                $sf['parent'] = false;
            } elseif ($nl == $qj) {
                $sf['displayname'] = 'Junk';
                $sf['parent'] = false;
            } elseif ($nl == $qd) {
                $sf['displayname'] = 'Drafts';
                $sf['parent'] = false;
                $sf['type'] = SYNC_FOLDER_TYPE_DRAFTS;
            }
            $sm[$ni] = $sf;
        }

        if (!isset($ss['inbox']))
            ZLog::Write(LOGLEVEL_ERROR, 'No INBOX found, likely a configuration error');

        $lp = strtolower(IMAP_FOLDER_PREFIX);

        /* second pass, determine parent vs displayname cutoff */
        foreach ($sm as $ni => $sf) {
            /* parent already set? */
            if (isset($sm[$ni]['parent'])) {
                /* determine displayname if necessary */
                if (!isset($sm[$ni]['displayname'])) {
                    /* root, or parent + delimiter */
                    $sp = $sm[$ni]['parent'] === false ? '' :
                      $sm[$ni]['parent'] . $this->serverdelimiter;
                    /* root, or ni = parent + delimiter + displayname */
                    if ($sp !== '' && $sp === substr($ni, 0, strlen($sp)))
                        $sm[$ni]['displayname'] = substr($ni, strlen($sp));
                    /* root is handled trivially below */
                }
            } else {
                /* if no parent can be found, default to root */
                $sm[$ni]['parent'] = false;
                /* determine parent */
                $nl = (int)strlen($ni);
                $so = 0;
                do {
                    $sp = strrpos($ni, $this->serverdelimiter, -$so);
                    if ($sp === false)
                        break;
                    $np = substr($ni, 0, $sp);
                    if (strlen($np) > 0 && strlen($np) < $nl &&
                      isset($sm[$np])) {
                        /* found a non-root case */
                        $sm[$ni]['parent'] = $np;
                        $sm[$ni]['displayname'] = substr($ni, $sp + 1);
                        /* drop initial prefix */
                        if ($lp != '' && strtolower($np) === $lp)
                            $sm[$ni]['parent'] = false;
                        break;
                    }
                    $so = $nl - (int)$sp + 1;
                } while ($so <= $nl);
            }
            /* not found? solve trivially */
            if (!isset($sm[$ni]['displayname']))
                $sm[$ni]['displayname'] = $ni;
            /* now convert into the correct encoding */
            $sm[$ni]['displayname'] = util_fixutf8(mb_convert_encoding($sm[$ni]['displayname'],
              'UTF-8', 'UTF7-IMAP'));
        }

        $ss['folders'] = $sm;
        $this->sk_mailboxes = $ss;
    }

    /**
     * Unmasks a ServerEntryId and returns the IMAP message UID
     *
     * @param string $folderid  hex folderid generated by convertImapId()
     * @param string $serverid  opaque serverid generated by getServerIdFromImapId()
     *
     * @access protected
     * @return string           IMAP message UID
     */
    protected function getImapIdFromServerId($folderid, $serverid) {
        if (preg_match('/^[0-9]+$/', $serverid)) {
            /* old, not unique */
            return $serverid;
        }
        if (!strpos($serverid, '/')) {
            /* no slash */
            util_debugJ('ERR', true, 'getImapIdFromServerId: invalid serverid');
            return false;
        }
        $sid = explode('/', $serverid);
        if (count($sid) != 3 || $sid[0] != 'syncphony-imap' ||
          !preg_match('/^[0-9]+$/', $sid[2])) {
            util_debugJ('ERR', true, 'getImapIdFromServerId: invalid serverid');
            return false;
        }
        if ($sid[1] !== $folderid) {
            util_debugJ('ERR', true, 'getImapIdFromServerId: serverid folder mismatch');
            return false;
        }
        return $sid[2];
    }

    /**
     * Masks an IMAP message UID for a specific folder into an opaque
     * ServerEntryId which is globally unique (per user+device tuple)
     *
     * @param string $folderid  hex folderid generated by convertImapId()
     * @param int    $imapUID   numeric IMAP message UID
     *
     * @access protected
     * @return string           opaque server ID
     */
    protected function getServerIdFromImapId($folderid, $imapUID) {
        if (!preg_match('/^[0-9a-fA-F]+$/', $folderid)) {
            util_debugJ('ERR', true, 'getServerIdFromImapId: invalid folderid');
            return false;
        }
        if (!preg_match('/^[0-9]+$/', strval($imapUID))) {
            util_debugJ('ERR', true, 'getServerIdFromImapId: invalid imapUID');
            return false;
        }
        return sprintf('syncphony-imap/%s/%d', $folderid, $imapUID);
    }
};
