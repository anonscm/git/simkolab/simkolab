<?php
/***********************************************
* File      :   compat.php
* Project   :   SimKolab 4
* Descr     :   Help function for files
*
* Created   :   01.10.2007
*
* Copyright 2007 - 2016 Zarafa Deutschland GmbH
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License, version 3,
* as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* Consult the Debian copyright file for details.
************************************************/

if (!function_exists("apache_request_headers")) {
    /**
      * When using other webservers or using php as cgi in apache
      * the function apache_request_headers() is not available.
      * This function parses the environment variables to extract
      * the necessary headers for Z-Push
      */
    function apache_request_headers() {
        $headers = array();
        foreach ($_SERVER as $key => $value)
            if (substr($key, 0, 5) == 'HTTP_')
                $headers[strtr(substr($key, 5), '_', '-')] = $value;

        return $headers;
    }
}
