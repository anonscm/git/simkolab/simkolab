<?php

namespace Sabre\VObject;

/**
 * Useful utilities for working with various strings.
 *
 * Copyright (C) 2011-2016 Evert Pot (http://evertpot.com/),
 *	fruux GmbH (https://fruux.com/)
 * Copyright © 2014, 2016, 2017 mirabilos,
 *	tarent solutions GmbH (https://www.tarent.de/)
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * - Neither the name Sabre nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

class StringUtil {

    /**
     * Returns true or false depending on if a string is valid UTF-8
     *
     * @param string $str
     * @return bool
     */
    static public function isUTF8($str) {

        // Control characters
        if (preg_match('%[\x00-\x08\x0B-\x0C\x0E-\x1F\x7F]%', $str)) {
            return false;
        }

        return (bool)preg_match('%%u', $str);

    }

    /**
     * This method tries its best to convert the input string to UTF-8.
     *
     * Currently only ISO-5991-1 input and UTF-8 input is supported, but this
     * may be expanded upon if we receive other examples.
     *
     * @param string $str
     * @return string
     */
    static public function convertToUTF8($str) {
        /* ensure UTF-8 and remove any C0 control characters (C1 are fine) */
        $r = preg_replace('%(?:[\x00-\x08\x0B-\x0C\x0E-\x1F\x7F])%', '',
          util_fixutf8($str));
        return is_string($str) && ($r === $str) ? $str : $r;
    }

}
