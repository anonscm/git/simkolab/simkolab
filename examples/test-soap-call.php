<?php
define('SYNCPHONY_SERVICE_WSDL', 'http://localhost:8080/kolab-ws/KolabService/KolabServicePortTypeImpl?wsdl');
require_once('/usr/share/simkolab/bb/backend/syncphony/UsernameToken.php');
require_once('/usr/share/simkolab/bb/backend/syncphony/SoapSecurityHeader.php');

function xmlescape($s) {
	return (str_replace(
	    array("&",     "<",     ">",     '"',     "'"),
	    array("&#38;", "&#60;", "&#62;", "&#34;", "&#39;"),
	    $s));
}

$c = new SoapClient(SYNCPHONY_SERVICE_WSDL, array("trace" => 1));

$nameSpace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
$nameSpaceTypePassword = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";

$x_username = 'user@host.domain.com';
$x_password = 'password';

$userToken = new SoapVar($x_username, XSD_STRING, NULL, $nameSpace, NULL, $nameSpace);
$passwToken = new SoapVar('<wssehack:Password xmlns:wssehack="' .
    xmlescape($nameSpace) . '" Type="' . xmlescape($nameSpaceTypePassword) .
    '">' . xmlescape($x_password) . '</wssehack:Password>', XSD_ANYXML);

$userNameToken = new UsernameToken($userToken, $passwToken);
$userNameTokenObject = new SoapVar($userNameToken, SOAP_ENC_OBJECT, NULL, $nameSpace,
		'UsernameToken', $nameSpace);

$securityHeaderFrame = new SoapSecurityHeader($userNameTokenObject);
$userToken = new SoapVar($securityHeaderFrame, SOAP_ENC_OBJECT, NULL, $nameSpace,
		'UsernameToken', $nameSpace);

$secHeaderValue = new SoapVar($userToken, SOAP_ENC_OBJECT, NULL,
		$nameSpace, 'Security', $nameSpace);
$secHeader = new SoapHeader($nameSpace, 'Security', $secHeaderValue);

print_r($c->__soapCall('getCurrentTime', array(), NULL, $secHeader));
