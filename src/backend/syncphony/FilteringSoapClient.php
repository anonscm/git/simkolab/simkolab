<?php
/*-
 * Copyright © 2017
 *	mirabilos <t.glaser@tarent.de>
 *
 * Provided that these terms and disclaimer and all copyright notices
 * are retained or reproduced in an accompanying document, permission
 * is granted to deal in this work without restriction, including un‐
 * limited rights to use, publicly perform, distribute, sell, modify,
 * merge, give away, or sublicence.
 *
 * This work is provided “AS IS” and WITHOUT WARRANTY of any kind, to
 * the utmost extent permitted by applicable law, neither express nor
 * implied; without malicious intent or gross negligence. In no event
 * may a licensor, author or contributor be held liable for indirect,
 * direct, other damage, loss, or other issues arising in any way out
 * of dealing in the work, even if advised of the possibility of such
 * damage or existence of a defect, except proven that it results out
 * of said person’s immediate fault when using the work as intended.
 *-
 * Filtering SOAP request. Ensures no invalid XML is sent on the wire
 * until https://bugzilla.gnome.org/show_bug.cgi?id=739574 is fixed.
 */

class FilteringSoapClient extends SoapClient {
	public function __doRequest($request, $location, $action, $version, $one_way=0) {
		$dbg = array(
			'request' => $request,
			'location' => $location,
			'action' => $action,
			'version' => $version,
			'one_way' => $one_way,
		);
		$freq = util_xmlutf8($request);
		if ($freq !== $request)
			$dbg['request-fixed'] = $freq;
		$response = parent::__doRequest($request, $location, $action, $version, $one_way);
		$frsp = util_xmlutf8($response);
		if (SOAP_LOGRAWXML) {
			$dbg['response'] = $response;
			if ($frsp !== $response)
				$dbg['response-fixed'] = $frsp;
			util_debugJ(true, NULL, 'FilteringSoapClient::__doRequest', $dbg);
		}
		return $frsp;
	}
}
